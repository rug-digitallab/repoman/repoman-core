# RepoMan Core
Repository containing the main RepoMan microservice, the platform dependant workers, and a project containing common enums 
and models. For specific information regarding design decisions refer to [the wiki](https://gitlab.com/rug-digitallab/documentation/digital-lab/-/wikis/RepoMan/Design).

# Prerequisites

## Repositories
The GitLab worker requires one GitLab repository to store the terraform artifacts and a group for the creation of the courses.

### Courses Group
The courses group is a GitLab group that contains the courses that are created by the worker. Courses will be created as
subgroups of this group. Note the courses group ID. 

### Artifacts Repository
The artifacts repository is a GitLab repository which stores the terraform artifacts that describe the structure of the course 
on GitLab and the users that are enrolled to this course. Note the artifacts repository project ID. This repository requires 
a very specific CI setup. It should have a `.gitlab-ci.yml` file that looks like this:

```yaml
include:
  - project: rug-digitallab/resources/gitlab-ci-templates
    ref: develop
    file: terraform/terraform.yml

stages:
  - Validate
  - Test
  - Plan
  - Apply

variables:
  TF_STATE_NAME: $CI_COMMIT_BRANCH

before_script:
  - if [[ $CI_COMMIT_BRANCH == course/* ]]; then export TF_STATE_NAME=${CI_COMMIT_BRANCH#course/}; fi

.course_rule: &course_rule
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^course\//'

validate:
  stage: Validate
  interruptible: true
  <<: *course_rule

plan:
  stage: Plan
  interruptible: true
  <<: *course_rule

kics-iac-sast:
  stage: Test
  <<: *course_rule

apply:
  stage: Apply
  <<: *course_rule
  when: manual
```

And a `main.tf.json` that looks like this:

```yaml
{
  "terraform": {
    "required_providers": {
      "gitlab": {
        "source": "gitlabhq/gitlab",
        "version": "17.3.1"
      }
    },
    "backend": {
      "http": {
      }
    }
  },
  "variable": {
    "courses_access_token": {
      "type": "string"
    }
  },
  "provider": {
    "gitlab": [
      {
        "base_url": "https://gitlab.com",
        "token": "${var.courses_access_token}"
      }
    ]
  }
}
```

## Access Tokens

### Personal Access Token 
Create a service account on the top-level group of the courses group: https://docs.gitlab.com/ee/api/group_service_accounts.html#create-a-service-account-user

Create a personal access token for the service account with `api` permissions: https://docs.gitlab.com/ee/api/group_service_accounts.html#create-a-personal-access-token-for-a-service-account-user

Invite the newly created service account to the courses group with `owner` permissions. 

This personal access token is configured as a GitLab CI/CD variable called `TF_VAR_courses_access_token` in the 
artifacts repository.

### Project Access Token
Create a project access token in the artifacts repository with the following settings:
- role: `maintainer` (as `developer` does not have access to the TF state) or a custom role with `Admin terraform state` permissions.  
- permissions: `api`

The artifacts repository also requires a GitLab CI/CD variable called `TF_VAR_courses_access_token` that contains 
the access token of the group in which the courses should be created.

## Environment Variables
The GitLab worker requires some environment variables to work. You can provide these directly or indirectly in a `.env` 
file in the `worker-gitlab` directory. The `.env` file should contain the following variables:

```
ARTIFACTS_GITLAB_URL=https://gitlab.com
ARTIFACTS_PROJECT_ID=12345678
ARTIFACTS_PROJECT_ACCESS_TOKEN=xxxxxxxxxx
COURSES_GROUP_ID=12345678
```

Where:
- `ARTIFACTS_GITLAB_URL` is the URL of the GitLab instance where the artifacts repository is hosted. 
- `ARTIFACTS_PROJECT_ID` is the GitLab ID of the project where the worker should store the terraform artifacts.
- `ARTIFACTS_PROJECT_ACCESS_TOKEN` - is a project access token that is used to push the artifacts to the repository that was specified in `ARTIFACTS_REPOSITORY_ID`.
- `COURSES_GROUP_ID` - is the GitLab ID of the group in which the courses should be created.
