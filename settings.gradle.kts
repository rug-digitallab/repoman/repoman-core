rootProject.name = "repoman-core"

pluginManagement {
    plugins {
        val digitalLabGradlePluginVersion: String by settings

        id("nl.rug.digitallab.gradle.plugin.quarkus.project") version digitalLabGradlePluginVersion
        id("nl.rug.digitallab.gradle.plugin.quarkus.library") version digitalLabGradlePluginVersion
    }

    repositories {
        maven("https://gitlab.com/api/v4/groups/65954571/-/packages/maven") // Digital Lab Group Repository
        gradlePluginPortal()
    }
}

include("repositories")
include("common-repoman")
include("worker-gitlab")
