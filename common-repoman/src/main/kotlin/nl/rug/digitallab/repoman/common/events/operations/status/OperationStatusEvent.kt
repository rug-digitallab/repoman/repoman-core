package nl.rug.digitallab.repoman.common.events.operations.status

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.JsonTypeInfo
import nl.rug.digitallab.repoman.common.enums.OperationStatus
import nl.rug.digitallab.repoman.common.events.operations.OperationEvent

/**
 * Event that is sent when the status of a git worker operation is updated.
 *
 * @property operationEvent Contains the actual event that was sent, which responded in this status updated event.
 * @property operationStatus The status of a git worker operation.
 */
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "operationStatus",
)
@JsonSubTypes(
    // Needs to be compile-time constant.
    Type(value = StartedStatusEvent::class, name = "StartedStatusEvent"),
    Type(value = CompletedStatusEvent::class, name = "CompletedStatusEvent"),
    Type(value = FailedStatusEvent::class, name = "FailedStatusEvent"),
)
interface OperationStatusEvent {
    val operationEvent: OperationEvent
    val operationStatus: OperationStatus
}
