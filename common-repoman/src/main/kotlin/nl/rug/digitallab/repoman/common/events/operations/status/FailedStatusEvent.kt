package nl.rug.digitallab.repoman.common.events.operations.status

import nl.rug.digitallab.repoman.common.ErrorId
import nl.rug.digitallab.repoman.common.enums.OperationStatus
import nl.rug.digitallab.repoman.common.events.operations.OperationEvent

/**
 * Event that is sent when the operation of a git worker has failed.
 *
 * @property message The message that describes the failure.
 * @property errorId The id of the error that caused the failure.
 */
data class FailedStatusEvent (
    override val operationEvent: OperationEvent,
    val message: String,
    val errorId: ErrorId? = null,
) : OperationStatusEvent {
    override val operationStatus: OperationStatus = OperationStatus.FAILED
}
