package nl.rug.digitallab.repoman.common.enums

/**
 * The operations that can be performed the Git Worker.
 */
enum class OperationType {
    /**
     * CREATE_COURSE_SERIES - Operation for creating a new course series.
     */
    CREATE_COURSE_SERIES,

    /**
     * CREATE_COURSE_INSTANCE - Operation for creating a new course instance.
     */
    CREATE_COURSE_INSTANCE,

    /**
     * UPDATE_STUDENT_GROUPS - Operation for updating the student groups of a course instance.
     */
    UPDATE_STUDENT_GROUPS,

    /**
     * UPDATE_ENROLLMENTS - Operation for updating the course enrollments of a course instance.
     */
    UPDATE_ENROLLMENTS,
}
