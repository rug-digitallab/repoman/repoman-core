package nl.rug.digitallab.repoman.common.events.constraints

import jakarta.validation.Constraint
import jakarta.validation.Payload
import kotlin.annotation.AnnotationTarget.*
import kotlin.reflect.KClass

/**
 * Constraint for validating a name. Has no validators predefined as the validation logic depends on the platform.
 *
 * @property message The message to return when the constraint is violated.
 * @property groups The groups the constraint belongs to.
 * @property payload The payload associated to the constraint.
 */
@Target(FIELD, PROPERTY, VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Constraint(validatedBy = []) // Should be registered by the platform-specific ValidatorFactoryCustomizer
annotation class GroupName(
    val message: String = "",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = [],
)
