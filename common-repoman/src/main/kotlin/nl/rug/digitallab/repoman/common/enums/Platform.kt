package nl.rug.digitallab.repoman.common.enums

/**
 * The Git platforms that can be used.
 */
enum class Platform {
    /**
     * GITLAB - Platform enum indicating the use of GitLab.
     */
    GITLAB,
}
