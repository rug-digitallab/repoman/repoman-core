package nl.rug.digitallab.repoman.common.events.operations

import jakarta.validation.Valid
import nl.rug.digitallab.repoman.common.OperationId
import nl.rug.digitallab.repoman.common.enums.OperationType
import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.dtos.Enrollments
import nl.rug.digitallab.repoman.common.events.configurations.EnrollmentsConfiguration
import nl.rug.digitallab.repoman.common.events.constraints.GroupSlug

/**
 * Event for updating the enrollments of a course instance.
 *
 * Note: JsonProperty has to be explicitly annotated because of it being an overridden value.
 *
 * @property courseSeriesSlug The slug of the course series on which the update happens.
 * @property courseInstanceSlug The slug of the course instance on which the update happens.
 * @property enrollments All enrollments of the course.
 */
data class UpdateEnrollmentsEvent(
    override val operationId: OperationId,

    override val platform: Platform,

    @field:GroupSlug
    val courseSeriesSlug: String,

    @field:GroupSlug
    val courseInstanceSlug: String,

    @field:Valid
    val enrollments: Enrollments,

    @field:Valid
    val configuration: EnrollmentsConfiguration,
) : OperationEvent {
    override val operationType: OperationType = OperationType.UPDATE_ENROLLMENTS
}
