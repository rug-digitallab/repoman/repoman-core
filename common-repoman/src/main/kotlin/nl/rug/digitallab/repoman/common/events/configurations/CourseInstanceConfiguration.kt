package nl.rug.digitallab.repoman.common.events.configurations

/**
 * All configuration options for a course instance.
 *
 * @property createGeneralIssues Whether the general issues project should be created.
 */
data class CourseInstanceConfiguration(
    val createGeneralIssues: Boolean = true,
)
