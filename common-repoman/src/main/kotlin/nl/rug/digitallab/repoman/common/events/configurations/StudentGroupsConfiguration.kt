package nl.rug.digitallab.repoman.common.events.configurations

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.validation.constraints.AssertTrue
import jakarta.validation.constraints.Min
import jakarta.validation.constraints.NotNull
import nl.rug.digitallab.repoman.common.ProjectId

/**
 * All configuration options for the student groups.
 *
 * @property projectSource The source of the project. This can be a fork, a template, or none in case of a new project.
 * @property projectId The ID of the project. This is only required when the project source is a fork or a template.
 */
data class StudentGroupsConfiguration(
    @field:NotNull
    val projectSource: ProjectSource,

    @field:Min(0)
    val projectId: ProjectId?,
) {
    @JsonIgnore
    @AssertTrue(message = "The projectId should only be set when the project source is FORK or TEMPLATE.")
    fun isValid(): Boolean {
        return when (projectSource) {
            ProjectSource.FORK, ProjectSource.TEMPLATE -> projectId != null
            ProjectSource.NONE -> projectId == null
        }
    }
}
