package nl.rug.digitallab.repoman.common.events.operations.status

import nl.rug.digitallab.repoman.common.enums.OperationStatus
import nl.rug.digitallab.repoman.common.events.operations.OperationEvent

/**
 * Event that is sent when the operation of a git worker has started.
 */
data class StartedStatusEvent (
    override val operationEvent: OperationEvent,
) : OperationStatusEvent {
    override val operationStatus: OperationStatus = OperationStatus.STARTED
}
