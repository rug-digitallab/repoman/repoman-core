package nl.rug.digitallab.repoman.common.events.configurations

/**
 * The [ProjectSource] enum class represents the source of the project. This can be a fork, a template, or none in case
 * of a new project.
 */
enum class ProjectSource {
    NONE,
    FORK,
    TEMPLATE,
}
