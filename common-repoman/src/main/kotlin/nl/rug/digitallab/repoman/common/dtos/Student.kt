package nl.rug.digitallab.repoman.common.dtos

import com.fasterxml.jackson.annotation.JsonIgnore
import nl.rug.digitallab.repoman.common.enums.Role
import nl.rug.digitallab.repoman.common.events.constraints.GroupName
import nl.rug.digitallab.repoman.common.events.constraints.GroupSlug
import nl.rug.digitallab.repoman.common.toSlug

/**
 * A specific enrollment of type Student.
 *
 * @property groupName The group name to which the student belongs. Optional, since students may also audit the course
 * without being part of a group.
 * @property groupSlug The slug representation of the group name.
 */
data class Student(
    override val handle: String,
    override val username: String,

    @field:GroupName
    val groupName: String?,
) : Enrollment {
    @field:JsonIgnore
    override val role = Role.STUDENT

    @field:JsonIgnore
    @field:GroupSlug
    val groupSlug = groupName?.toSlug()
}
