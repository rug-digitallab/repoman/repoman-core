package nl.rug.digitallab.repoman.common.dtos

import com.fasterxml.jackson.annotation.JsonIgnore
import nl.rug.digitallab.repoman.common.enums.Role
import nl.rug.digitallab.repoman.common.events.constraints.GroupName
import nl.rug.digitallab.repoman.common.events.constraints.GroupSlug
import nl.rug.digitallab.repoman.common.toSlug

/**
 * A specific enrollment of type Teacher.
 *
 * @property groupName The student group to which the teacher belongs. Can be null if the teacher is not part of a specific group.
 * @property groupSlug The slug representation of the group name.
 */
data class Teacher(
    override val handle: String,
    override val username: String,

    @field:GroupName
    val groupName: String?,
) : Enrollment {
    @field:JsonIgnore
    override val role = Role.TEACHER

    @field:JsonIgnore
    @field:GroupSlug
    val groupSlug = groupName?.toSlug()
}
