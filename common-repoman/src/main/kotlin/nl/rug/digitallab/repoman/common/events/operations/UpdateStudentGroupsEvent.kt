package nl.rug.digitallab.repoman.common.events.operations

import jakarta.validation.Valid
import nl.rug.digitallab.repoman.common.OperationId
import nl.rug.digitallab.repoman.common.dtos.StudentGroup
import nl.rug.digitallab.repoman.common.enums.OperationType
import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.events.configurations.StudentGroupsConfiguration
import nl.rug.digitallab.repoman.common.events.constraints.GroupSlug

/**
 * Event for updating the student groups of a course instance.
 *
 * Note: JsonProperty has to be explicitly annotated because of it being an overridden value.
 *
 * @property courseSeriesSlug The slug of the course series on which the update happens.
 * @property courseInstanceSlug The slug of the course instance on which the update happens.
 * @property groups All student groups of the course that have to be updated.
 * @property configuration The configuration for the enrollments.
 */
data class UpdateStudentGroupsEvent(
    override val operationId: OperationId,

    override val platform: Platform,

    @field:GroupSlug
    val courseSeriesSlug: String,

    @field:GroupSlug
    val courseInstanceSlug: String,

    @field:Valid
    val groups: List<StudentGroup>,

    @field:Valid
    val configuration: StudentGroupsConfiguration,
) : OperationEvent {
    override val operationType: OperationType = OperationType.UPDATE_STUDENT_GROUPS
}
