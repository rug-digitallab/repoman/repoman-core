package nl.rug.digitallab.repoman.common.dtos

import com.fasterxml.jackson.annotation.JsonIgnore
import nl.rug.digitallab.repoman.common.events.constraints.GroupName
import nl.rug.digitallab.repoman.common.events.constraints.GroupSlug
import nl.rug.digitallab.repoman.common.toSlug

/**
 * Represents a student group.
 *
 * @property groupName The name of the group.
 * @property groupSlug The slug of the group.
 */
data class StudentGroup(
    @field:GroupName
    val groupName: String,
) {
    @field:JsonIgnore
    @field:GroupSlug
    val groupSlug = groupName.toSlug()
}
