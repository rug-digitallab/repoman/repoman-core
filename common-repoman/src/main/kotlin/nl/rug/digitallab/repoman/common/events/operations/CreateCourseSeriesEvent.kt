package nl.rug.digitallab.repoman.common.events.operations

import jakarta.validation.constraints.Min
import nl.rug.digitallab.repoman.common.GroupId
import nl.rug.digitallab.repoman.common.OperationId
import nl.rug.digitallab.repoman.common.enums.OperationType
import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.events.constraints.GroupName
import nl.rug.digitallab.repoman.common.events.constraints.GroupSlug

/**
 * Event for creating a new course series.
 *
 * Note: JsonProperty has to be explicitly annotated because of it being an overridden value.
 *
 * @property courseSeriesName The name of the new course series.
 * @property courseSeriesSlug The slug of the new course series.
 * @property groupId The group id in case there is already a GitLab group for this series, this is optional.
 */
data class CreateCourseSeriesEvent(
    override val operationId: OperationId,

    override val platform: Platform,

    @field:GroupName
    val courseSeriesName: String,

    @field:GroupSlug
    val courseSeriesSlug: String,

    @field:Min(0)
    val groupId: GroupId?,
) : OperationEvent {
    override val operationType: OperationType = OperationType.CREATE_COURSE_SERIES
}
