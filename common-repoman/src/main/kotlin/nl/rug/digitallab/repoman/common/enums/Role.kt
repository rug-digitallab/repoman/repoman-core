package nl.rug.digitallab.repoman.common.enums

/**
 * The different types of enrollments that a course instance can have.
 */
enum class Role {
    /**
     * STUDENT - Student taking the course as part of a group of students.
     */
    STUDENT,

    /**
     * REVIEWER - User that is (code) reviewing the work of student groups.
     */
    REVIEWER,

    /**
     * TEACHER - Staff member involved in teaching the course, optionally responsible for a specific group of students.
     */
    TEACHER,

    /**
     * MAINTAINER - Staff member involved in maintaining the course, responsible for the course as a whole.
     */
    MAINTAINER,
}
