package nl.rug.digitallab.repoman.common

import java.util.UUID

typealias OperationId = UUID
typealias ErrorId = UUID
typealias GroupId = Long
typealias ProjectId = Long
