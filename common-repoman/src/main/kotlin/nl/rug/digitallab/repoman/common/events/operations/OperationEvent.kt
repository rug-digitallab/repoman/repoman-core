package nl.rug.digitallab.repoman.common.events.operations

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.JsonTypeInfo
import nl.rug.digitallab.repoman.common.OperationId
import nl.rug.digitallab.repoman.common.enums.OperationType
import nl.rug.digitallab.repoman.common.enums.Platform


/**
 * Base operation event interface with common properties.
 *
 * @property operationId The operation id which can be used to trace the event.
 * @property platform The platform on which the operation should be executed, corresponds to a worker.
 * @property operationType The type of operation to be executed.
 *
 * Note: the property name has to be compile-time constant therefore it is hardcoded.
 */
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "operationType",
)
@JsonSubTypes(
    // Needs to be compile-time constant.
    Type(value = CreateCourseSeriesEvent::class, name = "CreateCourseSeriesEvent"),
    Type(value = CreateCourseInstanceEvent::class, name = "CreateCourseInstanceEvent"),
    Type(value = UpdateStudentGroupsEvent::class, name = "UpdateStudentGroupsEvent"),
    Type(value = UpdateEnrollmentsEvent::class, name = "UpdateEnrollmentsEvent"),
)
interface OperationEvent {
    val operationId: OperationId
    val operationType: OperationType
    val platform: Platform
}
