package nl.rug.digitallab.repoman.common.dtos

import com.fasterxml.jackson.annotation.JsonIgnore
import nl.rug.digitallab.repoman.common.enums.Role
import nl.rug.digitallab.repoman.common.events.constraints.GroupName
import nl.rug.digitallab.repoman.common.events.constraints.GroupSlug
import nl.rug.digitallab.repoman.common.toSlug

/**
 * A specific enrollment of type Reviewer.
 *
 * @property groupName The group name to which the reviewer belongs. Mandatory, since reviewers must be part of a group.
 * @property groupSlug The slug representation of the group name.
 */
data class Reviewer(
    override val handle: String,
    override val username: String,

    @field:GroupName
    val groupName: String,
) : Enrollment {
    @field:JsonIgnore
    override val role = Role.REVIEWER

    @field:JsonIgnore
    @field:GroupSlug
    val groupSlug = groupName.toSlug()
}
