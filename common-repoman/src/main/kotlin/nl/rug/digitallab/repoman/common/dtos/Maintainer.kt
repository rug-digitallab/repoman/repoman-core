package nl.rug.digitallab.repoman.common.dtos

import com.fasterxml.jackson.annotation.JsonIgnore
import nl.rug.digitallab.repoman.common.enums.Role

/**
 * A specific enrollment of type Maintainer.
 */
data class Maintainer(
    override val handle: String,
    override val username: String,
) : Enrollment {
    @field:JsonIgnore
    override val role = Role.MAINTAINER
}
