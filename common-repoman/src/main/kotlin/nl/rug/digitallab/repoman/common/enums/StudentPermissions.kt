package nl.rug.digitallab.repoman.common.enums

enum class StudentPermissions {
    /**
     * Students have limited permissions in their group.
     */
    LIMITED,

    /**
     * Students have full permissions in their group.
     */
    FULL,
}
