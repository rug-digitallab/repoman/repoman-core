package nl.rug.digitallab.repoman.common.enums

/**
 * The status of a Git Worker operation.
 */
enum class OperationStatus {
    /**
     * STARTED - Indicates that the Git Worker has received the operation and has started handling it.
     */
    STARTED,

    /**
     * COMPLETED - Indicates that the Git Worker has completely handled the operation.
     */
    COMPLETED,

    /**
     * FAILED - Indicated that the Git Worker was unable to process the operation.
     */
    FAILED,
}
