package nl.rug.digitallab.repoman.common


/**
 * Helper function to convert a string to a slug.
 *
 * @return The string as a slug.
 */
fun String.toSlug(): String = this.replace(" ", "-").lowercase()
