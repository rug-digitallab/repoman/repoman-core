package nl.rug.digitallab.repoman.common.dtos

import jakarta.validation.Valid
import nl.rug.digitallab.repoman.common.events.constraints.NoDuplicates

/**
 * Represents all users that are enrolled in a course instance.
 *
 * @property students All students enrolled in the course, includes their group.
 * @property reviewers All student reviewers enrolled in the course, includes the group they review.
 * @property teachers All teachers enrolled in the course, optionally includes a group they are responsible for.
 * @property maintainers All maintainers enrolled in the course.
 */
data class Enrollments(
    @field:Valid
    @field:NoDuplicates
    val students: List<Student>,
    @field:Valid
    @field:NoDuplicates
    val reviewers: List<Reviewer>,
    @field:Valid
    @field:NoDuplicates
    val teachers: List<Teacher>,
    @field:Valid
    @field:NoDuplicates
    val maintainers: List<Maintainer>,
)
