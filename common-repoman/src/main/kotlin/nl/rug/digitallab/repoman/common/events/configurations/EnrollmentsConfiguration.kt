package nl.rug.digitallab.repoman.common.events.configurations

import jakarta.validation.constraints.NotNull
import nl.rug.digitallab.repoman.common.enums.StudentPermissions

/**
 * All configuration options for the enrollments.
 *
 * @property failOnInvalidHandles Whether to fail if the handles are invalid.
 * @property studentPermissions The permissions that students have in their student group.
 */
data class EnrollmentsConfiguration(
    val failOnInvalidHandles: Boolean = true,

    @field:NotNull
    val studentPermissions: StudentPermissions = StudentPermissions.FULL,
)
