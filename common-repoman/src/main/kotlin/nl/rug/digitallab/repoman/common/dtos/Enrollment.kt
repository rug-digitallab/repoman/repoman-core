package nl.rug.digitallab.repoman.common.dtos

import nl.rug.digitallab.repoman.common.enums.Role

/**
 * Interface that has the basic information for each course enrollment.
 *
 * @property role The role of the user.
 * @property handle The user's handle on the given SCM platform.
 * @property username The user's tenant username.
 */
interface Enrollment {
    val role: Role
    val handle: String
    val username: String
}
