package nl.rug.digitallab.repoman.common.events.operations

import jakarta.validation.Valid
import nl.rug.digitallab.repoman.common.OperationId
import nl.rug.digitallab.repoman.common.enums.OperationType
import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.events.configurations.CourseInstanceConfiguration
import nl.rug.digitallab.repoman.common.events.constraints.GroupName
import nl.rug.digitallab.repoman.common.events.constraints.GroupSlug

/**
 * Event for creating a new course instance.
 *
 * Note: JsonProperty has to be explicitly annotated because of it being an overridden value.
 *
 * @property courseSeriesSlug The slug of the course series to which the new course instance should belong.
 * @property courseInstanceName The name of the new course instance.
 * @property courseInstanceSlug The slug of the new course instance.
 */
data class CreateCourseInstanceEvent(
    override var operationId: OperationId,

    override var platform: Platform,

    @field:GroupSlug
    val courseSeriesSlug: String,

    @field:GroupName
    val courseInstanceName: String,

    @field:GroupSlug
    val courseInstanceSlug: String,

    @field:Valid
    val configuration: CourseInstanceConfiguration,
) : OperationEvent {
    override val operationType: OperationType = OperationType.CREATE_COURSE_INSTANCE
}
