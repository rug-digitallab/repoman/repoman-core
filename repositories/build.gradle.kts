plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.project")
}

dependencies {
    val commonQuarkusVersion: String by project

    implementation(project(":common-repoman"))

    implementation("io.quarkus:quarkus-observability-devservices-lgtm")
    // REST
    implementation("io.quarkus:quarkus-rest")
    implementation("io.quarkus:quarkus-rest-jackson")
    implementation("nl.rug.digitallab.common.quarkus:jackson:$commonQuarkusVersion")
    implementation("io.quarkus:quarkus-smallrye-openapi")
    implementation("nl.rug.digitallab.common.quarkus:exception-mapper-rest:$commonQuarkusVersion")
    implementation("io.quarkus:quarkus-hibernate-validator")
    implementation("io.quarkus:quarkus-messaging-kafka")
    // Jackson
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-csv")

    testImplementation("io.quarkus:quarkus-test-kafka-companion")
    testImplementation("io.quarkus:quarkus-test-vertx")
    testImplementation("io.rest-assured:rest-assured")
    testImplementation("io.rest-assured:kotlin-extensions")
    testImplementation("io.smallrye.reactive:smallrye-reactive-messaging-in-memory")
    testImplementation("nl.rug.digitallab.common.quarkus:test-rest:$commonQuarkusVersion")
}
