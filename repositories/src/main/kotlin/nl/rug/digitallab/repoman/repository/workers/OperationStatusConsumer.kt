package nl.rug.digitallab.repoman.repository.workers

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.common.events.operations.status.*
import org.eclipse.microprofile.reactive.messaging.Incoming
import org.jboss.logging.Logger


/**
 * The [OperationStatusConsumer] consumes operation status updates.
 */
@ApplicationScoped
class OperationStatusConsumer {
    @Inject
    private lateinit var log: Logger

    /**
     * Handles incoming operation status updates.
     *
     * @param operationStatusEvent The operation status update.
     */
    @Incoming("worker-status")
    fun handleEvent(operationStatusEvent: OperationStatusEvent) {
        // WebSocket communication should be implemented here
        when (operationStatusEvent) {
            is StartedStatusEvent, is CompletedStatusEvent ->
                log.info("Operation ${operationStatusEvent.operationEvent.operationId} -> ${operationStatusEvent.operationStatus}")
            is FailedStatusEvent ->
                log.error("Operation ${operationStatusEvent.operationEvent.operationId} -> ${operationStatusEvent.operationStatus}: ${operationStatusEvent.errorId ?: "No error ID"}")
            else ->
                log.warn("Unknown operation status event: $operationStatusEvent")
        }
    }
}
