package nl.rug.digitallab.repoman.repository.exceptions

/**
 * The [YamlParsingException] is the exception for parsing YAML files.
 */
class YamlParsingException(message: String, override val cause: Throwable? = null) :
    ParsingException(message, cause)
