package nl.rug.digitallab.repoman.repository.workers

import io.opentelemetry.instrumentation.annotations.WithSpan
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.replaceWithUnit
import io.smallrye.reactive.messaging.MutinyEmitter
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.common.events.operations.OperationEvent
import nl.rug.digitallab.repoman.common.enums.Platform
import org.eclipse.microprofile.reactive.messaging.Channel
import org.jboss.logging.Logger

/**
 * The [OperationProducer] sends operation events to a given platform channel.
 */
@ApplicationScoped
class OperationProducer {
    @Inject
    @Channel("worker-gitlab-operation")
    private lateinit var gitlabOperationEmitter: MutinyEmitter<OperationEvent>

    @Inject
    private lateinit var log: Logger

    /**
     * Queues an event on the appropriate platform channel. It is routed to the correct emitter based on the platform.
     *
     * @param event The event to queue.
     *
     * @return A [Uni] that completes when the event is queued.
     */
    @WithSpan
    fun queueEvent(event: OperationEvent): Uni<Unit> {
        val emitter = getPlatformEmitter(event.platform)

        log.debug("Sending ${event.operationType} event on queue with id ${event.operationId} for platform ${event.platform}")

        return emitter
            .send(event)
            .invoke { _ -> log.info("Sent ${event.operationType} event on queue with id ${event.operationId} for platform ${event.platform}") }
            .replaceWithUnit()
            .onFailure().invoke { e -> log.error("Failed to send ${event.operationType} event on queue with id ${event.operationId} for platform ${event.platform}", e) }
    }

    /**
     * Retrieves the emitter for the given platform.
     *
     * @param platform The platform to retrieve the emitter for.
     *
     * @return The emitter for the given platform.
     */
    private fun getPlatformEmitter(platform: Platform): MutinyEmitter<OperationEvent> =
        when (platform) {
            Platform.GITLAB -> gitlabOperationEmitter
        }
}
