package nl.rug.digitallab.repoman.repository.dtos

import nl.rug.digitallab.repoman.common.GroupId
import nl.rug.digitallab.repoman.common.enums.Platform

/**
 * The [AddCourseSeriesGroupRequest] is the request DTO for the creation of a new course series group.
 *
 * @property platform The Git platform to be used.
 * @property name The name of the course series.
 * @property slug The slug of the course series.
 * @property groupId The group id of the course series. If null, a new group will be created.
 */
data class AddCourseSeriesGroupRequest(
    val platform: Platform,
    val name: String,
    val slug: String,
    val groupId: GroupId?,
)
