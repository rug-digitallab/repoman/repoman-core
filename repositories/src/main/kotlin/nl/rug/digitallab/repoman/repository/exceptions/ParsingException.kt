package nl.rug.digitallab.repoman.repository.exceptions

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import org.jboss.resteasy.reactive.RestResponse.Status

/**
 * The [ParsingException] is the base exception for parsing exceptions.
 *
 * @param message The message of the exception.
 * @param cause The cause of the exception.
 */
open class ParsingException(message: String, override val cause: Throwable? = null) :
    MappedException(message, Status.BAD_REQUEST, cause)
