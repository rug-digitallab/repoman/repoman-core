package nl.rug.digitallab.repoman.repository.managers

import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.common.OperationId
import nl.rug.digitallab.repoman.common.dtos.Enrollments
import nl.rug.digitallab.repoman.common.dtos.StudentGroup
import nl.rug.digitallab.repoman.common.events.configurations.EnrollmentsConfiguration
import nl.rug.digitallab.repoman.common.events.configurations.StudentGroupsConfiguration
import nl.rug.digitallab.repoman.common.events.operations.CreateCourseInstanceEvent
import nl.rug.digitallab.repoman.common.events.operations.CreateCourseSeriesEvent
import nl.rug.digitallab.repoman.common.events.operations.UpdateEnrollmentsEvent
import nl.rug.digitallab.repoman.common.events.operations.UpdateStudentGroupsEvent
import nl.rug.digitallab.repoman.repository.configurations.Service
import nl.rug.digitallab.repoman.repository.parsing.csv.CsvParser
import nl.rug.digitallab.repoman.repository.dtos.*
import nl.rug.digitallab.repoman.repository.workers.OperationProducer
import nl.rug.digitallab.repoman.repository.parsing.yaml.YamlParser

@ApplicationScoped
class RepositoriesManager {
    @Inject
    private lateinit var producer: OperationProducer

    @Inject
    private lateinit var csvParser: CsvParser

    @Inject
    private lateinit var yamlParser: YamlParser

    /**
     * Add a new course series group to the specified platform.
     *
     * @param request The request DTO.
     *
     * @return A [Uni] that completes when the create course series operation is queued.
     */
    fun addCourseSeriesGroup(request: AddCourseSeriesGroupRequest): Uni<Unit> {
        val event = CreateCourseSeriesEvent(
            operationId = OperationId.randomUUID(),
            platform = request.platform,
            courseSeriesName = request.name,
            courseSeriesSlug = request.slug,
            groupId = request.groupId,
        )

        return producer
            .queueEvent(event)
    }

    /**
     * Add a new course instance group to the specified platform.
     *
     * @param request The request DTO.
     *
     * @return A [Uni] that completes when the create course instance operation is queued.
     */
    fun addCourseInstanceGroup(
        courseSeriesSlug: String,
        request: AddCourseInstanceGroupRequest,
    ): Uni<Unit> {
        val event = CreateCourseInstanceEvent(
            operationId = OperationId.randomUUID(),
            platform = request.platform,
            courseSeriesSlug = courseSeriesSlug,
            courseInstanceName = request.name,
            courseInstanceSlug = request.slug,
            configuration = request.configuration,
        )

        return producer
            .queueEvent(event)
    }

    /**
     * Update the student groups of a course instance.
     *
     * @param request The request DTO.
     *
     * @return A [Uni] that completes when the update student groups operation is queued.
     */
    fun updateStudentGroups(
        courseSeriesSlug: String,
        courseInstanceSlug: String,
        request: UpdateStudentGroupsRequest,
    ): Uni<Unit> {
        val event = UpdateStudentGroupsEvent(
            operationId = OperationId.randomUUID(),
            platform = request.platform,
            courseSeriesSlug = courseSeriesSlug,
            courseInstanceSlug = courseInstanceSlug,
            groups = request.groups.map { StudentGroup(it) },
            configuration = request.configuration,
        )

        return producer
            .queueEvent(event)
    }

    /**
     * Update the course instance enrollments from a CSV file.
     *
     * @param request The request DTO.
     *
     * @return A [Uni] that completes when the update course instance enrollments operation is queued.
     */
    fun updateStudentGroupsFromCsv(request: UpdateStudentGroupsCsvRequest): Uni<Unit> {
        return uni { csvParser.parse(request.groupsCsv.uploadedFile()) }
            .flatMap { csv ->
                updateStudentGroups(
                    request.courseSeriesSlug,
                    request.courseInstanceSlug,
                    UpdateStudentGroupsRequest(
                        platform = request.platform,
                        groups = csv.toStudentGroups(),
                        configuration = request.configuration,
                    ),
                )
            }
    }

    /**
     * Update the course instance enrollments.
     *
     * @param request The request DTO.
     *
     * @return A [Uni] that completes when the update course instance enrollments operation is queued.
     */
    fun updateEnrollments(
        courseSeriesSlug: String,
        courseInstanceSlug: String,
        request: UpdateEnrollmentsRequest,
    ): Uni<Unit> {
        val event = UpdateEnrollmentsEvent(
            operationId = OperationId.randomUUID(),
            platform = request.platform,
            courseSeriesSlug = courseSeriesSlug,
            courseInstanceSlug = courseInstanceSlug,
            enrollments = request.enrollments,
            configuration = request.configuration,
        )

        return producer
            .queueEvent(event)
    }

    /**
     * Update the course instance enrollments from a CSV file.
     *
     * @param request The request DTO.
     *
     * @return A [Uni] that completes when the update course instance enrollments operation is queued.
     */
    fun updateEnrollmentsFromCsv(request: UpdateEnrollmentsCsvRequest): Uni<Unit> {
        return uni { csvParser.parse(request.enrollmentsCsv.uploadedFile()) }
            .flatMap {
                updateEnrollments(
                    request.courseSeriesSlug,
                    request.courseInstanceSlug,
                    UpdateEnrollmentsRequest(
                        platform = request.platform,
                        enrollments = it,
                        configuration = request.configuration,
                    ),
                )
            }
    }

    fun applyConfiguration(request: ApplyConfigurationRequest): Uni<Unit> {
        return uni {
            yamlParser.parseYaml(request.configYaml.uploadedFile()).also {
                check(it.services.contains(Service.REPOSITORY_MANAGEMENT)) {
                    "The configuration does not enable the repository management service."
                }
            }
        }.flatMap { configuration ->
            updateStudentGroupsFromCsv(
                request.toUpdateStudentGroupsCsvRequest(configuration.repositoryManagement.groups)
            ).flatMap {
                updateEnrollmentsFromCsv(
                    request.toUpdateEnrollmentsCsvRequest(configuration.repositoryManagement.enrollments)
                )
            }
        }
    }
}

/**
 * Convert a [Enrollments] to a list of unique [StudentGroup]s.
 *
 * @return A list of unique [StudentGroup]s.
 */
private fun Enrollments.toStudentGroups(): Set<String> =
    // Collect unique groups from students and teacher enrollments
    (students.mapNotNull { it.groupName } + teachers.mapNotNull { it.groupName } + reviewers.map { it.groupName })
        .toSet()

/**
 * Convert an [ApplyConfigurationRequest] to an [UpdateStudentGroupsCsvRequest] using the provided
 * [StudentGroupsConfiguration].
 *
 * @param configuration The student groups configuration.
 *
 * @return The [UpdateStudentGroupsCsvRequest].
 */
private fun ApplyConfigurationRequest.toUpdateStudentGroupsCsvRequest(configuration: StudentGroupsConfiguration): UpdateStudentGroupsCsvRequest =
    UpdateStudentGroupsCsvRequest(
        courseSeriesSlug = courseSeriesSlug,
        courseInstanceSlug = courseInstanceSlug,
        platform = platform,
        groupsCsv = enrollmentsCsv,
        configuration = configuration,
    )

/**
 * Convert an [ApplyConfigurationRequest] to an [UpdateEnrollmentsCsvRequest] using the provided
 * [EnrollmentsConfiguration].
 *
 * @param configuration The enrollments configuration.
 *
 * @return The [UpdateEnrollmentsCsvRequest].
 */
private fun ApplyConfigurationRequest.toUpdateEnrollmentsCsvRequest(configuration: EnrollmentsConfiguration): UpdateEnrollmentsCsvRequest =
    UpdateEnrollmentsCsvRequest(
        courseSeriesSlug = courseSeriesSlug,
        courseInstanceSlug = courseInstanceSlug,
        platform = platform,
        enrollmentsCsv = enrollmentsCsv,
        configuration = configuration,
    )
