package nl.rug.digitallab.repoman.repository

import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import jakarta.validation.Valid
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import nl.rug.digitallab.repoman.repository.dtos.*
import nl.rug.digitallab.repoman.repository.managers.RepositoriesManager
import org.jboss.resteasy.reactive.ResponseStatus
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.RestResponse.StatusCode

/**
 * The [RepositoriesResource] is responsible for handling all REST requests related to repositories.
 */
@Path("/api/v1/repositories")
class RepositoriesResource {
    @Inject
    lateinit var repositoriesManager: RepositoriesManager

    /**
     * Creates a new course series
     *
     * @param addCourseSeriesGroupRequest The DTO containing the details of the new course series.
     *
     * @return a [Unit] response.
     */
    @POST
    @Path("courses/series")
    @ResponseStatus(StatusCode.ACCEPTED)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun addCourseSeriesGroup(
        addCourseSeriesGroupRequest: AddCourseSeriesGroupRequest,
    ): Uni<Unit> {
        return repositoriesManager
            .addCourseSeriesGroup(addCourseSeriesGroupRequest)
    }

    /**
     * Creates a new course instance for a specific course series.
     *
     * @param courseSeriesSlug The slug of the course series.
     * @param addCourseInstanceGroupRequest The DTO containing the details of the new course instance.
     *
     * @return a [Unit] response.
     */
    @POST
    @Path("courses/series/{courseSeriesSlug}/instances")
    @ResponseStatus(StatusCode.ACCEPTED)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun addCourseInstanceGroup(
        @RestPath courseSeriesSlug: String,
        addCourseInstanceGroupRequest: AddCourseInstanceGroupRequest,
    ): Uni<Unit> {
        return repositoriesManager
            .addCourseInstanceGroup(courseSeriesSlug, addCourseInstanceGroupRequest)
    }

    /**
     * Update the student groups of a course instance.
     *
     * @param courseSeriesSlug The slug of the course series.
     * @param courseInstanceSlug The slug of the course instance.
     * @param updateStudentGroupsRequest The DTO containing the updated student groups.
     *
     * @return a [Unit] response.
     */
    @POST
    @Path("courses/series/{courseSeriesSlug}/instances/{courseInstanceSlug}/groups")
    @ResponseStatus(StatusCode.ACCEPTED)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun updateStudentGroups(
        @RestPath courseSeriesSlug: String,
        @RestPath courseInstanceSlug: String,
        @Valid updateStudentGroupsRequest: UpdateStudentGroupsRequest,
    ): Uni<Unit> {
        return repositoriesManager
            .updateStudentGroups(courseSeriesSlug, courseInstanceSlug, updateStudentGroupsRequest)
    }

    /**
     * Update the student groups of a course instance from a CSV file.
     *
     * @param updateStudentGroupsCsvRequest The DTO containing the updated student groups.
     *
     * @return a [Unit] response.
     */
    @POST
    @Path("courses/series/{courseSeriesSlug}/instances/{courseInstanceSlug}/groups/csv")
    @ResponseStatus(StatusCode.ACCEPTED)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    fun updateStudentGroupsFromCsv(
        @BeanParam @Valid updateStudentGroupsCsvRequest: UpdateStudentGroupsCsvRequest,
    ): Uni<Unit> {
        return repositoriesManager
            .updateStudentGroupsFromCsv(updateStudentGroupsCsvRequest)
    }

    /**
     * Update the enrollments of a course instance.
     *
     * @param courseSeriesSlug The slug of the course series.
     * @param courseInstanceSlug The slug of the course instance.
     * @param updateEnrollmentsRequest The DTO containing the enrollments.
     *
     * @return a [Unit] response.
     */
    @POST
    @Path("courses/series/{courseSeriesSlug}/instances/{courseInstanceSlug}/enrollments")
    @ResponseStatus(StatusCode.ACCEPTED)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun updateEnrollments(
        @RestPath courseSeriesSlug: String,
        @RestPath courseInstanceSlug: String,
        @Valid updateEnrollmentsRequest: UpdateEnrollmentsRequest,
    ): Uni<Unit> {
        return repositoriesManager
            .updateEnrollments(courseSeriesSlug, courseInstanceSlug, updateEnrollmentsRequest)
    }

    /**
     * Update the enrollments of a course instance from a CSV file.
     *
     * @param updateEnrollmentsCsvRequest The DTO containing the enrollments.
     *
     * @return a [Unit] response.
     */
    @POST
    @Path("courses/series/{courseSeriesSlug}/instances/{courseInstanceSlug}/enrollments/csv")
    @ResponseStatus(StatusCode.ACCEPTED)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    fun updateEnrollmentsFromCsv(
        @BeanParam @Valid updateEnrollmentsCsvRequest: UpdateEnrollmentsCsvRequest,
    ): Uni<Unit> {
        return repositoriesManager
            .updateEnrollmentsFromCsv(updateEnrollmentsCsvRequest)
    }

    /**
     * Apply a configuration to a course instance.
     *
     * @param applyConfigurationRequest The DTO containing the configuration to apply.
     */
    @POST
    @Path("courses/series/{courseSeriesSlug}/instances/{courseInstanceSlug}/config")
    @ResponseStatus(StatusCode.ACCEPTED)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    fun applyConfiguration(
        @BeanParam @Valid applyConfigurationRequest: ApplyConfigurationRequest,
    ): Uni<Unit> {
        return repositoriesManager
            .applyConfiguration(applyConfigurationRequest)
    }
}
