package nl.rug.digitallab.repoman.repository.exceptions

/**
 * The [CsvParsingException] is the exception for parsing CSV files.
 */
class CsvParsingException(message: String, override val cause: Throwable? = null) :
    ParsingException(message, cause)
