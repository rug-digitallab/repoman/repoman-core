package nl.rug.digitallab.repoman.repository.configurations

import jakarta.validation.Valid

/**
 * The [Configuration] data class represents the config.yml file present in the Configuration repository of the
 * course instance.
 *
 * @property services The list of services to be used.
 * @property repositoryManagement The [RepoManConfiguration] configuration.
 */
data class Configuration(
    val services: List<Service>,

    @field:Valid
    val repositoryManagement: RepoManConfiguration,
)
