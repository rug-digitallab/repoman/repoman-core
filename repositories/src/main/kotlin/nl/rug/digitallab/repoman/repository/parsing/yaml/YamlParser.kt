package nl.rug.digitallab.repoman.repository.parsing.yaml

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.validation.Validator
import nl.rug.digitallab.repoman.repository.configurations.Configuration
import nl.rug.digitallab.repoman.repository.exceptions.YamlParsingException
import java.nio.file.Path

/**
 * Parses a YAML file into a [Configuration] object.
 */
@ApplicationScoped
class YamlParser {
    @Inject
    private lateinit var yamlMapper: YAMLMapper

    @Inject
    private lateinit var validator: Validator

    /**
     * Parses a YAML file into a [Configuration] object.
     *
     * @param yaml The path to the YAML file.
     *
     * @return The parsed configuration.
     *
     * @throws IllegalArgumentException If the configuration is invalid.
     */
    fun parseYaml(yaml: Path): Configuration {
        try {
            val configuration = yamlMapper.readValue(yaml.toFile(), Configuration::class.java)

            validator.validate(configuration).also { violations ->
                check(violations.isEmpty()) {
                    violations.joinToString(separator = "\n") { it.message }
                }
            }

            return configuration
        } catch (e: Exception) {
            throw YamlParsingException("Unable to parse the provided YAML", e)
        }
    }
}
