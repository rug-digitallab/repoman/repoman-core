package nl.rug.digitallab.repoman.repository.configurations

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * The [Service] enum class represents the services that can be enabled in the configuration.
 */
enum class Service {
    @JsonProperty("repositoryManagement")
    REPOSITORY_MANAGEMENT
}
