package nl.rug.digitallab.repoman.repository.parsing.csv

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.dataformat.csv.CsvMapper
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.common.dtos.*
import nl.rug.digitallab.repoman.common.enums.Role
import nl.rug.digitallab.repoman.repository.exceptions.CsvParsingException
import java.nio.file.Files
import java.nio.file.Path

@ApplicationScoped
class CsvParser {
    @Inject
    private lateinit var csvMapper: CsvMapper

    /**
     * Parses the provided CSV file into a [Enrollments] object.
     *
     * @param csvPath The path to the CSV file to be parsed.
     *
     * @return The parsed [Enrollments] object.
     */
    fun parse(csvPath: Path): Enrollments {
        val enrollmentRows = parseRows(csvPath)
        return toEnrollments(enrollmentRows)
    }

    /**
     * Parses the rows in the CSV file into a list of [EnrollmentRow] objects.
     *
     * @param csvPath The path to the CSV file to be parsed.
     *
     * @return The list of [EnrollmentRow] objects.
     *
     * @throws CsvParsingException If the CSV file cannot be parsed.
     */
    private fun parseRows(csvPath: Path): List<EnrollmentRow> {
        val schema = csvMapper.schemaFor(EnrollmentRow::class.java)
            .withHeader()
            .withColumnReordering(true)

        val rowsInFile = try {
            Files.newBufferedReader(csvPath).use { reader ->
                csvMapper.readerFor(EnrollmentRow::class.java)
                    .with(schema)
                    .readValues<EnrollmentRow>(reader)
                    .readAll()
            }
        } catch (e: Exception) {
            throw CsvParsingException("Unable to parse the provided CSV", e)
        }

        return rowsInFile
    }

    /**
     * Converts the rows in the CSV file into a [Enrollments] object. This method also validates the
     * required fields in the CSV file.
     *
     * @param enrollmentRows The list of [EnrollmentRow] objects.
     *
     * @return The [Enrollments] object.
     */
    private fun toEnrollments(enrollmentRows: List<EnrollmentRow>): Enrollments {
        val students = mutableListOf<Student>()
        val reviewers = mutableListOf<Reviewer>()
        val teachers = mutableListOf<Teacher>()
        val maintainers = mutableListOf<Maintainer>()

        for (row in enrollmentRows) {
            // Username, handle and role are required fields.
            if (row.username.isBlank() || row.handle.isBlank() || row.role.isBlank()) {
                throw CsvParsingException("CSV format is correct but one or more required fields are empty, row: $row")
            }

            val role = try {
                Role.valueOf(row.role.uppercase())
            } catch (e: IllegalArgumentException) {
                throw CsvParsingException("CSV format is correct but uses incorrect roles: $row", e)
            }

            // Check if the group name is empty for maintainers, as maintainers are not associated with a group.
            if(role == Role.MAINTAINER && row.groupName.isNotBlank()) {
                throw CsvParsingException("CSV format is correct but maintainers can access all groups, therefore they " +
                        "should not be associated with a group, row: $row")
            }

            when (role) {
                Role.STUDENT -> students.add(
                    Student(
                        row.handle,
                        row.username,
                        row.groupName.ifBlank { null },
                    )
                )

                Role.REVIEWER -> reviewers.add(
                    Reviewer(
                        row.handle,
                        row.username,
                        row.groupName,
                    )
                )

                Role.TEACHER -> teachers.add(
                    Teacher(
                        row.handle,
                        row.username,
                        row.groupName.ifBlank { null },
                    )
                )

                Role.MAINTAINER -> maintainers.add(
                    Maintainer(
                        row.handle,
                        row.username,
                    )
                )
            }
        }

        return Enrollments(
            students,
            reviewers,
            teachers,
            maintainers,
        )
    }

    /**
     * The [EnrollmentRow] represents a row in the CSV file. This is the expected format of the CSV file.
     */
    private data class EnrollmentRow(
        @JsonProperty("Group.Name")
        val groupName: String,
        @JsonProperty("Username")
        val username: String,
        @JsonProperty("Handle")
        val handle: String,
        @JsonProperty("Role")
        val role: String,
    )
}
