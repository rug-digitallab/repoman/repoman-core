package nl.rug.digitallab.repoman.repository.parsing.csv

import com.fasterxml.jackson.dataformat.csv.CsvFactoryBuilder
import com.fasterxml.jackson.dataformat.csv.CsvParser
import io.quarkus.arc.Unremovable
import jakarta.enterprise.context.Dependent
import nl.rug.digitallab.common.quarkus.jackson.customizers.BuilderCustomizer

/**
 * Customizes the [CsvFactoryBuilder] to enable specific features used by the repositories service.
 */
@Dependent
@Unremovable
class CsvMapperCustomizer: BuilderCustomizer<CsvFactoryBuilder> {
    override fun customize(builder: CsvFactoryBuilder) {
        builder.enable(CsvParser.Feature.IGNORE_TRAILING_UNMAPPABLE)
        builder.enable(CsvParser.Feature.TRIM_SPACES)
        builder.enable(CsvParser.Feature.ALLOW_TRAILING_COMMA)
    }
}

