package nl.rug.digitallab.repoman.repository.dtos

import jakarta.validation.constraints.NotNull
import jakarta.ws.rs.core.MediaType
import nl.rug.digitallab.common.kotlin.helpers.noarg.NoArgConstructor
import nl.rug.digitallab.repoman.common.events.configurations.EnrollmentsConfiguration
import nl.rug.digitallab.repoman.common.enums.Platform
import org.jboss.resteasy.reactive.PartType
import org.jboss.resteasy.reactive.RestForm
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.multipart.FileUpload

/**
 * The [UpdateEnrollmentsCsvRequest] is the request DTO for updating the enrollments of a course instance.
 *
 * @property courseSeriesSlug The slug of the course series.
 * @property courseInstanceSlug The slug of the course instance.
 * @property platform The Git platform to be used.
 * @property enrollmentsCsv The enrollments for the course instance.
 * @property configuration The configuration for the enrollments.
 */
@NoArgConstructor
data class UpdateEnrollmentsCsvRequest(
    @field:RestPath
    var courseSeriesSlug: String,

    @field:RestPath
    var courseInstanceSlug: String,

    @field:RestForm
    @field:NotNull
    var platform: Platform,

    @field:RestForm
    @field:NotNull
    var enrollmentsCsv: FileUpload,

    @field:RestForm
    @field:PartType(MediaType.APPLICATION_JSON)
    @field:NotNull
    var configuration: EnrollmentsConfiguration,
)
