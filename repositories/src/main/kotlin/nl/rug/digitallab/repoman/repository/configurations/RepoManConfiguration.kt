package nl.rug.digitallab.repoman.repository.configurations

import jakarta.validation.Valid
import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.events.configurations.EnrollmentsConfiguration
import nl.rug.digitallab.repoman.common.events.configurations.StudentGroupsConfiguration

/**
 * The [RepoManConfiguration] is the configuration for the repository manager.
 *
 * @property platform The Git platform to be used.
 * @property groups The configuration for the student groups.
 * @property enrollments The configuration for the enrollments.
 */
data class RepoManConfiguration(
    val platform: Platform,
    @field:Valid val groups: StudentGroupsConfiguration,
    @field:Valid val enrollments: EnrollmentsConfiguration,
)
