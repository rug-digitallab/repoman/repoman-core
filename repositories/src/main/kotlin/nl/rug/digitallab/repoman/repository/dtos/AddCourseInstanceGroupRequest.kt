package nl.rug.digitallab.repoman.repository.dtos

import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.events.configurations.CourseInstanceConfiguration

/**
 * The [AddCourseInstanceGroupRequest] is the request DTO for the creation of a new course instance group.
 *
 * @property platform The Git platform to be used.
 * @property name The name of the course instance.
 * @property slug The slug of the course instance.

 */
data class AddCourseInstanceGroupRequest(
    val platform: Platform,
    val name: String,
    val slug: String,
    val configuration: CourseInstanceConfiguration,
)
