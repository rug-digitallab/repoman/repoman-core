package nl.rug.digitallab.repoman.repository.dtos

import jakarta.validation.constraints.NotNull
import nl.rug.digitallab.common.kotlin.helpers.noarg.NoArgConstructor
import nl.rug.digitallab.repoman.common.enums.Platform
import org.jboss.resteasy.reactive.RestForm
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.multipart.FileUpload

/**
 * The [ApplyConfigurationRequest] is the request DTO for applying a configuration to a course instance. The
 * configuration originates from a YAML file in the artifact repository of the course instance. This request is a
 * combination of the students groups and enrollments requests.
 *
 * @property courseSeriesSlug The slug of the course series.
 * @property courseInstanceSlug The slug of the course instance.
 * @property platform The Git platform to be used.
 * @property configYaml The configuration YAML file.
 * @property enrollmentsCsv The enrollments CSV file.
 */
@NoArgConstructor
data class ApplyConfigurationRequest(
    @field:RestPath
    var courseSeriesSlug: String,

    @field:RestPath
    var courseInstanceSlug: String,

    @field:RestForm
    @field:NotNull
    var platform: Platform,

    @field:RestForm
    @field:NotNull
    var configYaml: FileUpload,

    @field:RestForm
    @field:NotNull
    var enrollmentsCsv: FileUpload,
)

