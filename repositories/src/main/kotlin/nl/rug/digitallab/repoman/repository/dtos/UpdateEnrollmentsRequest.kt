package nl.rug.digitallab.repoman.repository.dtos

import jakarta.validation.Valid
import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.dtos.Enrollments
import nl.rug.digitallab.repoman.common.events.configurations.EnrollmentsConfiguration

/**
 * The [UpdateEnrollmentsRequest] is the request DTO for updating the enrollments of a course instance.
 *
 * @property platform The Git platform to be used.
 * @property enrollments The enrollments for the course instance.
 * @property configuration The configuration for the enrollments.
 */
data class UpdateEnrollmentsRequest(
    val platform: Platform,
    val enrollments: Enrollments,

    @field:Valid
    val configuration: EnrollmentsConfiguration,
)
