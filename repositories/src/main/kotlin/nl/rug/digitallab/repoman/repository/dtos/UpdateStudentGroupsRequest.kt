package nl.rug.digitallab.repoman.repository.dtos

import jakarta.validation.Valid
import nl.rug.digitallab.common.kotlin.helpers.noarg.NoArgConstructor
import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.events.configurations.StudentGroupsConfiguration

/**
 * The [UpdateStudentGroupsRequest] is the request DTO for updating the student groups of a course instance.
 *
 * @property platform The Git platform to be used.
 * @property groups The student groups for the course instance.
 * @property configuration The configuration for the student groups.
 */
@NoArgConstructor
data class UpdateStudentGroupsRequest(
    val platform: Platform,
    val groups: Set<String>,

    @field:Valid
    val configuration: StudentGroupsConfiguration,
)
