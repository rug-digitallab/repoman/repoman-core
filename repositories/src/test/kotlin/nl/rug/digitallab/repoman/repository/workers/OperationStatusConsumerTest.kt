package nl.rug.digitallab.repoman.repository.workers

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.common.ErrorId
import nl.rug.digitallab.repoman.common.OperationId
import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.events.operations.CreateCourseSeriesEvent
import nl.rug.digitallab.repoman.common.events.operations.status.FailedStatusEvent
import nl.rug.digitallab.repoman.common.events.operations.status.StartedStatusEvent
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow

@QuarkusTest
class OperationStatusConsumerTest {
    @Inject
    lateinit var operationStatusConsumer: OperationStatusConsumer

    private val testOperationId = OperationId.randomUUID()

    private val startedStatusEvent = StartedStatusEvent(
        CreateCourseSeriesEvent(
            testOperationId,
            Platform.GITLAB,
            "Test CourseSeries",
            "test-course-series",
            null,
        ),
    )

    private val failedStatusEvent = FailedStatusEvent(
        CreateCourseSeriesEvent(
            testOperationId,
            Platform.GITLAB,
            "Test CourseSeries",
            "test-course-series",
            null,
        ),
        "Test error message",
        ErrorId.randomUUID(),
    )

    @Test
    fun `Handling a operation status event should succeed without exceptions`() {
        assertDoesNotThrow {
            operationStatusConsumer.handleEvent(startedStatusEvent)
            operationStatusConsumer.handleEvent(failedStatusEvent)
        }
    }
}
