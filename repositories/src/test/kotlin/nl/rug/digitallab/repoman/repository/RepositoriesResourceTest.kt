package nl.rug.digitallab.repoman.repository

import io.quarkus.test.junit.QuarkusTest
import io.restassured.http.ContentType
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import jakarta.ws.rs.core.UriBuilder
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResourceAsStream
import nl.rug.digitallab.common.kotlin.helpers.resource.listResources
import nl.rug.digitallab.repoman.common.dtos.Enrollments
import nl.rug.digitallab.repoman.common.dtos.Student
import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.events.configurations.CourseInstanceConfiguration
import nl.rug.digitallab.repoman.repository.dtos.AddCourseInstanceGroupRequest
import nl.rug.digitallab.repoman.repository.dtos.AddCourseSeriesGroupRequest
import nl.rug.digitallab.repoman.common.events.configurations.EnrollmentsConfiguration
import nl.rug.digitallab.repoman.common.events.configurations.ProjectSource
import nl.rug.digitallab.repoman.common.events.configurations.StudentGroupsConfiguration
import nl.rug.digitallab.repoman.repository.dtos.UpdateEnrollmentsRequest
import nl.rug.digitallab.repoman.repository.dtos.UpdateStudentGroupsRequest
import org.gradle.internal.service.scopes.Scope.Project
import org.hamcrest.CoreMatchers.equalTo
import org.jboss.resteasy.reactive.RestResponse.StatusCode
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import java.net.URL
import java.nio.file.Paths
import kotlin.io.path.inputStream
import kotlin.reflect.jvm.javaMethod

@QuarkusTest
class RepositoriesResourceTest {
    private val courseSeriesName = "Test Course Series"
    private val courseSeriesSlug = "test-course-series"
    private val courseInstanceName = "Test Course Instance"
    private val courseInstanceSlug = "test-course-instance"

    @Test
    fun `Adding a course series group should succeed`() {
        val addCourseSeriesGroupRequest = AddCourseSeriesGroupRequest(
            Platform.GITLAB,
            courseSeriesName,
            courseSeriesSlug,
            null,
        )

        Given {
            contentType(ContentType.JSON)
            body(addCourseSeriesGroupRequest)
        } When {
            post(addCourseSeriesGroupEndpoint())
        } Then {
            statusCode(StatusCode.ACCEPTED)
        }
    }

    @Test
    fun `Adding a course series group with an existing group ID should succeed`() {
        val addCourseSeriesGroupRequest = AddCourseSeriesGroupRequest(
            Platform.GITLAB,
            courseSeriesName,
            courseSeriesSlug,
            12345,
        )

        Given {
            contentType(ContentType.JSON)
            body(addCourseSeriesGroupRequest)
        } When {
            post(addCourseSeriesGroupEndpoint())
        } Then {
            statusCode(StatusCode.ACCEPTED)
        }
    }

    @Test
    fun `Adding a course instance group should succeed`() {
        val addCourseInstanceGroupRequest = AddCourseInstanceGroupRequest(
            platform = Platform.GITLAB,
            name = courseInstanceName,
            slug = courseInstanceSlug,
            configuration = CourseInstanceConfiguration()
        )

        Given {
            contentType(ContentType.JSON)
            body(addCourseInstanceGroupRequest)
        } When {
            post(addCourseInstanceGroupEndpoint(courseSeriesSlug))
        } Then {
            statusCode(StatusCode.ACCEPTED)
        }
    }

    @TestFactory
    fun `Updating course instance student groups should succeed`(): List<DynamicTest> {
        // Generate valid permutations of templateFromProjectId and forkFromProjectId
        val projectSources = listOf(ProjectSource.FORK, ProjectSource.TEMPLATE, ProjectSource.NONE)
        val projectIds = listOf(null, 12345L)

        val validPermutations = projectSources.flatMap { projectSource ->
                projectIds.map { projectId ->
                    StudentGroupsConfiguration(
                        projectSource = projectSource,
                        projectId = projectId,
                    )
                }
            }
            .filterNot { it.projectSource == ProjectSource.NONE && it.projectId != null }
            .filterNot { it.projectSource != ProjectSource.NONE && it.projectId == null }


        return validPermutations.map { configuration ->
            DynamicTest.dynamicTest("Updating course instance student groups with configuration $configuration should succeed") {
                Given {
                    contentType(ContentType.JSON)
                    body(UpdateStudentGroupsRequest(
                        platform = Platform.GITLAB,
                        configuration = configuration,
                        groups = setOf(),
                    ))
                } When {
                    post(updateStudentGroupsEndpoint(courseSeriesSlug, courseInstanceSlug))
                } Then {
                    statusCode(StatusCode.ACCEPTED)
                }
            }
        }
    }

    @TestFactory
    fun `Updating course instance student groups from a CSV file should succeed`() =
        listResources(Paths.get("enrollments-csv/correct"))
            .map { it.asPath() }
            .map {
                DynamicTest.dynamicTest("Updating course instance student groups from a CSV file '${it.fileName}' should succeed") {
                    Given {
                        contentType(ContentType.MULTIPART)
                        multiPart("platform", Platform.GITLAB.toString())
                        multiPart("groupsCsv", "test-groups-correctN.csv", it.inputStream())
                        multiPart("configuration", StudentGroupsConfiguration(
                            projectSource = ProjectSource.NONE,
                            projectId = null,
                        ), "application/json")
                    } When {
                        post(updateStudentGroupsFromCsvEndpoint(courseSeriesSlug, courseInstanceSlug))
                    } Then {
                        statusCode(StatusCode.ACCEPTED)
                    }
                }
            }

    @TestFactory
    fun `Updating course instance student groups and specifying fork or template but no project ID should fail`(): List<DynamicTest> {
        val projectSources = listOf(ProjectSource.FORK, ProjectSource.TEMPLATE)

        return projectSources.map { projectSource ->
            DynamicTest.dynamicTest("Updating course instance student groups with $projectSource but no project ID should fail") {
                val configuration = StudentGroupsConfiguration(projectSource, null)

                Given {
                    contentType(ContentType.JSON)
                    body(UpdateStudentGroupsRequest(
                        platform = Platform.GITLAB,
                        configuration = configuration,
                        groups = setOf(),
                    ))
                } When {
                    post(updateStudentGroupsEndpoint(courseSeriesSlug, courseInstanceSlug))
                } Then {
                    statusCode(StatusCode.BAD_REQUEST)
                }
            }
        }
    }

    @Test
    fun `Updating course instance enrollments should succeed`() {
        val updateEnrollmentsRequest = UpdateEnrollmentsRequest(
            platform = Platform.GITLAB,
            enrollments = Enrollments(
                listOf(),
                listOf(),
                listOf(),
                listOf(),
            ),
            configuration = EnrollmentsConfiguration(
                failOnInvalidHandles = false,
            ),
        )

        Given {
            contentType(ContentType.JSON)
            body(updateEnrollmentsRequest)
        } When {
            post(updateEnrollmentsEndpoint(courseSeriesSlug, courseInstanceSlug))
        } Then {
            statusCode(StatusCode.ACCEPTED)
        }
    }

    @Test
    fun `Updating course instance enrollments with duplicate enrollments should fail`() {
        val updateEnrollmentsRequest = UpdateEnrollmentsRequest(
            platform = Platform.GITLAB,
            enrollments = Enrollments(
                listOf(
                    Student("s1111111", "test-user1", "A"),
                    Student("s1111111", "test-user1", "A"),
                ),
                listOf(),
                listOf(),
                listOf(),
            ),
            configuration = EnrollmentsConfiguration(
                failOnInvalidHandles = false,
            ),
        )

        Given {
            contentType(ContentType.JSON)
            body(updateEnrollmentsRequest)
        } When {
            post(updateEnrollmentsEndpoint(courseSeriesSlug, courseInstanceSlug))
        } Then {
            statusCode(StatusCode.ACCEPTED)
        }
    }

    @TestFactory
    fun `Updating course instance enrollments from a CSV file should succeed`() =
        listResources(Paths.get("enrollments-csv/correct"))
            .map { it.asPath() }
            .map {
                DynamicTest.dynamicTest("Updating course instance enrollments from a CSV file '${it.fileName}' should succeed") {
                    Given {
                        contentType(ContentType.MULTIPART)
                        multiPart("platform", Platform.GITLAB.toString())
                        multiPart("enrollmentsCsv", "test-users-correctN.csv", it.inputStream())
                        multiPart("configuration", EnrollmentsConfiguration(
                            failOnInvalidHandles = false,
                        ), "application/json")
                    } When {
                        post(updateEnrollmentsFromCsvEndpoint(courseSeriesSlug, courseInstanceSlug))
                    } Then {
                        statusCode(StatusCode.ACCEPTED)
                    }
                }
            }

    @Test
    fun `Updating course instance enrollments from an unparsable CSV file should fail`() {
        val csvStream = getResourceAsStream("enrollments-csv/unparsable/test-users-unparsable.csv")

        Given {
            contentType(ContentType.MULTIPART)
            multiPart("platform", Platform.GITLAB.toString())
            multiPart("enrollmentsCsv", "test-users-unparsable.csv", csvStream)
            multiPart("configuration", EnrollmentsConfiguration(
                failOnInvalidHandles = false,
            ), "application/json")
        } When {
            post(updateEnrollmentsFromCsvEndpoint(courseSeriesSlug, courseInstanceSlug))
        } Then {
            statusCode(StatusCode.BAD_REQUEST)
            body("errorMessage", equalTo("Unable to parse the provided CSV"))
        }
    }

    @TestFactory
    fun `Updating course instance enrollments from an parsable but invalid CSV file should fail`() =
        listResources(Paths.get("enrollments-csv/incorrect"))
            .map { it.asPath() }
            .map {
                 DynamicTest.dynamicTest("Updating course instance enrollments from parsable but invalid CSV file '${it.fileName}' should return the expected error code") {
                    Given {
                        contentType(ContentType.MULTIPART)
                        multiPart("platform", Platform.GITLAB.toString())
                        multiPart("enrollmentsCsv", "test-users-incorrectN.csv", it.inputStream())
                        multiPart("configuration", EnrollmentsConfiguration(
                            failOnInvalidHandles = false,
                        ), "application/json")
                    } When {
                        post(updateEnrollmentsFromCsvEndpoint(courseSeriesSlug, courseInstanceSlug))
                    } Then {
                        statusCode(StatusCode.BAD_REQUEST)
                    }
                }
            }

    @TestFactory
    fun `Applying any combination of valid configuration and valid enrollments should succeed`(): List<DynamicTest> {
        val configurations = listResources("configuration-yaml/correct")
        val enrollments = listResources("enrollments-csv/correct")

        val combinations = configurations.flatMap { configuration ->
            enrollments.map { enrollment ->
                TestCase(configuration, enrollment)
            }
        }

        return combinations.map { (configuration, enrollment) ->
            DynamicTest.dynamicTest("Applying configuration '${configuration.asPath().fileName}' and enrollments '${enrollment.asPath().fileName}' should succeed") {
                Given {
                    contentType(ContentType.MULTIPART)
                    multiPart("platform", Platform.GITLAB.toString())
                    multiPart("configYaml", "configurationN", configuration.openStream())
                    multiPart("enrollmentsCsv", "enrollmentsCsvN", enrollment.openStream())
                } When {
                    post(applyConfigurationEndpoint(courseSeriesSlug, courseInstanceSlug))
                } Then {
                    statusCode(StatusCode.ACCEPTED)
                }
            }
        }
    }

    @TestFactory
    fun `Applying any combination that contains invalid configurations or invalid enrollments should fail`(): List<DynamicTest> {
        val validConfigurations = listResources("configuration-yaml/correct")
        val validEnrollments = listResources("enrollments-csv/correct")
        val invalidConfigurations = listResources("configuration-yaml/incorrect")
        val invalidEnrollments = listResources("enrollments-csv/incorrect")

        val combinations = invalidConfigurations.flatMap { configuration ->
            invalidEnrollments.map { enrollment ->
                TestCase(configuration, enrollment) // Both invalid
            }
        } + invalidConfigurations.flatMap { configuration ->
            validEnrollments.map { enrollment ->
                TestCase(configuration, enrollment) // Invalid configuration
            }
        } + validConfigurations.flatMap { configuration ->
            invalidEnrollments.map { enrollment ->
                TestCase(configuration, enrollment) // Invalid enrollment
            }
        }

        return combinations.map { (configuration, enrollment) ->
            DynamicTest.dynamicTest("Applying configuration '${configuration.asPath().fileName}' and enrollments '${enrollment.asPath().fileName}' should fail") {
                Given {
                    contentType(ContentType.MULTIPART)
                    multiPart("platform", Platform.GITLAB.toString())
                    multiPart("configYaml", "configurationN", configuration.openStream())
                    multiPart("enrollmentsCsv", "enrollmentsCsvN", enrollment.openStream())
                } When {
                    post(applyConfigurationEndpoint(courseSeriesSlug, courseInstanceSlug))
                } Then {
                    statusCode(StatusCode.BAD_REQUEST)
                }
            }
        }
    }

    data class TestCase(val configuration: URL, val enrollments: URL)

    companion object {
        fun addCourseSeriesGroupEndpoint(): String {
            return UriBuilder
                .fromResource(RepositoriesResource::class.java)
                .path(RepositoriesResource::addCourseSeriesGroup.javaMethod)
                .build()
                .toString()
        }

        fun addCourseInstanceGroupEndpoint(courseSeriesSlug: String): String {
            return UriBuilder
                .fromResource(RepositoriesResource::class.java)
                .path(RepositoriesResource::addCourseInstanceGroup.javaMethod)
                .build(courseSeriesSlug)
                .toString()
        }

        fun updateStudentGroupsEndpoint(courseSeriesSlug: String, courseInstanceSlug: String): String {
            return UriBuilder
                .fromResource(RepositoriesResource::class.java)
                .path(RepositoriesResource::updateStudentGroups.javaMethod)
                .build(courseSeriesSlug, courseInstanceSlug)
                .toString()
        }

        fun updateStudentGroupsFromCsvEndpoint(courseSeriesSlug: String, courseInstanceSlug: String): String {
            return UriBuilder
                .fromResource(RepositoriesResource::class.java)
                .path(RepositoriesResource::updateStudentGroupsFromCsv.javaMethod)
                .build(courseSeriesSlug, courseInstanceSlug)
                .toString()
        }

        fun updateEnrollmentsEndpoint(courseSeriesSlug: String, courseInstanceSlug: String): String {
            return UriBuilder
                .fromResource(RepositoriesResource::class.java)
                .path(RepositoriesResource::updateEnrollments.javaMethod)
                .build(courseSeriesSlug, courseInstanceSlug)
                .toString()
        }

        fun updateEnrollmentsFromCsvEndpoint(courseSeriesSlug: String, courseInstanceSlug: String): String {
            return UriBuilder
                .fromResource(RepositoriesResource::class.java)
                .path(RepositoriesResource::updateEnrollmentsFromCsv.javaMethod)
                .build(courseSeriesSlug, courseInstanceSlug)
                .toString()
        }

        fun applyConfigurationEndpoint(courseSeriesSlug: String, courseInstanceSlug: String): String {
            return UriBuilder
                .fromResource(RepositoriesResource::class.java)
                .path(RepositoriesResource::applyConfiguration.javaMethod)
                .build(courseSeriesSlug, courseInstanceSlug)
                .toString()
        }
    }
}
