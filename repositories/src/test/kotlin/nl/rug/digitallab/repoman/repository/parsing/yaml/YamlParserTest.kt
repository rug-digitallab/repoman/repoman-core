package nl.rug.digitallab.repoman.repository.parsing.yaml

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.common.kotlin.helpers.resource.listResources
import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.events.configurations.ProjectSource
import nl.rug.digitallab.repoman.repository.configurations.Service
import nl.rug.digitallab.repoman.repository.exceptions.YamlParsingException
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.assertThrows
import kotlin.io.path.Path

@QuarkusTest
class YamlParserTest {
    @Inject
    private lateinit var yamlParser: YamlParser

    @Test
    fun `Parsing the default YAML file from worker-gitlab should return a Configuration object`() {
        // We cannot read the file as a resource because we are in a different module. The worker-gitlab does not have
        // access to the YamlParser class, so we read the file from the file system.
        val config = yamlParser.parseYaml(Path("../worker-gitlab/src/main/resources/templates/configuration/config.example.yml"))

        assertTrue(config.services.contains(Service.REPOSITORY_MANAGEMENT))
        assertEquals(Platform.GITLAB, config.repositoryManagement.platform)
        assertEquals(ProjectSource.NONE, config.repositoryManagement.groups.projectSource)
        assertEquals(null, config.repositoryManagement.groups.projectId)
        assertEquals(false, config.repositoryManagement.enrollments.failOnInvalidHandles)
    }

    @Test
    fun `Parsing a valid YAML file should return a Configuration object`() {
        val config = yamlParser.parseYaml(getResource("configuration-yaml/correct/config-default.yml").asPath())

        assertTrue(config.services.contains(Service.REPOSITORY_MANAGEMENT))
        assertEquals(Platform.GITLAB, config.repositoryManagement.platform)
        assertEquals(ProjectSource.NONE, config.repositoryManagement.groups.projectSource)
        assertEquals(null, config.repositoryManagement.groups.projectId)
        assertEquals(false, config.repositoryManagement.enrollments.failOnInvalidHandles)
    }

    @TestFactory
    fun `Parsing a valid YAML configuration should succeed`() =
        listResources("configuration-yaml/correct")
            .map { it.asPath() }
            .map {
                DynamicTest.dynamicTest("Parsing a valid YAML configuration should succeed for ${it.fileName}") {
                    assertDoesNotThrow { yamlParser.parseYaml(it) }
                }
            }

    @TestFactory
    fun `Parsing an invalid YAML file should throw an exception`() =
        listResources("configuration-yaml/incorrect")
            .map { it.asPath() }
            .map {
                DynamicTest.dynamicTest("Parsing an invalid YAML file should throw an exception for ${it.fileName}") {
                    assertThrows<YamlParsingException> { yamlParser.parseYaml(it) }
                }
            }

}
