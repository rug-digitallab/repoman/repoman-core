package nl.rug.digitallab.repoman.repository.workers

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.reactive.messaging.memory.InMemoryConnector
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.common.OperationId
import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.events.operations.CreateCourseSeriesEvent
import nl.rug.digitallab.repoman.common.events.operations.OperationEvent
import org.awaitility.Awaitility.await
import org.eclipse.microprofile.reactive.messaging.spi.Connector
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@QuarkusTest
class OperationProducerTest {
    @Inject
    lateinit var producer: OperationProducer

    @Inject
    @Connector("smallrye-in-memory")
    lateinit var connector: InMemoryConnector

    private val testCourseSeriesName = "Test Course Series"
    private val testCourseSeriesSlug = "test-course-series"

    @BeforeEach
    fun `Empty the queue`() {
        val consumer = connector.sink<CreateCourseSeriesEvent>("worker-gitlab-operation")
        consumer.clear()
    }

    @Test
    @RunOnVertxContext
    fun `Queueing an operation event should result in the event being consumed`(asserter: UniAsserter) {
        val inMemorySink = connector.sink<OperationEvent>("worker-gitlab-operation")

        val event = CreateCourseSeriesEvent(
            OperationId.randomUUID(),
            Platform.GITLAB,
            testCourseSeriesName,
            testCourseSeriesSlug,
            null,
        )

        asserter
            .assertThat({ producer.queueEvent(event) }) {
                await().until {
                    inMemorySink.received().isNotEmpty()
                }

                val receivedEvent = inMemorySink.received().first().payload as CreateCourseSeriesEvent

                assertEquals(event.operationId, receivedEvent.operationId)
                assertEquals(event.operationType, receivedEvent.operationType)
                assertEquals(event.platform, receivedEvent.platform)
                assertEquals(event.courseSeriesSlug, receivedEvent.courseSeriesSlug)
                assertEquals(event.courseSeriesName, receivedEvent.courseSeriesName)
                assertEquals(1, inMemorySink.received().size)
            }

    }
}
