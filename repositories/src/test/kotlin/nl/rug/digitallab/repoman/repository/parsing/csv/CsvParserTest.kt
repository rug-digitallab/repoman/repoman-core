package nl.rug.digitallab.repoman.repository.parsing.csv

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.common.kotlin.helpers.resource.listResources
import nl.rug.digitallab.repoman.common.enums.Role
import nl.rug.digitallab.repoman.repository.exceptions.CsvParsingException
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import java.nio.file.Paths

@QuarkusTest
class CsvParserTest {
    @Inject
    private lateinit var csvParser: CsvParser

    @Test
    fun `Parsing a valid CSV file should result in the expected enrollments`() {
        val enrollments = csvParser.parse(getResource("enrollments-csv/correct/test-users-correct-users-with-multiple-groups.csv").asPath())

        val students = enrollments.students
        val reviewers = enrollments.reviewers
        val teachers = enrollments.teachers
        val maintainers = enrollments.maintainers

        assertEquals(3, students.size)
        assertEquals(1, reviewers.size)
        assertEquals(3, teachers.size)
        assertEquals(1, maintainers.size)

        assertTrue(students.map { it.username }.containsAll(listOf("s1111111", "s1111112")))
        assertTrue(reviewers.map { it.username }.containsAll(listOf("s1111113")))
        assertTrue(teachers.map { it.username }.containsAll(listOf("p2111113", "p2111114")))
        assertTrue(maintainers.map { it.username }.containsAll(listOf("p31111115")))

        assertTrue(students.map { it.handle }.containsAll(listOf("test-user1", "test-user2")))
        assertTrue(reviewers.map { it.handle }.containsAll(listOf("test-user3")))
        assertTrue(teachers.map { it.handle }.containsAll(listOf("test-user4", "test-user5")))
        assertTrue(maintainers.map { it.handle }.containsAll(listOf("test-user6")))

        assertTrue(students.map { it.groupName }.containsAll(listOf("Group 1", "Group 2")))
        assertTrue(reviewers.map { it.groupName }.containsAll(listOf("Group 2")))
        assertTrue(teachers.map { it.groupName }.containsAll(listOf("Group 1", "Group 2", null)))
    }

    @TestFactory
    fun `Parsing valid CSV file should succeed`() =
        listResources("enrollments-csv/correct")
            .map { it.asPath() }
            .map {
                DynamicTest.dynamicTest("Parsing valid CSV file should return the expected course instance enrollments for ${it.fileName}") {
                    assertDoesNotThrow { csvParser.parse(it) }
                }
            }

    @TestFactory
    fun `Parsing invalid CSV file should fail`() =
        (listResources("enrollments-csv/incorrect") + listResources(Paths.get("enrollments-csv/unparsable")))
            .map { it.asPath() }
            .map {
                DynamicTest.dynamicTest("Parsing invalid CSV file should fail for ${it.fileName}") {
                    assertThrows<CsvParsingException> { csvParser.parse(it) }
                }
            }

    @Test
    fun `Parsing a row with extra spaces should trimmed`() {
        val enrollments = csvParser.parse(getResource("enrollments-csv/correct/test-users-correct-extra-spaces.csv").asPath())

        assertEquals(1, enrollments.students.size)

        val student = enrollments.students.first()

        assertEquals("A", student.groupName)
        assertEquals("s1111111", student.username)
        assertEquals("test-user1", student.handle)
        assertEquals(Role.STUDENT, student.role)
    }
}
