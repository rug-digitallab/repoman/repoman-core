package nl.rug.digitallab.repoman.repository.managers

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.reactive.messaging.memory.InMemoryConnector
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.common.quarkus.test.rest.MockFileUpload
import nl.rug.digitallab.repoman.common.dtos.Enrollments
import nl.rug.digitallab.repoman.common.enums.OperationType
import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.events.configurations.CourseInstanceConfiguration
import nl.rug.digitallab.repoman.common.events.operations.CreateCourseInstanceEvent
import nl.rug.digitallab.repoman.common.events.operations.CreateCourseSeriesEvent
import nl.rug.digitallab.repoman.common.events.operations.UpdateEnrollmentsEvent
import nl.rug.digitallab.repoman.common.events.configurations.EnrollmentsConfiguration
import nl.rug.digitallab.repoman.common.events.configurations.ProjectSource
import nl.rug.digitallab.repoman.common.events.configurations.StudentGroupsConfiguration
import nl.rug.digitallab.repoman.common.events.operations.UpdateStudentGroupsEvent
import nl.rug.digitallab.repoman.repository.dtos.*
import org.eclipse.microprofile.reactive.messaging.spi.Connector
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RepositoriesManagerTest {
    @Inject
    lateinit var repositoriesManager: RepositoriesManager

    @Inject
    @Connector("smallrye-in-memory")
    lateinit var connector: InMemoryConnector

    private val workerGitLabOperationTopic = "worker-gitlab-operation"
    private val courseSeriesName = "Test Course Series"
    private val courseSeriesSlug = "test-course-series"
    private val courseInstanceName = "Test Course Instance"
    private val courseInstanceSlug = "test-course-instance"

    @BeforeEach
    fun `Empty the queue`(){
        val consumer = connector.sink<CreateCourseSeriesEvent>(workerGitLabOperationTopic)
        consumer.clear()
    }

    @Test
    @RunOnVertxContext
    fun `Adding a course series group should emit an event`(asserter: UniAsserter){
        val addCourseSeriesGroupRequest = AddCourseSeriesGroupRequest(
            Platform.GITLAB,
            courseSeriesName,
            courseSeriesSlug,
            null,
        )

        val consumer = connector.sink<CreateCourseSeriesEvent>(workerGitLabOperationTopic)

        asserter
            .execute<Unit> { repositoriesManager.addCourseSeriesGroup(addCourseSeriesGroupRequest) }
            .execute {
                assertEquals(1, consumer.received().size)
                val createCourseSeriesEvent = consumer.received().first().payload

                assertEquals(OperationType.CREATE_COURSE_SERIES, createCourseSeriesEvent.operationType)
                assertEquals(addCourseSeriesGroupRequest.platform, createCourseSeriesEvent.platform)
                assertEquals(addCourseSeriesGroupRequest.name, createCourseSeriesEvent.courseSeriesName)
                assertEquals(addCourseSeriesGroupRequest.slug, createCourseSeriesEvent.courseSeriesSlug)
                assertEquals(addCourseSeriesGroupRequest.groupId, createCourseSeriesEvent.groupId)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Adding a course series group with an existing group ID should emit an event`(asserter: UniAsserter){
        val addCourseSeriesGroupRequest = AddCourseSeriesGroupRequest(
            Platform.GITLAB,
            courseSeriesName,
            courseSeriesSlug,
            12345,
        )

        val consumer = connector.sink<CreateCourseSeriesEvent>(workerGitLabOperationTopic)

        asserter
            .execute<Unit> { repositoriesManager.addCourseSeriesGroup(addCourseSeriesGroupRequest) }
            .execute {
                assertEquals(1, consumer.received().size)
                val createCourseSeriesEvent = consumer.received().first().payload

                assertEquals(OperationType.CREATE_COURSE_SERIES, createCourseSeriesEvent.operationType)
                assertEquals(addCourseSeriesGroupRequest.platform, createCourseSeriesEvent.platform)
                assertEquals(addCourseSeriesGroupRequest.name, createCourseSeriesEvent.courseSeriesName)
                assertEquals(addCourseSeriesGroupRequest.slug, createCourseSeriesEvent.courseSeriesSlug)
                assertEquals(addCourseSeriesGroupRequest.groupId, createCourseSeriesEvent.groupId)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Adding a course instance group should emit an event`(asserter: UniAsserter){
        val addCourseInstanceGroupRequest = AddCourseInstanceGroupRequest(
            platform = Platform.GITLAB,
            name = courseInstanceName,
            slug = courseInstanceSlug,
            configuration = CourseInstanceConfiguration(),
        )

        val consumer = connector.sink<CreateCourseInstanceEvent>(workerGitLabOperationTopic)

        asserter
            .execute<Unit> { repositoriesManager.addCourseInstanceGroup(courseSeriesSlug, addCourseInstanceGroupRequest) }
            .execute {
                assertEquals(1, consumer.received().size)
                val createCourseInstanceEvent = consumer.received().first().payload

                assertEquals(OperationType.CREATE_COURSE_INSTANCE, createCourseInstanceEvent.operationType)
                assertEquals(addCourseInstanceGroupRequest.platform, createCourseInstanceEvent.platform)
                assertEquals(addCourseInstanceGroupRequest.name, createCourseInstanceEvent.courseInstanceName)
                assertEquals(addCourseInstanceGroupRequest.slug, createCourseInstanceEvent.courseInstanceSlug)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Updating course instance student groups should emit an event`(asserter: UniAsserter){
        val updateStudentGroupsRequest = UpdateStudentGroupsRequest(
            platform = Platform.GITLAB,
            groups = setOf(),
            configuration = StudentGroupsConfiguration(
                projectSource = ProjectSource.NONE,
                projectId = null,
            ),
        )

        val consumer = connector.sink<UpdateStudentGroupsEvent>(workerGitLabOperationTopic)

        asserter
            .execute<Unit> { repositoriesManager.updateStudentGroups(courseSeriesSlug, courseInstanceSlug, updateStudentGroupsRequest) }
            .execute {
                assertEquals(1, consumer.received().size)
                val updateStudentGroupsEvent = consumer.received().first().payload

                assertEquals(OperationType.UPDATE_STUDENT_GROUPS, updateStudentGroupsEvent.operationType)
                assertEquals(Platform.GITLAB, updateStudentGroupsEvent.platform)
                assertEquals(courseSeriesSlug, updateStudentGroupsEvent.courseSeriesSlug)
                assertEquals(courseInstanceSlug, updateStudentGroupsEvent.courseInstanceSlug)
                assertEquals(updateStudentGroupsEvent.groups, updateStudentGroupsEvent.groups)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Updating course instance student groups by CSV should emit an event`(asserter: UniAsserter){
        val updateStudentGroupsRequest = UpdateStudentGroupsCsvRequest(
            courseSeriesSlug = courseSeriesSlug,
            courseInstanceSlug = courseInstanceSlug,
            platform = Platform.GITLAB,
            groupsCsv = MockFileUpload(getResource("enrollments-csv/correct/test-users-correct-simple.csv").asPath()),
            configuration = StudentGroupsConfiguration(
                projectSource = ProjectSource.NONE,
                projectId = null,
            ),
        )

        val consumer = connector.sink<UpdateStudentGroupsEvent>(workerGitLabOperationTopic)

        asserter
            .execute<Unit> { repositoriesManager.updateStudentGroupsFromCsv(updateStudentGroupsRequest) }
            .execute {
                assertEquals(1, consumer.received().size)
                val updateStudentGroupsEvent = consumer.received().first().payload

                assertEquals(OperationType.UPDATE_STUDENT_GROUPS, updateStudentGroupsEvent.operationType)
                assertEquals(Platform.GITLAB, updateStudentGroupsEvent.platform)
                assertEquals(courseSeriesSlug, updateStudentGroupsEvent.courseSeriesSlug)
                assertEquals(courseInstanceSlug, updateStudentGroupsEvent.courseInstanceSlug)
                assertEquals(2, updateStudentGroupsEvent.groups.size)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Updating course instance enrollments should emit an event`(asserter: UniAsserter){
        val updateEnrollmentsRequest = UpdateEnrollmentsRequest(
            platform = Platform.GITLAB,
            enrollments = Enrollments(
                listOf(),
                listOf(),
                listOf(),
                listOf(),
            ),
            configuration = EnrollmentsConfiguration(
                failOnInvalidHandles = false,
            ),
        )

        val consumer = connector.sink<UpdateEnrollmentsEvent>(workerGitLabOperationTopic)

        asserter
            .execute<Unit> { repositoriesManager.updateEnrollments(courseSeriesSlug, courseInstanceSlug, updateEnrollmentsRequest) }
            .execute {
                assertEquals(1, consumer.received().size)
                val updateEnrollmentsEvent = consumer.received().first().payload

                assertEquals(OperationType.UPDATE_ENROLLMENTS, updateEnrollmentsEvent.operationType)
                assertEquals(Platform.GITLAB, updateEnrollmentsEvent.platform)
                assertEquals(courseSeriesSlug, updateEnrollmentsEvent.courseSeriesSlug)
                assertEquals(courseInstanceSlug, updateEnrollmentsEvent.courseInstanceSlug)
                assertEquals(updateEnrollmentsRequest.enrollments, updateEnrollmentsEvent.enrollments)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Updating course instance enrollments by CSV should emit an event`(asserter: UniAsserter){
        val updateEnrollmentsCsvRequest = UpdateEnrollmentsCsvRequest(
            courseSeriesSlug = courseSeriesSlug,
            courseInstanceSlug = courseInstanceSlug,
            platform = Platform.GITLAB,
            enrollmentsCsv = MockFileUpload(getResource("enrollments-csv/correct/test-users-correct-simple.csv").asPath()),
            configuration = EnrollmentsConfiguration(
                failOnInvalidHandles = false,
            ),
        )

        val consumer = connector.sink<UpdateEnrollmentsEvent>(workerGitLabOperationTopic)

        asserter
            .execute<Unit> { repositoriesManager.updateEnrollmentsFromCsv(updateEnrollmentsCsvRequest) }
            .execute {
                assertEquals(1, consumer.received().size)
                val updateEnrollmentsEvent = consumer.received().first().payload

                assertEquals(OperationType.UPDATE_ENROLLMENTS, updateEnrollmentsEvent.operationType)
                assertEquals(Platform.GITLAB, updateEnrollmentsEvent.platform)
                assertEquals(courseSeriesSlug, updateEnrollmentsEvent.courseSeriesSlug)
                assertEquals(courseInstanceSlug, updateEnrollmentsEvent.courseInstanceSlug)
                assertEquals(2, updateEnrollmentsEvent.enrollments.students.size)
                assertEquals(1, updateEnrollmentsEvent.enrollments.teachers.size)
                assertEquals(1, updateEnrollmentsEvent.enrollments.maintainers.size)
            }
    }
}
