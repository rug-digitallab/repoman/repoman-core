package nl.rug.digitallab.repoman.worker.gitlab.terraform.stacks

import com.hashicorp.cdktf.App
import com.hashicorp.cdktf.Token
import com.hashicorp.cdktf.providers.gitlab.group.Group
import com.hashicorp.cdktf.providers.gitlab.branch.Branch
import com.hashicorp.cdktf.providers.gitlab.branch_protection.BranchProtection
import com.hashicorp.cdktf.providers.gitlab.project.Project
import nl.rug.digitallab.repoman.common.dtos.*
import nl.rug.digitallab.repoman.common.events.configurations.ProjectSource
import nl.rug.digitallab.repoman.common.events.configurations.StudentGroupsConfiguration
import nl.rug.digitallab.repoman.worker.gitlab.terraform.studentMainBranchDefaults
import nl.rug.digitallab.repoman.worker.gitlab.terraform.studentProjectDefaults

/**
 * Class representing course instance student groups as a RepoManStack, which is a Terraform CDK concept.
 * One stack represents one Terraform project or module. IDs must be unique in a RepoManStack.
 *
 * @param courseInstanceSlug The slug of the course instance for which the student groups are created.
 * @param groups List of student groups to create.
 * @param configuration Configuration for the student groups.
 */
class StudentGroupsStack(
    scope: App,
    private val courseInstanceSlug: String,
    groups: List<StudentGroup>,
    private val configuration: StudentGroupsConfiguration,
) : RepoManStack(scope, "ci_${courseInstanceSlug}_groups") {
    // These are the fixed local variables that contain the necessary group IDs.
    private val studentsGroupId = id { "\${local.ci_${courseInstanceSlug}_students_group_id}" }
    private val teacherResourcesGroupId = id { "\${local.ci_${courseInstanceSlug}_teacher_resources_group_id}" }

    init {
        createStudents(groups)
    }

    /**
     * Create all resources for a list of students.
     *
     * @param groups - List of student groups to create.
     */
    private fun createStudents(groups: List<StudentGroup>) {
        createStudentGroups(groups)
        createStudentProjects(groups)
    }

    /**
     * Creates the gitlab group resources for all the students.
     *
     * @param groups - List of student groups to create.
     */
    private fun createStudentGroups(groups: List<StudentGroup>) {
        groups.forEach { group ->
            // Creates the main GitLab group for the students.
            Group.Builder.create(this, id { "ci_${courseInstanceSlug}_${group.groupSlug}" })
                .name(group.groupName)
                .path(group.groupSlug)
                .parentId(Token.asNumber(studentsGroupId))
                .build()
        }
    }

    /**
     * Create the project resources for each student group.
     *
     * @param groups - List of student groups to create a project for.
     */
    private fun createStudentProjects(groups: List<StudentGroup>) {
        groups.forEach { group ->
            val projectName = "${group.groupName} Project"
            val projectSlug = "${group.groupSlug}-project"

            // Create Projects which only have a main branch.
            val studentGroupProjectBuilder = Project.Builder.create(this, id { "ci_${courseInstanceSlug}_$projectSlug" })
                .studentProjectDefaults()
                .name(projectName)
                .path(projectSlug)
                .namespaceId(Token.asNumber(id { "\${gitlab_group.ci_${courseInstanceSlug}_${group.groupSlug}.id}" }))
                .archiveOnDestroy(true)

            // Instantiating a project from a template can only be done with a personal access token, not with a group access token.
            if(configuration.projectSource == ProjectSource.TEMPLATE) {
                studentGroupProjectBuilder
                    // Use group-level templates
                    .useCustomTemplate(true)
                    // The group with the templates (`gitlab-templates`)
                    .groupWithProjectTemplatesId(Token.asNumber(teacherResourcesGroupId))
                    // The template project ID within the group (`gitlab-templates`)
                    .templateProjectId(configuration.projectId)
            }

            if(configuration.projectSource == ProjectSource.FORK) {
                studentGroupProjectBuilder
                    .forkedFromProjectId(configuration.projectId)
                    .mrDefaultTargetSelf(true)
            }

            val studentGroupProject = studentGroupProjectBuilder.build()

            // Do not set up the branches if we are forking or initiating a template from a project. We don't know which
            // branches already exist. If we create branches that already exist, Terraform will error out.
            if(configuration.projectSource != ProjectSource.NONE)
                return@forEach

            createStudentProjectMainBranchWithProtection(studentGroupProject.id, projectSlug)
        }
    }

    /**
     * Creates the default branches for the student projects. It also sets branch protection.
     *
     * @param projectId - ID of the project on which we want to add the branch.
     * @param projectSlug - Slug of the project we want to add the branch.
     */
    private fun createStudentProjectMainBranchWithProtection(projectId: String, projectSlug: String) {
        // Add develop branch.
        Branch.Builder.create(this, id { "ci_${courseInstanceSlug}_${projectSlug}_develop" })
            .name("develop")
            .ref("main")
            .project(projectId)
            .build()

        // Makes it so only owners can merge on main.
        BranchProtection.Builder.create(this, id { "ci_${courseInstanceSlug}_${projectSlug}_main" })
            .studentMainBranchDefaults()
            .branch("main")
            .project(projectId)
            .build()
    }
}
