package nl.rug.digitallab.repoman.worker.gitlab.enums

import com.hashicorp.cdktf.providers.gitlab.project.Project
import com.hashicorp.cdktf.providers.gitlab.project.ProjectPushRules

/**
 * The base course instance GitLab projects that should always be created for every course instance.
 *
 * @property slug The slug name of the project - used in URLs.
 * @property displayName The user-friendly name of the project as shown in GitLab UI.
 * @property description Optional description of the project shown in GitLab UI.
 * @property defaultSettings Expects a [Project.Builder] and returns a [Project.Builder] with the default project settings.
 */
enum class BaseCourseInstanceProject(
    override val slug: String,
    override val displayName: String, // "name" is already a property of an enum
    override val description: String = "",
    override val defaultSettings: (Project.Builder) -> Project.Builder,
): BaseCourseInstanceResource<Project.Builder> {
    /**
     * CONFIGURATION - Group for configuring Digital Lab products.
     */
    CONFIGURATION(
        slug = "configuration",
        displayName = "Configuration",
        description = "This project contains the configuration for Digital Lab products.",
        { builder ->
            builder
                // Enable repository, issues, and CI
                .buildsAccessLevel("enabled")
                .issuesAccessLevel("enabled")
                .issuesEnabled(true)
                .repositoryAccessLevel("enabled")
                // Disable all other features
                .analyticsAccessLevel("disabled")
                .containerRegistryAccessLevel("disabled")
                .environmentsAccessLevel("disabled")
                .featureFlagsAccessLevel("disabled")
                .infrastructureAccessLevel("disabled")
                .monitorAccessLevel("disabled")
                .packagesEnabled(false)
                .pagesAccessLevel("disabled")
                .releasesAccessLevel("disabled")
                .requirementsAccessLevel("disabled")
                .securityAndComplianceAccessLevel("disabled")
                .snippetsEnabled(false)
                // Allows us to commit files to the repository with digitallab@rug.nl
                .pushRules(ProjectPushRules.builder().memberCheck(false).build())
        },
    ),

    /**
     * ARTIFACTS - Group for storing artifacts for a specific course instance.
     */
    ARTIFACTS(
        slug = "artifacts",
        displayName = "Artifacts",
        description = "This project contains the Terraform artifacts for this course instance. " +
                "The Terraform artifacts are used to provision the student groups.",
        { builder ->
            builder
                // Enable repository, CI, and Terraform state
                .buildsAccessLevel("enabled")
                .repositoryAccessLevel("enabled")
                .infrastructureAccessLevel("enabled")
                // Disable all other features
                .analyticsAccessLevel("disabled")
                .containerRegistryAccessLevel("disabled")
                .environmentsAccessLevel("disabled")
                .featureFlagsAccessLevel("disabled")
                .issuesAccessLevel("disabled")
                .monitorAccessLevel("disabled")
                .packagesEnabled(false)
                .pagesAccessLevel("disabled")
                .releasesAccessLevel("disabled")
                .requirementsAccessLevel("disabled")
                .securityAndComplianceAccessLevel("disabled")
                .snippetsEnabled(false)
                // Allows us to commit files to the repository with digitallab@rug.nl
                .pushRules(ProjectPushRules.builder().memberCheck(false).build())
        },
    ),
}
