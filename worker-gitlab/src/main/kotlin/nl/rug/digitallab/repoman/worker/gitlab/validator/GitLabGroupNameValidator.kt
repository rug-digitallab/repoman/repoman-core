package nl.rug.digitallab.repoman.worker.gitlab.validator

import nl.rug.digitallab.repoman.common.events.constraints.GroupName

/**
 * Constraint validator for [GroupName].
 *
 * Constraints:
 *  -  Must start and end with a letter (a-zA-Z) or a digit (0-9)
 *  -  Must contain only letters (a-zA-Z), digits (0-9), underscores (_), dots (.), parentheses (()), dashes (-), or spaces.
 *
 * https://docs.gitlab.com/ee/user/reserved_names.html#limitations-on-usernames-project-and-group-names
 */
class GitLabGroupNameValidator : GitLabBaseValidator<GroupName>("^[a-zA-Z0-9][a-zA-Z0-9_.()\\- ]*[a-zA-Z0-9]\$")
