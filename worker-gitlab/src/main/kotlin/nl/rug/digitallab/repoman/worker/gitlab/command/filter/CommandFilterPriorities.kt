package nl.rug.digitallab.repoman.worker.gitlab.command.filter

const val NORMALIZATION_FILTER_PRIORITY = 50
const val VALIDATION_FILTER_PRIORITY = 100
const val ADAPT_FILTER_PRIORITY = 200
const val UPLOAD_FILTER_PRIORITY = Int.MAX_VALUE - 10
