package nl.rug.digitallab.repoman.worker.gitlab.command.impl

import jakarta.validation.Valid
import nl.rug.digitallab.repoman.common.events.operations.UpdateEnrollmentsEvent
import nl.rug.digitallab.repoman.worker.gitlab.terraform.TerraformFacade
import nl.rug.digitallab.repoman.worker.gitlab.command.Command

/**
 * A Terraform command that updates the enrollments of a course instance.
 *
 * @param event The event that contains the information to update the enrollments.
 * @param terraformFacade The Terraform facade to execute the command.
 */
data class UpdateEnrollmentsCommand(
    @field:Valid val event: UpdateEnrollmentsEvent,
    private val terraformFacade: TerraformFacade,
) : Command {
    override fun execute() = terraformFacade.updateEnrollments(event)

    override fun toString(): String {
        return "UpdateEnrollmentsCommand(event=$event)"
    }
}
