package nl.rug.digitallab.repoman.worker.gitlab.command.filter

import nl.rug.digitallab.repoman.worker.gitlab.command.Command
import nl.rug.digitallab.repoman.worker.gitlab.command.CommandResult

typealias RunCommandFilter = (Command) -> CommandResult

/**
 * A filter that can be applied to a [Command]. Each command filter should be annotated with [jakarta.enterprise.context.ApplicationScoped]
 * and [jakarta.annotation.Priority] to specify the order in which the filters are applied. Highest priority filters are
 * applied first, while receiving the result of the command in reverse order. Every call to [filter] should call
 * [RunCommandFilter] to continue applying the filters.
 */
interface CommandFilter {
    val name: String

    /**
     * Filters the given [command]. The filter should call [RunCommandFilter] to continue filtering the command.
     *
     * @param command The command to filter.
     * @param nextFilter The next filter in the chain.
     */
    fun filter(command: Command, nextFilter: RunCommandFilter): CommandResult
}
