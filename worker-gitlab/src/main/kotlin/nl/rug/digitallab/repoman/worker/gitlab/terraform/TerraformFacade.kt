package nl.rug.digitallab.repoman.worker.gitlab.terraform

import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralDirectory
import nl.rug.digitallab.repoman.common.events.operations.CreateCourseInstanceEvent
import nl.rug.digitallab.repoman.common.events.operations.CreateCourseSeriesEvent
import nl.rug.digitallab.repoman.common.events.operations.UpdateEnrollmentsEvent
import nl.rug.digitallab.repoman.common.events.operations.UpdateStudentGroupsEvent
import nl.rug.digitallab.repoman.worker.gitlab.command.TerraformResult
import java.nio.file.Path

/**
 * Facade class that abstracts the Terraform knowledge away from the Consumers. It uses the [TerraformGenerator] to
 * generate new Terraform files. The facade creates a temporary output directory for each operation, which is deleted
 * after the file is generated.
 */
@ApplicationScoped
class TerraformFacade {
    @Inject
    private lateinit var generator: TerraformGenerator

    /**
     * Create a new courseSeries by generating the Terraform stack.
     *
     * @param event The event that contains the information to create the course series.
     *
     * @return The result of the command.
     */
    @WithSpan
    fun createCourseSeries(event: CreateCourseSeriesEvent) =
        generate { outputDirectory ->
            generator.generateCourseSeriesConfig(event, outputDirectory)
        }

    /**
     * Create a new CourseInstance by generating the Terraform stack.
     *
     * @param event The event that contains the information to create the course instance.
     *
     * @return The result of the command.
     */
    @WithSpan
    fun createCourseInstance(event: CreateCourseInstanceEvent) =
        generate { outputDirectory ->
            generator.generateCourseInstanceConfig(event, outputDirectory)
        }

    /**
     * Create the student groups for the course instance by generating the Terraform stack.
     *
     * @param event The event that contains the information to update the student groups.
     *
     * @return The result of the command.
     */
    @WithSpan
    fun updateStudentGroups(event: UpdateStudentGroupsEvent) =
        generate { outputDirectory ->
            generator.generateStudentGroupsConfig(event, outputDirectory)
        }

    /**
     * Create the enrollments for the course instance by generating the Terraform stack.
     *
     * @param event The event that contains the information to update the enrollments.
     *
     * @return The result of the command.
     */
    @WithSpan
    fun updateEnrollments(event: UpdateEnrollmentsEvent) =
        generate { outputDirectory ->
            generator.generateEnrollmentsConfig(event, outputDirectory)
        }

    /**
     * Generate the Terraform file in a temporary directory.
     *
     * @param generator The function that generates the Terraform file.
     */
    private fun generate(generator: (Path) -> TerraformResult): TerraformResult =
        withEphemeralDirectory { outputDirectory ->
            return generator(outputDirectory)
        }
}
