package nl.rug.digitallab.repoman.worker.gitlab.command.impl

import jakarta.validation.Valid
import nl.rug.digitallab.repoman.common.events.operations.UpdateStudentGroupsEvent
import nl.rug.digitallab.repoman.worker.gitlab.terraform.TerraformFacade
import nl.rug.digitallab.repoman.worker.gitlab.command.Command

/**
 * A Terraform command that updates the student groups of a course instance.
 *
 * @param event The event that contains the information to update the student groups.
 * @param terraformFacade The Terraform facade to execute the command.
 */
data class UpdateStudentGroupsCommand(
    @field:Valid val event: UpdateStudentGroupsEvent,
    private val terraformFacade: TerraformFacade,
) : Command {
    override fun execute() = terraformFacade.updateStudentGroups(event)

    override fun toString(): String {
        return "UpdateStudentGroupsCommand(event=$event)"
    }
}
