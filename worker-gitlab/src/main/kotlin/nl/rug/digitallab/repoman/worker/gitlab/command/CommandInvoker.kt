package nl.rug.digitallab.repoman.worker.gitlab.command

import io.opentelemetry.api.trace.Span
import io.opentelemetry.api.trace.StatusCode
import io.opentelemetry.instrumentation.annotations.WithSpan
import io.quarkus.arc.All
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.worker.gitlab.exceptions.FailedCommandException
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.CommandFilter
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.CommandFilterChain

/**
 * Invokes a Terraform command. The command is executed through a filter chain.
 */
@ApplicationScoped
class CommandInvoker {
    @Inject
    @All
    private lateinit var filters: MutableList<CommandFilter> // Filters are sorted by priority.

    /**
     * Executes a Terraform command. Execution consists of:
     * 1. Building the filter chain.
     * 2. Running the filter chain.
     * 3. Executing the command after the last filter is applied.
     * 4. Returning the result of the command to each filter.
     *
     * @param command The Terraform command to execute.
     *
     * @return The result of the command.
     *
     * @throws FailedCommandException If the command execution fails.
     */
    @WithSpan
    fun execute(command: Command): CommandResult {
        val commandFilterChain = CommandFilterChain(filters)

        return try {
            commandFilterChain.execute(command)
        } catch (e: Exception) {
            Span.current()
                .setStatus(StatusCode.ERROR)

            throw FailedCommandException("Failed to execute Terraform command: ${e.message}", e)
        }
    }
}
