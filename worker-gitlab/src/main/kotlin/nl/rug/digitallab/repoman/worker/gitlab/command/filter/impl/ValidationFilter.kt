package nl.rug.digitallab.repoman.worker.gitlab.command.filter.impl

import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.annotation.Priority
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.validation.Validator
import nl.rug.digitallab.repoman.worker.gitlab.command.Command
import nl.rug.digitallab.repoman.worker.gitlab.command.CommandResult
import nl.rug.digitallab.repoman.worker.gitlab.validator.GitLabValidations
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.CommandFilter
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.RunCommandFilter
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.VALIDATION_FILTER_PRIORITY
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.CreateCourseInstanceCommand
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.CreateCourseSeriesCommand
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.UpdateEnrollmentsCommand
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.UpdateStudentGroupsCommand
import org.jboss.logging.Logger

/**
 * A filter that checks the validity of the input of the commands. Each command has its own validation rules. Note that
 * we are stricter than the GitLab guidelines, as we require POSIX portable strings for the course series and instance
 * slugs and names. GitLab's guidelines: https://docs.gitlab.com/ee/user/reserved_names.html
 */
@ApplicationScoped
@Priority(VALIDATION_FILTER_PRIORITY)
class ValidationFilter : CommandFilter {
    @Inject
    private lateinit var log: Logger

    @Inject
    private lateinit var validations: GitLabValidations

    @Inject
    private lateinit var validator: Validator

    private val branchPrefix = "course"

    override val name = "ValidationFilter"

    @WithSpan
    override fun filter(command: Command, nextFilter: RunCommandFilter): CommandResult {
        // Perform general validations that do not need interaction with the GitLab API
        val constraintViolations = validator.validate(command)
        require(constraintViolations.isEmpty()) {
            "Command ${command::class.simpleName} is invalid: ${constraintViolations.joinToString(", ") { it.message }}"
        }

        // Perform command specific validations that need interaction with the GitLab API
        when (command) {
            is CreateCourseSeriesCommand -> validateCreateCourseSeriesCommand(command)
            is CreateCourseInstanceCommand -> validateCreateCourseInstanceCommand(command)
            is UpdateStudentGroupsCommand -> validateUpdateStudentGroupsCommand(command)
            is UpdateEnrollmentsCommand -> validateUpdateEnrollmentsCommand(command)
            else -> log.debug("No validation rules for command ${command::class.simpleName}")
        }

        return nextFilter(command)
    }

    /**
     * Validate commands of type [CreateCourseSeriesCommand].
     *
     * @param command The command to validate.
     *
     * @throws IllegalArgumentException if the command is invalid.
     */
    private fun validateCreateCourseSeriesCommand(command: CreateCourseSeriesCommand) {
        // We cannot validate the optional groupId since the token does not have the required permissions to
        // check the existence of the group, which is usually a private group outside our own organization.
    }

    /**
     * Validate commands of type [CreateCourseInstanceCommand].
     *
     * @param command The command to validate.
     *
     * @throws IllegalArgumentException if the command is invalid.
     * @throws IllegalStateException if the command is invalid.
     */
    private fun validateCreateCourseInstanceCommand(command: CreateCourseInstanceCommand) {
        val courseSeriesSlug = command.event.courseSeriesSlug

        validations.checkBranchExists("$branchPrefix/$courseSeriesSlug")
        validations.checkFileExists(branchName = "$branchPrefix/$courseSeriesSlug", fileName = "cs_$courseSeriesSlug.tf.json")
    }

    /**
     * Validate commands of type [UpdateStudentGroupsCommand].
     *
     * @param command The command to validate.
     *
     * @throws IllegalArgumentException if the command is invalid.
     * @throws IllegalStateException if the command is invalid.
     */
    private fun validateUpdateStudentGroupsCommand(command: UpdateStudentGroupsCommand) {
        val courseSeriesSlug = command.event.courseSeriesSlug
        val courseInstanceSlug = command.event.courseInstanceSlug

        validations.checkBranchExists("$branchPrefix/$courseSeriesSlug")
        validations.checkFileExists(
            branchName = "$branchPrefix/$courseSeriesSlug",
            fileName = "ci_$courseInstanceSlug.tf.json"
        )
    }

    /**
     * Validate commands of type [UpdateEnrollmentsCommand].
     *
     * @param command The command to validate.
     *
     * @throws IllegalArgumentException if the command is invalid.
     * @throws IllegalStateException if the command is invalid.
     */
    private fun validateUpdateEnrollmentsCommand(command: UpdateEnrollmentsCommand) {
        val courseSeriesSlug = command.event.courseSeriesSlug
        val courseInstanceSlug = command.event.courseInstanceSlug

        validations.checkBranchExists("$branchPrefix/$courseSeriesSlug")
        validations.checkFileExists(
            branchName = "$branchPrefix/$courseSeriesSlug",
            fileName = "ci_$courseInstanceSlug.tf.json"
        )
        validations.checkFileExists(
            branchName = "$branchPrefix/$courseSeriesSlug",
            fileName = "ci_${courseInstanceSlug}_groups.tf.json"
        )
    }
}
