package nl.rug.digitallab.repoman.worker.gitlab.enums

/**
 * Interface for a base course instance resource. The resource is a group or a project.
 *
 * @param T The type of the resource.
 * @property slug The slug of the resource.
 * @property displayName The display name of the resource.
 * @property description The description of the resource.
 * @property defaultSettings The default settings of the resource.
 */
interface BaseCourseInstanceResource<T> {
    val slug: String
    val displayName: String // "name" is already a property of an enum
    val description: String
    val defaultSettings: (T) -> T
}
