package nl.rug.digitallab.repoman.worker.gitlab.exceptions

import nl.rug.digitallab.repoman.common.ErrorId

/**
 * Exception that is thrown when a command has failed.
 *
 * @property message The message that describes the failure.
 * @property cause The cause of the failure.
 * @property errorId A unique identifier for this specific failure.
 */
class FailedCommandException(
    message: String,
    cause: Throwable,
    val errorId: ErrorId = ErrorId.randomUUID(),
) : Exception(message, cause)
