package nl.rug.digitallab.repoman.worker.gitlab

import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.inject.Produces
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.worker.gitlab.configs.ArtifactsConfig
import org.gitlab4j.api.GitLabApi

/**
 * Factory for producing GitLabApi instances that can be injected.
 */
@ApplicationScoped
class GitLabApiFactory {
    @Inject
    private lateinit var artifactsConfig: ArtifactsConfig

    /**
     * Produce a new GitLabApi instance.
     *
     * @return A new GitLabApi instance.
     */
    @Produces
    @ApplicationScoped
    fun produceGitLabApi(): GitLabApi {
        return GitLabApi(artifactsConfig.gitlabUrl.toString(), artifactsConfig.projectAccessToken)
    }
}

