package nl.rug.digitallab.repoman.worker.gitlab.terraform.stacks

import com.hashicorp.cdktf.App
import com.hashicorp.cdktf.TerraformLocal
import com.hashicorp.cdktf.TerraformStack
import com.hashicorp.cdktf.TerraformVariable
import com.hashicorp.cdktf.providers.gitlab.provider.GitlabProvider

/**
 * Abstract Terraform RepoManStack class which contains parts that always need to be present such as the Providers.
 *
 * @param scope The App scope.
 * @param id The ID of the stack.
 */
abstract class RepoManStack(
    private val scope: App,
    val id: String,
) : TerraformStack(scope, id) {
    /**
     * Create the artifacts of the Terraform RepoManStack.
     */
    fun synth() {
        // IMPORTANT: The GitLab provider is removed from the stack before uploading to GitLab. This is done by the
        // AdaptFilter. Instead, the provider is added to the main.tf.json file. However, we keep the
        // provider here to ensure Terraform can generate the stacks, and for local testing purposes.
        val token = TerraformVariable.Builder.create(this, "access-token")
            .type("string")
            .build()
            .stringValue

        GitlabProvider.Builder.create(this, "gitlab-provider")
            .token(token)
            .baseUrl("https://gitlab.com")
            .build()

        scope.synth()
    }

    /**
     * Format IDs of resources in a consistent way in accordance with: https://www.terraform-best-practices.com/naming
     *
     * @param text The text to format.
     *
     * @return The formatted text.
     */
    protected fun id(text: () -> String): String {
        return text()
            .replace("-", "_")
    }

    /**
     * Create a local variable in Terraform.
     *
     * @param name The name of the local variable.
     * @param value The value of the local variable.
     */
    protected fun createLocal(name: String, value: String) {
        TerraformLocal(this, name, value)
    }
}
