package nl.rug.digitallab.repoman.worker.gitlab.terraform.stacks

import com.hashicorp.cdktf.*
import com.hashicorp.cdktf.providers.gitlab.branch_protection.BranchProtection
import com.hashicorp.cdktf.providers.gitlab.group.Group
import com.hashicorp.cdktf.providers.gitlab.group_access_token.GroupAccessToken
import com.hashicorp.cdktf.providers.gitlab.project.Project
import com.hashicorp.cdktf.providers.gitlab.project_access_token.ProjectAccessToken
import com.hashicorp.cdktf.providers.gitlab.project_variable.ProjectVariable
import com.hashicorp.cdktf.providers.gitlab.repository_file.RepositoryFile
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.repoman.common.events.configurations.CourseInstanceConfiguration
import nl.rug.digitallab.repoman.worker.gitlab.enums.BaseCourseInstanceGroup
import nl.rug.digitallab.repoman.worker.gitlab.enums.BaseCourseInstanceProject
import nl.rug.digitallab.repoman.worker.gitlab.terraform.courseInstanceBaseGroupDefaults
import nl.rug.digitallab.repoman.worker.gitlab.terraform.courseInstanceBaseRepositoryDefaults
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi

/**
 * Class representing a course instance as a RepoManStack, which is a Terraform CDK concept.
 * One stack represents one Terraform project or module. IDs must be unique in a RepoManStack.
 *
 * @param courseSeriesSlug The slug of the course series.
 * @param courseInstanceName The name of the course instance.
 * @param courseInstanceSlug The slug of the course instance.
 * @param courseInstanceConfiguration The configuration of the course instance.
 */
class CourseInstanceStack(
    scope: App,
    private val courseSeriesSlug: String,
    private val courseInstanceName: String,
    private val courseInstanceSlug: String,
    courseInstanceConfiguration: CourseInstanceConfiguration
) : RepoManStack(scope, "ci_$courseInstanceSlug") {
    private val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

    init {
        // Adds a data point for the existing course series group based on the full path
        val courseInstance = createMainCourseInstanceGroup()
        val baseGroups = createBaseGroups(courseInstance)
        val baseRepositories = createBaseRepositories(courseInstance)

        val studentResources = baseGroups.getValue(BaseCourseInstanceGroup.STUDENT_RESOURCES)
        val teacherResources = baseGroups.getValue(BaseCourseInstanceGroup.TEACHER_RESOURCES)
        val students = baseGroups.getValue(BaseCourseInstanceGroup.STUDENTS)
        val configuration = baseRepositories.getValue(BaseCourseInstanceProject.CONFIGURATION)
        val artifacts = baseRepositories.getValue(BaseCourseInstanceProject.ARTIFACTS)

        // Do not create general issues project if it is disabled in the configuration
        if(courseInstanceConfiguration.createGeneralIssues) {
            val slug = "general-issues"
            val displayName = "General Issues"
            val description = "This project contains an issue board where students can create issues. " +
                    "This can be used as a forum for general questions."

            Project.Builder.create(this, id { "ci_${courseInstanceSlug}_${slug}" })
                // Enable issues
                .issuesAccessLevel("enabled")
                .issuesEnabled(true)
                // Disable all other features
                .analyticsAccessLevel("disabled")
                .buildsAccessLevel("disabled")
                .containerRegistryAccessLevel("disabled")
                .environmentsAccessLevel("disabled")
                .featureFlagsAccessLevel("disabled")
                .infrastructureAccessLevel("disabled")
                .monitorAccessLevel("disabled")
                .mergeRequestsEnabled(false)
                .mergeRequestsAccessLevel("disabled")
                .packagesEnabled(false)
                .pagesAccessLevel("disabled")
                .releasesAccessLevel("disabled")
                .repositoryAccessLevel("disabled")
                .requirementsAccessLevel("disabled")
                .securityAndComplianceAccessLevel("disabled")
                .snippetsEnabled(false)
                .wikiEnabled(false)
                .wikiAccessLevel("disabled")
                .name(displayName)
                .path(slug)
                .description(description)
                .namespaceId(Token.asNumber(studentResources.id))
                .build()
        }

        initializeConfiguration(configuration, artifacts)
        initializeArtifacts(
            artifacts,
            courseInstance,
            studentResources,
            students,
            teacherResources,
        )

        // Create local variables for the group IDs to be used in other stacks while
        // we remain agnostic of whether the groups are resources or data objects.
        createLocal(id { "ci_${courseInstanceSlug}_student_resources_group_id" }, studentResources.id)
        createLocal(id { "ci_${courseInstanceSlug}_teacher_resources_group_id" }, teacherResources.id)
        createLocal(id { "ci_${courseInstanceSlug}_students_group_id" }, students.id)
    }

    /**
     * Create the main course instance group within the course series.
     *
     * @return The ID of the created course instance group.
     */
    private fun createMainCourseInstanceGroup(): Group =
        Group.Builder.create(this, id { "ci_$courseInstanceSlug" })
            .name(courseInstanceName)
            .path(courseInstanceSlug)
            .parentId(Token.asNumber("\${local.cs_group_id}"))
            .build()

    /**
     * Create base groups within the course instance.
     *
     * @param courseInstance The course instance group.
     *
     * @return The IDs of the created base groups.
     */
    private fun createBaseGroups(courseInstance: Group): Map<BaseCourseInstanceGroup, Group> =
        BaseCourseInstanceGroup.entries.associateWith {
            Group.Builder.create(this, id { "ci_${courseInstanceSlug}_${it.slug}" })
                .courseInstanceBaseGroupDefaults(it)
                .name(it.displayName)
                .path(it.slug)
                .description(it.description)
                .parentId(Token.asNumber(courseInstance.id))
                .build()
        }

    /**
     * Create base repositories within the course instance.
     *
     * @param courseInstance The course instance group.
     *
     * @return The IDs of the created base repositories.
     */
    private fun createBaseRepositories(courseInstance: Group): Map<BaseCourseInstanceProject, Project> {
        return BaseCourseInstanceProject.entries.associateWith {
            val project = Project.Builder.create(this, id { "ci_${courseInstanceSlug}_${it.slug}" })
                .courseInstanceBaseRepositoryDefaults(it)
                .name(it.displayName)
                .path(it.slug)
                .description(it.description)
                .namespaceId(Token.asNumber(courseInstance.id))

            // The artifacts repository should have a default branch that matches the course series slug
            if (it == BaseCourseInstanceProject.ARTIFACTS)
                project.defaultBranch("course/${courseSeriesSlug}")

            return@associateWith project.build()
        }
    }

    /**
     * Initialize the artifacts repository with the GitLab CI configuration.
     *
     * @param artifactsRepository The artifacts repository.
     * @param courseInstance The course instance group.
     * @param studentResourcesGroup The student resources group.
     * @param studentsGroup The students group.
     * @param teacherResourcesGroup The teacher resources group.
     *
     * @return The ID of the created file.
     */
    private fun initializeArtifacts(
        artifactsRepository: Project,
        courseInstance: Group,
        studentResourcesGroup: Group,
        studentsGroup: Group,
        teacherResourcesGroup: Group,
    ) {
        val branchName = "course/${courseSeriesSlug}"

        // Create the .gitlab-ci.yml file in the artifacts repository
        createRepositoryFile(".gitlab-ci.yml", getResource("templates/artifacts/.gitlab-ci.yml").readText(), branchName, artifactsRepository)

        // Create the main.tf.json file in the artifacts repository
        createRepositoryFile("main.tf.json", getResource("templates/artifacts/main.tf.json").readText(), branchName, artifactsRepository)

        // Create the ci_${courseInstanceSlug}.tf.json file in the artifacts repository of the course instance. This
        // is created as an empty file, as the gitlab-worker checks for the presence of this file. This is useful when
        // operating in the "global" artifacts repository, but not in the course instance artifacts repository. Sooner
        // or later we will converge to a single solution, but for now we want to support both global art and course
        // instance artifacts repositories.
        val ciFileContent = getResource("templates/artifacts/ci.tf.json")
            .readText()
            .replace("\$CI_SLUG\$", id { courseInstanceSlug })

        createRepositoryFile("ci_${courseInstanceSlug}.tf.json", ciFileContent, branchName, artifactsRepository)

        // Generate YYY-MM-DD expiration date that is 1 year from now
        val expirationDate = LocalDate.now().plusYears(1).format(formatter)

        // Create a group access token on the course instance level
        val courseInstanceAccessToken = GroupAccessToken.Builder.create(this, id { "ci_${courseInstanceSlug}_course_instance_access_token" })
            .accessLevel("owner")
            .scopes(listOf("api"))
            .name("Access Token for Artifacts Repository")
            .group(courseInstance.id)
            .expiresAt(expirationDate)
            .build()

        // The artifacts repository needs the course instance group access token to be able to create resources
        createProjectVariable("TF_VAR_GROUP_ACCESS_TOKEN", courseInstanceAccessToken.token, artifactsRepository)

        // Create the project variables used to populate the locals
        // - TF_VAR_STUDENT_RESOURCES_GROUP_ID
        // - TF_VAR_STUDENTS_GROUP_ID
        // - TF_VAR_TEACHER_RESOURCES_GROUP_ID
        createProjectVariable("TF_VAR_STUDENT_RESOURCES_GROUP_ID", studentResourcesGroup.id, artifactsRepository)
        createProjectVariable("TF_VAR_STUDENTS_GROUP_ID", studentsGroup.id, artifactsRepository)
        createProjectVariable("TF_VAR_TEACHER_RESOURCES_GROUP_ID", teacherResourcesGroup.id, artifactsRepository)
    }

    /**
     * Initialize the configuration repository with the GitLab CI configuration.
     *
     * @param configurationRepository The configuration repository.
     * @param artifactsRepository The artifacts repository.
     *
     * @return The IDs of the created files.
     */
    private fun initializeConfiguration(configurationRepository: Project, artifactsRepository: Project) {
        // Allow pushes to main by developers, there is only one branch in the configuration repository
        BranchProtection.Builder.create(this, id { "ci_${courseInstanceSlug}_artifacts_main_branch_protection" })
            .branch("main")
            .pushAccessLevel("developer")
            .project(configurationRepository.id)
            .build()

        // Create the configuration.yml file in the configuration repository
        createRepositoryFile("config.example.yml", getResource("templates/configuration/config.example.yml").readText(), "main", configurationRepository)

        // Create the .gitlab-ci.yml file in the configuration repository
        createRepositoryFile(".gitlab-ci.yml", getResource("templates/configuration/.gitlab-ci.yml").readText(), "main", configurationRepository)

        // Create the docker-compose.yml file in the configuration repository
        createRepositoryFile("docker-compose.yml", getResource("templates/configuration/docker-compose.yml").readText(), "main", configurationRepository)

        // Generate YYY-MM-DD expiration date that is 1 year from now
        val expirationDate = LocalDate.now().plusYears(1).format(formatter)

        // Create a project access token for the artifacts repository
        val artifactsAccessToken = ProjectAccessToken.Builder.create(this, id { "ci_${courseInstanceSlug}_artifacts_access_token" })
            .accessLevel("owner")
            .scopes(listOf("api"))
            .name("Access Token for Artifacts Repository")
            .project(artifactsRepository.id)
            .expiresAt(expirationDate)
            .build()

        // Configure the project variables for running RepoMan in the configuration repository
        // - ARTIFACTS_PROJECT_ACCESS_TOKEN
        // - ARTIFACTS_GITLAB_URL
        // - ARTIFACTS_PROJECT_ID
        createProjectVariable("ARTIFACTS_PROJECT_ACCESS_TOKEN", artifactsAccessToken.token, configurationRepository)
        createProjectVariable("ARTIFACTS_GITLAB_URL", "https://gitlab.com", configurationRepository)
        createProjectVariable("ARTIFACTS_PROJECT_ID", artifactsRepository.id, configurationRepository)

        // Configure the project variables for the curl POST request to the RepoMan API in the CI pipeline
        // - COURSE_SERIES
        // - COURSE_INSTANCE
        createProjectVariable("COURSE_SERIES", courseSeriesSlug, configurationRepository, isMasked = false)
        createProjectVariable("COURSE_INSTANCE", courseInstanceSlug, configurationRepository, isMasked = false)
    }

    /**
     * Helper function to create a project variable in Terraform.
     *
     * @param key The key of the project variable.
     * @param value The value of the project variable.
     * @param targetProject The project where the variable should be created.
     * @param isMasked Whether the variable should be masked.
     */
    private fun createProjectVariable(
        key: String,
        value: String,
        targetProject: Project,
        isMasked: Boolean = true,
    ) {
        require(!isMasked || value.length >= 8) {
            "The value of the project variable $key is too short to be masked."
        }

        ProjectVariable.Builder.create(this, id { "ci_${courseInstanceSlug}_${targetProject.pathInput}_${key.lowercase()}" })
            .key(key)
            .value(value)
            .variableType("env_var")
            .masked(isMasked)
            .project(targetProject.id)
            .build()
    }

    /**
     * Helper function to create a repository file in Terraform.
     *
     * @param filePath The path of the file to create.
     * @param content The content of the file.
     * @param branch The branch to create the file in.
     * @param targetProject The project where the file should be created.
     */
    private fun createRepositoryFile(filePath: String, content: String, branch: String, targetProject: Project): RepositoryFile {
        return RepositoryFile.Builder.create(this, id { "ci_${courseInstanceSlug}_${targetProject.pathInput}_$filePath" })
            .repositoryFileDefaults()
            .branch(branch)
            .commitMessage("Initialize $filePath")
            .content(content.encodeToBase64())
            .encoding("base64")
            .filePath(filePath)
            .project(targetProject.id)
            .build()
    }
}

/**
 * Encode a string to Base64. This is useful to prevent Terraform from parsing references in a file, this can happen
 * when you use Terraform to create another Terraform file in a repository based on some template.
 *
 * @return The Base64 encoded string.
 */
@OptIn(ExperimentalEncodingApi::class)
private fun String.encodeToBase64(): String =
    Base64.encode(this
        .replace("\r\n", "\n")
        .toByteArray()
    )


/**
 * Add the default settings for a RepositoryFile to the builder.
 *
 * @return The builder with the default settings added.
 */
private fun RepositoryFile.Builder.repositoryFileDefaults(): RepositoryFile.Builder =
    this.authorName("RepoMan")
        .authorEmail("digitallab@rug.nl")
        .encoding("text")
        .overwriteOnCreate(true)
