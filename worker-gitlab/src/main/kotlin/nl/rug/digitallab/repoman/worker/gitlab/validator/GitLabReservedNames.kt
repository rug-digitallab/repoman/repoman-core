package nl.rug.digitallab.repoman.worker.gitlab.validator

// See: https://docs.gitlab.com/ee/user/reserved_names.html#reserved-group-names
private val reservedNames = setOf(
    "\\-", "badges", "blame", "blob", "builds", "commits", "create", "create_dir", "edit",
    "environments/folders", "files", "find_file", "gitlab-lfs/objects", "info/lfs/objects",
    "new", "preview", "raw", "refs", "tree", "update", "wikis", ".well-known", "404.html",
    "422.html", "500.html", "502.html", "503.html", "admin", "api", "apple-touch-icon.png",
    "assets", "dashboard", "deploy.html", "explore", "favicon.ico", "favicon.png", "files",
    "groups", "health_check", "help", "import", "jwt", "login", "oauth", "profile", "projects",
    "public", "robots.txt", "s", "search", "sitemap", "sitemap.xml", "sitemap.xml.gz",
    "slash-command-logo.png", "snippets", "unsubscribes", "uploads", "users", "v2"
)

fun CharSequence.isReservedByGitLab() = this in reservedNames
