package nl.rug.digitallab.repoman.worker.gitlab.terraform

import com.hashicorp.cdktf.providers.gitlab.branch_protection.BranchProtection
import com.hashicorp.cdktf.providers.gitlab.group.Group
import com.hashicorp.cdktf.providers.gitlab.project.Project
import nl.rug.digitallab.repoman.worker.gitlab.enums.BaseCourseInstanceGroup
import nl.rug.digitallab.repoman.worker.gitlab.enums.BaseCourseInstanceProject
import org.gitlab4j.api.models.AccessLevel

/**
 * Set the default settings of a base course instance group.
 *
 * @param baseCourseInstanceGroup The base group for which the default settings should be applied.
 *
 * @return The [Group.Builder] with the default settings applied.
 */
fun Group.Builder.courseInstanceBaseGroupDefaults(baseCourseInstanceGroup: BaseCourseInstanceGroup): Group.Builder {
    return baseCourseInstanceGroup.defaultSettings(this)
}

/**
 * Set the default settings of a base course instance repository.
 *
 * @param baseCourseInstanceProject The base repository for which the default settings should be applied.
 *
 * @return The [Project.Builder] with the default settings applied.
 */
fun Project.Builder.courseInstanceBaseRepositoryDefaults(baseCourseInstanceProject: BaseCourseInstanceProject): Project.Builder {
    return baseCourseInstanceProject.defaultSettings(this)
}

/**
 * Set the default settings for the student main branch.
 *
 * @return The [Group.Builder] with the default settings applied.
 */
fun BranchProtection.Builder.studentMainBranchDefaults(): BranchProtection.Builder {
    return this
        .allowForcePush(false)
        .pushAccessLevel("no one") // Note: We cannot use AccessLevel.NONE, as it is not the same as "no one"
        .mergeAccessLevel(AccessLevel.MAINTAINER.lowercase())
}

/**
 * Set the default settings for student projects.
 *
 * @return The [Project.Builder] with the default settings applied.
 */
fun Project.Builder.studentProjectDefaults(): Project.Builder {
    return this
        .defaultBranch("main")
        .visibilityLevel("private")
}

/**
 * Get the AccessLevel enum name in lowercase.
 *
 * @return The lowercase name of the AccessLevel.
 */
fun AccessLevel.lowercase() = this.name.lowercase()
