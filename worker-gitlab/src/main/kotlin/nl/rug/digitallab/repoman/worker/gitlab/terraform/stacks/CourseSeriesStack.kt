package nl.rug.digitallab.repoman.worker.gitlab.terraform.stacks

import com.hashicorp.cdktf.App
import com.hashicorp.cdktf.Token
import com.hashicorp.cdktf.providers.gitlab.data_gitlab_group.DataGitlabGroup
import com.hashicorp.cdktf.providers.gitlab.group.Group
import nl.rug.digitallab.repoman.common.GroupId

/**
 * Class representing a course series as a RepoManStack, which is a Terraform CDK concept.
 * One stack represents one Terraform project or module. IDs must be unique in a RepoManStack.
 *
 * @param courseSeriesName The name of the course series.
 * @param courseSeriesSlug The slug of the course series.
 * @param parentId The ID of the parent group to which the course series belongs.
 * @param groupId The ID of the existing GitLab group if it already exists.
 */
class CourseSeriesStack(
    scope: App,
    private val courseSeriesName: String,
    private val courseSeriesSlug: String,
    parentId: GroupId,
    groupId: GroupId?,
) : RepoManStack(scope, "cs_$courseSeriesSlug") {
    init {
        val courseSeriesGroupId = if (groupId == null)
                createCourseSeriesGroupResource(parentId).id
            else
                createCourseSeriesGroupData(groupId).id

        createLocal("cs_group_id", courseSeriesGroupId)
    }

    /**git
     * Create a new course series group in the stack.
     *
     * @param parentId Group ID of the parent to which the course series belongs.
     *
     * @return The ID of the group.
     */
    private fun createCourseSeriesGroupResource(parentId: GroupId): Group =
        Group.Builder.create(this, id { "cs_$courseSeriesSlug" })
            .name(courseSeriesName)
            .path(courseSeriesSlug)
            .parentId(parentId)
            .build()

    /**
     * Creates a data object to reference the existing GitLab group as a course series group.
     *
     * @param groupId The group id of the already existing GitLab group.
     *
     * @return The ID of the group.
     */
    private fun createCourseSeriesGroupData(groupId: GroupId): DataGitlabGroup =
        DataGitlabGroup.Builder.create(this, id { "cs_$courseSeriesSlug" })
            .groupId(Token.asNumber(groupId))
            .build()
}
