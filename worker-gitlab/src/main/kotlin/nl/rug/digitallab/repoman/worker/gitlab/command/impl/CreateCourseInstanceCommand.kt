package nl.rug.digitallab.repoman.worker.gitlab.command.impl

import jakarta.validation.Valid
import nl.rug.digitallab.repoman.common.events.operations.CreateCourseInstanceEvent
import nl.rug.digitallab.repoman.worker.gitlab.terraform.TerraformFacade
import nl.rug.digitallab.repoman.worker.gitlab.command.Command

/**
 * A Terraform command that creates a course instance.
 *
 * @param event The event that contains the information to create the course instance.
 * @param terraformFacade The Terraform facade to execute the command.
 */
data class CreateCourseInstanceCommand(
    @field:Valid val event: CreateCourseInstanceEvent,
    private val terraformFacade: TerraformFacade,
) : Command {
    override fun execute() =
        terraformFacade.createCourseInstance(event)

    override fun toString(): String {
        return "CreateCourseInstanceCommand(event=$event)"
    }
}
