package nl.rug.digitallab.repoman.worker.gitlab.workers

import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.infrastructure.Infrastructure
import io.vertx.mutiny.core.Context
import io.vertx.mutiny.core.Vertx
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.common.events.operations.*
import nl.rug.digitallab.repoman.worker.gitlab.command.CommandInvoker
import nl.rug.digitallab.repoman.common.events.operations.OperationEvent
import nl.rug.digitallab.repoman.worker.gitlab.terraform.TerraformFacade
import nl.rug.digitallab.repoman.worker.gitlab.command.Command
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.CreateCourseInstanceCommand
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.CreateCourseSeriesCommand
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.UpdateEnrollmentsCommand
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.UpdateStudentGroupsCommand
import nl.rug.digitallab.repoman.worker.gitlab.exceptions.FailedCommandException
import org.eclipse.microprofile.reactive.messaging.Incoming
import org.jboss.logging.Logger

/**
 * The [OperationConsumer] consumes operation events. It handles the different types of events and calls the
 * appropriate methods in the [TerraformFacade]. It also sends status updates back to the appropriate channel.
 */
@ApplicationScoped
class OperationConsumer {
    @Inject
    private lateinit var producer: OperationStatusProducer

    @Inject
    private lateinit var terraformFacade: TerraformFacade

    @Inject
    private lateinit var commandInvoker: CommandInvoker

    @Inject
    private lateinit var log: Logger

    /**
     * Handles the incoming operation event. It first sends a started status update to the status channel, then
     * executes the corresponding command and sends a completed status update to the status channel. If an error occurs
     * during the execution of the command, a failed status update is sent to the status channel.
     *
     * @param event The incoming operation event.
     */
    @Incoming("worker-gitlab-operation")
    fun handleOperation(event: OperationEvent): Uni<Unit> {
        val context: Context = Vertx.currentContext()

        log.info("Received operation with id ${event.operationId} and type: ${event.operationType}")

        return Uni.createFrom()
            .item(producer.queueStartedStatus(event))
            .invoke { _ -> log.info("Handling ${event.operationType} event $event") }
            .emitOn(Infrastructure.getDefaultExecutor()) // Run blocking code on worker thread
            .map {
                // Get the corresponding command and execute it
                val command = event.toTerraformCommand()
                commandInvoker.execute(command)
            }
            .emitOn { runnable -> context.runOnContext { runnable.run() } } // Switch back to event-loop thread
            .flatMap {
                producer.queueCompletedStatus(event)
            }
            .onFailure().recoverWithUni { throwable ->
                log.error("Something went wrong whilst handling event $event: ${throwable.message}", throwable)

                // Collect the error ID if available
                val (message, errorId) = when (throwable) {
                    is FailedCommandException -> throwable.cause?.message to throwable.errorId
                    else -> throwable.message to null
                }

                producer.queueFailedStatus(event, message ?: "No error message", errorId)
            }
    }

    /**
     * Converts an [OperationEvent] to a [Command].
     *
     * @return The Terraform command.
     */
    fun OperationEvent.toTerraformCommand(): Command =
        when(this) {
            is CreateCourseSeriesEvent -> CreateCourseSeriesCommand(this, terraformFacade)
            is CreateCourseInstanceEvent -> CreateCourseInstanceCommand(this, terraformFacade)
            is UpdateStudentGroupsEvent -> UpdateStudentGroupsCommand(this, terraformFacade)
            is UpdateEnrollmentsEvent -> UpdateEnrollmentsCommand(this, terraformFacade)
            else -> throw IllegalArgumentException("Unknown event type: $this")
        }

}
