package nl.rug.digitallab.repoman.worker.gitlab

import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.inject.Produces
import jakarta.inject.Inject
import jakarta.inject.Named
import nl.rug.digitallab.repoman.worker.gitlab.configs.ArtifactsConfig

/**
 * Factory for producing Throttler instances that can be injected.
 */
@ApplicationScoped
class ThrottlerFactory {
    @Inject
    private lateinit var artifactsConfig: ArtifactsConfig

    /**
     * Produce a new Throttler instance for the GitLab API.
     *
     * @return A new Throttler instance.
     */
    @Produces
    @Named("gitlab")
    @ApplicationScoped
    fun produceGitLabApiThrottler(): Throttler {
        return Throttler(artifactsConfig.gitlabRateLimitPerSecond)
    }
}
