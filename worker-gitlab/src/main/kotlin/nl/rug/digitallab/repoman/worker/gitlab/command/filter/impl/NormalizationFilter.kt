package nl.rug.digitallab.repoman.worker.gitlab.command.filter.impl

import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.annotation.Priority
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.worker.gitlab.command.Command
import nl.rug.digitallab.repoman.worker.gitlab.command.CommandResult
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.CommandFilter
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.NORMALIZATION_FILTER_PRIORITY
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.RunCommandFilter
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.UpdateEnrollmentsCommand
import nl.rug.digitallab.repoman.worker.gitlab.validator.GitLabValidations
import org.jboss.logging.Logger

/**
 * A filter that normalizes the input of the commands. Each command has its own normalization rules.
 */
@ApplicationScoped
@Priority(NORMALIZATION_FILTER_PRIORITY)
class NormalizationFilter : CommandFilter {
    @Inject
    private lateinit var log: Logger

    @Inject
    private lateinit var validations: GitLabValidations

    override val name = "NormalizationFilter"

    @WithSpan
    override fun filter(command: Command, nextFilter: RunCommandFilter): CommandResult {
        // Perform command specific validations that need interaction with the GitLab API
        val normalizedCommand = when (command) {
            is UpdateEnrollmentsCommand -> normalizeUpdateEnrollmentsCommand(command)
            else -> {
                log.debug("No normalization rules for command ${command::class.simpleName}")
                command
            }
        }

        return nextFilter(normalizedCommand)
    }

    /**
     * Normalize the [UpdateEnrollmentsCommand].
     *
     * @param command The command to normalize.
     *
     * @return The normalized command.
     */
    private fun normalizeUpdateEnrollmentsCommand(command: UpdateEnrollmentsCommand): Command {
        return command
            .normalizeHandles()
            .normalizeInvalidHandles()
    }

    /**
     * Normalize the handles in the [UpdateEnrollmentsCommand]. This is done by removing the '@' prefix.
     *
     * @return The normalized command.
     *
     * @see UpdateEnrollmentsCommand
     */
    private fun UpdateEnrollmentsCommand.normalizeHandles(): UpdateEnrollmentsCommand {
        val enrollments = event.enrollments

        val normalizedEnrollments = enrollments.copy(
            maintainers = enrollments.maintainers.map { it.copy(handle = it.handle.normalizeHandle()) },
            teachers = enrollments.teachers.map { it.copy(handle = it.handle.normalizeHandle()) },
            students = enrollments.students.map { it.copy(handle = it.handle.normalizeHandle()) },
        )

        return copy(event = event.copy(enrollments = normalizedEnrollments))
    }

    /**
     * Normalize the handle by removing the '@' prefix.
     *
     * @return The normalized string.
     */
    private fun String.normalizeHandle(): String {
        return this.removePrefix("@")
    }

    /**
     * Normalize the invalid handles in the [UpdateEnrollmentsCommand]. This is done by removing the invalid
     * handles from the list of enrollments, or fails if the configuration specifies to fail on invalid handles.
     *
     * @return The normalized command.
     */
    private fun UpdateEnrollmentsCommand.normalizeInvalidHandles(): UpdateEnrollmentsCommand {
        val enrollments = with(event.enrollments) {
            listOf(maintainers, teachers, students)
        }.flatten()

        val invalidHandles =
            validations
                .requireAllHandlesExist(enrollments, event.configuration.failOnInvalidHandles)
                .map { it.handle }

        return if(invalidHandles.isNotEmpty()) {
            // Remove invalid handles from the list of enrollments
            val updatedEnrollments =
                with(event.enrollments) {
                    copy(
                        maintainers = maintainers.filterNot { invalidHandles.contains(it.handle) },
                        teachers = teachers.filterNot { invalidHandles.contains(it.handle) },
                        students = students.filterNot { invalidHandles.contains(it.handle) }
                    )
                }

            copy(event = event.copy(enrollments = updatedEnrollments))
        } else {
            this
        }
    }
}
