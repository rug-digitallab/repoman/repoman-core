package nl.rug.digitallab.repoman.worker.gitlab.workers

import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.replaceWithUnit
import io.smallrye.reactive.messaging.MutinyEmitter
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.common.ErrorId
import nl.rug.digitallab.repoman.common.events.operations.OperationEvent
import nl.rug.digitallab.repoman.common.events.operations.status.CompletedStatusEvent
import nl.rug.digitallab.repoman.common.events.operations.status.FailedStatusEvent
import nl.rug.digitallab.repoman.common.events.operations.status.OperationStatusEvent
import nl.rug.digitallab.repoman.common.events.operations.status.StartedStatusEvent
import org.eclipse.microprofile.reactive.messaging.Channel
import org.jboss.logging.Logger

/**
 * Application-scoped Kafka producer class that sends status events back to the repositories-service.
 */
@ApplicationScoped
class OperationStatusProducer {
    @Inject
    @Channel("worker-status")
    private lateinit var workerStatusEmitter: MutinyEmitter<OperationStatusEvent>

    @Inject
    private lateinit var log: Logger

    /**
     * Queues a STARTED status update for the specified event.
     *
     * @param operationEvent The operation event for which to queue the status update.
     */
    fun queueStartedStatus(operationEvent: OperationEvent): Uni<Unit>
        = queueUpdate(StartedStatusEvent(operationEvent))


    /**
     * Queues a COMPLETED status update for the specified event.
     *
     * @param operationEvent The operation event for which to queue the status update.
     */
    fun queueCompletedStatus(operationEvent: OperationEvent): Uni<Unit> =
        queueUpdate(CompletedStatusEvent(operationEvent))

    /**
     * Queues a FAILED status update for the specified event.
     *
     * @param operationEvent The operation event for which to queue the status update.
     * @param errorId The error ID to propagate. Null if no error ID is available.
     */
    fun queueFailedStatus(operationEvent: OperationEvent, message: String, errorId: ErrorId?): Uni<Unit> =
        queueUpdate(FailedStatusEvent(operationEvent, message, errorId))

    /**
     * Queues a status update for the specified event.
     *
     * @param operationStatusEvent The status event to queue.
     */
    private fun queueUpdate(operationStatusEvent: OperationStatusEvent): Uni<Unit> {

        log.info("Sending operation event on queue: $operationStatusEvent")

        return workerStatusEmitter
            .send(operationStatusEvent)
            .replaceWithUnit()
            .onFailure().invoke { e -> log.error("Failed to send operation event on queue: $operationStatusEvent", e) }
    }
}
