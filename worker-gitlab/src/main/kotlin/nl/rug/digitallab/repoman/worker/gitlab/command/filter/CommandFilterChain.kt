package nl.rug.digitallab.repoman.worker.gitlab.command.filter

import nl.rug.digitallab.repoman.worker.gitlab.command.Command
import org.jboss.logging.Logger

/**
 * A chain of [CommandFilter]s that can be applied to a [Command]. Filters are applied in the order they are provided.
 * After all filters have been applied, the command is executed. The result of the command is passed back to each filter
 * in reverse order of application.
 *
 * @param filters The list of filters to apply.
 */
class CommandFilterChain(private val filters: List<CommandFilter>) {
    private val log: Logger = Logger.getLogger(this::class.java)

    // The command execution function that is called when all filters have been applied
    private val executeCommand: RunCommandFilter = { command ->
        log.debug("All filter applied, executing command: ${command::class.qualifiedName}")
        command.execute()
    }

    val execute: RunCommandFilter = filters.foldRight(executeCommand, ::nextFilter)

    private fun nextFilter(filter: CommandFilter, nextFilter: RunCommandFilter): RunCommandFilter = { command ->
        log.debug("Applying filter: ${filter.name}")
        val result = filter.filter(command, nextFilter)
        log.debug("Applied filter: ${filter.name}")

        result
    }
}

