package nl.rug.digitallab.repoman.worker.gitlab

import com.google.common.util.concurrent.RateLimiter
import org.jetbrains.annotations.Blocking

/**
 * A simple rate limiter that throttles calls to a specified rate.
 *
 * @param callsPerSecond The number of calls per second to throttle to.
 */
class Throttler(callsPerSecond: Double) {
    private val rateLimiter = RateLimiter.create(callsPerSecond)

    /**
     * Call a method, throttling the calls to the specified rate. This method will block until the call is allowed.
     *
     * @param T The return type of the method.
     * @param method The method to call.
     *
     * @return The result of the method.
     */
    @Blocking
    operator fun <T> invoke(method: () -> T): T {
        rateLimiter.acquire()
        return method()
    }
}
