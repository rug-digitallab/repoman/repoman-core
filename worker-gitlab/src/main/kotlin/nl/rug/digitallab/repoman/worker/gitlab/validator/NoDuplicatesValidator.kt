package nl.rug.digitallab.repoman.worker.gitlab.validator

import jakarta.validation.ConstraintValidator
import jakarta.validation.ConstraintValidatorContext
import nl.rug.digitallab.repoman.common.events.constraints.NoDuplicates

/**
 * Constraint validator for [NoDuplicates].
 *
 * Constraints:
 * - No duplicates are allowed in the list.
 */
class NoDuplicatesValidator: ConstraintValidator<NoDuplicates, List<*>> {
    override fun isValid(value: List<*>?, context: ConstraintValidatorContext?): Boolean {
        context?.disableDefaultConstraintViolation() // We ignore the default violation message from the annotation

        // Null values are considered valid, as they should be validated by @NotNull
        if (value == null)
            return true

        // Find all duplicates
        val duplicates = value.groupBy { it }.filterValues { it.size > 1 }.mapValues { it.value.size }

        // No duplicates found
        if(duplicates.isEmpty())
            return true

        // Log the duplicates
        duplicates.forEach { (k, v) ->
            context?.buildConstraintViolationWithTemplate("Duplicate value '$k' found $v times")
                ?.addConstraintViolation()
        }

        return false
    }
}
