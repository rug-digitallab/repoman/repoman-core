package nl.rug.digitallab.repoman.worker.gitlab.command.filter.impl

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.annotation.Priority
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.worker.gitlab.command.Command
import nl.rug.digitallab.repoman.worker.gitlab.command.CommandResult
import nl.rug.digitallab.repoman.worker.gitlab.command.TerraformResult
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.ADAPT_FILTER_PRIORITY
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.CommandFilter
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.RunCommandFilter

/**
 * The [AdaptFilter] is responsible for adapting the Terraform files before they are uploaded to GitLab.
 * This is done by removing conflicting tags and prettifying the JSON. This allows the use of multiple stacks in a
 * single directory.
 */
@ApplicationScoped
@Priority(ADAPT_FILTER_PRIORITY)
class AdaptFilter: CommandFilter {
    @Inject
    private lateinit var objectMapper: ObjectMapper

    override val name = "AdaptFilter"

    // These are the tags that are removed from the JSON. As they are provided by a main.tf.json file in the root.
    private val toRemove = listOf(
        "//",         // Not necessary, but reduces the file size.
        "variable",   // Must be removed
        "terraform",  // Must be removed
        "provider",   // Must be removed
    )

    @WithSpan
    override fun filter(command: Command, nextFilter: RunCommandFilter): CommandResult {
        // Continue the filter chain
        return when(val result = nextFilter(command)) {
            is TerraformResult -> result.copy(stackContent = adapt(result.stackContent)) // Adapt the Terraform file
            else -> result // Only adapt Terraform files
        }
    }

    /**
     * Adapts the Terraform file by removing the specified to-be-removed properties.
     *
     * @param terraformContent The content of the Terraform file.
     *
     * @return The adapted JSON content.
     */
    private fun adapt(terraformContent: String): String {
        val jsonObject = readJsonObject(terraformContent)
        val adaptedJsonNode = adaptJsonObject(jsonObject)

        return prettify(adaptedJsonNode)
    }

    /**
     * Reads the JSON file from the provided path.
     *
     * @param jsonContent The JSON content as a string.
     *
     * @return The JSON content as an ObjectNode.
     */
    private fun readJsonObject(jsonContent: String): ObjectNode =
        objectMapper.readTree(jsonContent) as ObjectNode

    /**
     * Adapts the [ObjectNode] by removing the specified to-be-removed properties.
     *
     * @param jsonNode The to be adapted node.
     *
     * @return The adapted [ObjectNode] containing the adapted JSON.
     */
    private fun adaptJsonObject(jsonNode: ObjectNode): ObjectNode {
        val adaptedNode = jsonNode.deepCopy() // Deep copy so the original is not modified.
        return walkAndAdapt(adaptedNode)
    }

    /**
     * Walk through all JSON nodes and adapt each node by removing the specified to-be-removed properties.
     *
     * @param jsonNode The [ObjectNode] to be walked through.
     *
     * @return The adapted [ObjectNode].
     */
    private fun walkAndAdapt(jsonNode: ObjectNode): ObjectNode {
        jsonNode.fields().forEach { (_, value) ->
            if (value !is ObjectNode)
                return@forEach

            walkAndAdapt(value)
        }

        toRemove.forEach { jsonNode.remove(it) }

        return jsonNode
    }

    /**
     * Prettifies the JsonNode and returns it as a string.
     *
     * @param jsonNode The JSON node to be prettified.
     *
     * @return The prettified string content of the jsonNode.
     */
    private fun prettify(jsonNode: ObjectNode): String =
        objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode)
}
