package nl.rug.digitallab.repoman.worker.gitlab.command

/**
 * The result of executing a command.
 */
interface CommandResult

/**
 * The result of executing a Terraform command.
 *
 * @param stackId The id of the stack, which is based on its slug(s).
 * @param stackContent The content of the stack.
 */
data class TerraformResult(
    val stackId: String,
    val stackContent: String
): CommandResult
