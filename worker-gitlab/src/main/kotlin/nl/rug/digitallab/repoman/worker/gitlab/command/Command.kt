package nl.rug.digitallab.repoman.worker.gitlab.command

/**
 * A command that can be executed through the Terraform facade.
 */
interface Command {
    /**
     * Executes the command.
     *
     * @return The result of the command.
     */
    fun execute(): CommandResult
}
