package nl.rug.digitallab.repoman.worker.gitlab.configs

import io.quarkus.runtime.annotations.StaticInitSafe
import io.smallrye.config.ConfigMapping
import nl.rug.digitallab.repoman.common.ProjectId
import java.net.URL

/**
 * @property gitlabUrl The GitLab instance URL.
 * @property gitlabRateLimitPerSecond The rate limit of the GitLab instance.
 * @property projectId The project id of the GitLab artifacts project.
 * @property projectAccessToken The project access token of the GitLab artifacts project.
 * @property mainBranch The name of the branch to branch off from.
 * @property retryTimeoutMs The retry timeout in milliseconds between upload attempts.
 */
@StaticInitSafe
@ConfigMapping(prefix = "digital-lab.worker-gitlab.artifacts")
interface ArtifactsConfig {
    val gitlabUrl: URL
    val gitlabRateLimitPerSecond: Double
    val projectId: ProjectId
    val projectAccessToken: String
    val mainBranch: String
    val retryTimeoutMs: Long
}


