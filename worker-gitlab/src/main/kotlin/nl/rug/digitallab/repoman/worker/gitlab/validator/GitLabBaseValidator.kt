package nl.rug.digitallab.repoman.worker.gitlab.validator

import jakarta.validation.ConstraintValidator
import jakarta.validation.ConstraintValidatorContext
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext
import org.hibernate.validator.internal.engine.messageinterpolation.util.InterpolationHelper
import java.util.regex.Pattern

/**
 * Base constraint validator for GitLab group and slug names.
 */
abstract class GitLabBaseValidator<A : Annotation>(pattern: String) : ConstraintValidator<A, CharSequence?> {
    private val compiledPattern = Pattern.compile(pattern)
    private val escapedPattern = InterpolationHelper.escapeMessageParameter(compiledPattern.pattern())

    protected fun constraintViolationMessage(context: ConstraintValidatorContext?, message: String) {
        context?.disableDefaultConstraintViolation() // We ignore the default violation message from the annotation
        context
            ?.buildConstraintViolationWithTemplate(message)
            ?.addConstraintViolation()
    }

    override fun isValid(value: CharSequence?, context: ConstraintValidatorContext?): Boolean {
        // Null values are considered valid, as they should be validated by @NotNull
        if (value == null)
            return true

        // Value cannot be a reserved name.
        if(value.isReservedByGitLab()) {
            constraintViolationMessage(context, "'$value' is a reserved name!")
            return false
        }

        // Check if the value matches the pattern
        if(!compiledPattern.matcher(value).matches()) {
            constraintViolationMessage(context, "'$value' has an invalid format")

            // Add the pattern to the message parameters for interpolation
            if (context is HibernateConstraintValidatorContext) {
                context
                    .unwrap(HibernateConstraintValidatorContext::class.java)
                    ?.addMessageParameter("regexp", escapedPattern)
            }

            return false
        }

        // Valid value
        return true
    }
}
