package nl.rug.digitallab.repoman.worker.gitlab.command.filter.impl

import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.annotation.Priority
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.worker.gitlab.GitLabUploader
import nl.rug.digitallab.repoman.worker.gitlab.command.Command
import nl.rug.digitallab.repoman.worker.gitlab.command.CommandResult
import nl.rug.digitallab.repoman.worker.gitlab.command.TerraformResult
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.CommandFilter
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.RunCommandFilter
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.UPLOAD_FILTER_PRIORITY
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.CreateCourseInstanceCommand
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.CreateCourseSeriesCommand
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.UpdateEnrollmentsCommand
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.UpdateStudentGroupsCommand

/**
 * A filter that uploads the Terraform stack to GitLab after the command has been executed. Has a high priority, so it
 * receives the result of the command last.
 */
@ApplicationScoped
@Priority(UPLOAD_FILTER_PRIORITY)
class UploadFilter: CommandFilter {
    private val terraformFileExtension = "tf.json"
    private val branchPrefix = "course"

    @Inject
    private lateinit var uploader: GitLabUploader

    override val name = "UploadFilter"

    @WithSpan
    override fun filter(command: Command, nextFilter: RunCommandFilter): CommandResult {
        // Extracts the course series slug from the command that are relevant for the upload
        val courseSeriesSlug = when (command) {
            is CreateCourseSeriesCommand -> command.event.courseSeriesSlug
            is CreateCourseInstanceCommand -> command.event.courseSeriesSlug
            is UpdateStudentGroupsCommand -> command.event.courseSeriesSlug
            is UpdateEnrollmentsCommand -> command.event.courseSeriesSlug
            else -> return nextFilter(command)
        }

        // Continue the filter chain
        val result = nextFilter(command)

        when(result) {
            is TerraformResult -> {
                // Upload the Terraform stack to GitLab
                val branchName = "$branchPrefix/$courseSeriesSlug"
                val fileName = "${result.stackId}.$terraformFileExtension"
                uploader.uploadWithRetries(branchName, fileName, result.stackContent)
            }
        }

        return result
    }
}
