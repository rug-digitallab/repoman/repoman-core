package nl.rug.digitallab.repoman.worker.gitlab.enums

import com.hashicorp.cdktf.providers.gitlab.group.Group
import com.hashicorp.cdktf.providers.gitlab.group.GroupPushRules
import nl.rug.digitallab.repoman.worker.gitlab.terraform.lowercase
import org.gitlab4j.api.models.AccessLevel

/**
 * The base course instance GitLab groups that should always be created for every course instance.
 *
 * @property slug The slug name of the group - used in URLs.
 * @property displayName The user-friendly name of the group as shown in GitLab UI.
 * @property description Optional description of the group shown in GitLab UI.
 * @property defaultSettings Expects a [Group.Builder] and returns a [Group.Builder] with the default group settings.
 */
enum class BaseCourseInstanceGroup(
    override val slug: String,
    override val displayName: String, // "name" is already a property of an enum
    override val description: String = "",
    override val defaultSettings: (Group.Builder) -> Group.Builder,
) : BaseCourseInstanceResource<Group.Builder> {
    /**
     * STUDENTS - Group for the student groups and their projects.
     */
    STUDENTS(
        slug = "students",
        displayName = "Students",
        description = "This group contains subgroups per (group of) students. " +
                "Students only have access to their own group.",
        { builder ->
            builder
                .lfsEnabled(false)
                .projectCreationLevel(AccessLevel.DEVELOPER.lowercase())
                .subgroupCreationLevel(AccessLevel.OWNER.lowercase())
                .pushRules(
                    GroupPushRules.Builder()
                        .preventSecrets(true)
                        .build()
                )
        },
    ),

    /**
     * TEACHER_RESOURCES - Group for private resources shared with the teachers of the course.
     */
    TEACHER_RESOURCES(
        slug = "teacher-resources",
        displayName = "Teacher Resources",
        description = "This group contains resources accessible to all " +
                "teachers of the course, but is not visible to students. " +
                "This can be used to centralise course resources." +
                "This group is also the place to create GitLab templates.",
        { builder -> builder },
    ),

    /**
     * STUDENT_RESOURCES - Group for resources shared with the students.
     */
    STUDENT_RESOURCES(
        slug = "student-resources",
        displayName = "Student Resources",
        description = "This group contains resources accessible to " +
                "everyone in the course, students and teachers. " +
                "This can be used to share public artifacts for use in the course.",
        { builder -> builder },
    ),
}
