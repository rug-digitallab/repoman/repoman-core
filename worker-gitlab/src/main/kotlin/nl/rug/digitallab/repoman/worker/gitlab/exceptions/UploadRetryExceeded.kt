package nl.rug.digitallab.repoman.worker.gitlab.exceptions

import java.lang.RuntimeException

class UploadRetryExceeded(courseSeriesSlug: String, override val cause: Throwable? = null)
    : RuntimeException("Unable to upload file for $courseSeriesSlug. Maximum number of retries was reached.", cause)
