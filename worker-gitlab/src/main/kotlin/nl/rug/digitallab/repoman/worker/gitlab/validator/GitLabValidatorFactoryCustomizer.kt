package nl.rug.digitallab.repoman.worker.gitlab.validator

import io.quarkus.hibernate.validator.ValidatorFactoryCustomizer
import jakarta.enterprise.context.ApplicationScoped
import jakarta.validation.ConstraintValidator
import nl.rug.digitallab.repoman.common.events.constraints.GroupName
import nl.rug.digitallab.repoman.common.events.constraints.GroupSlug
import nl.rug.digitallab.repoman.common.events.constraints.NoDuplicates
import org.hibernate.validator.BaseHibernateValidatorConfiguration

/**
 * Customizes the Hibernate Validator factory to include custom validators for the GroupName and GroupSlug constraints.
 * As these constraints differ per platform, no default validators are provided. Instead, the platform-specific
 * implementations are configured at runtime.
 */
@ApplicationScoped
class GitLabValidatorFactoryCustomizer : ValidatorFactoryCustomizer {
    override fun customize(configuration: BaseHibernateValidatorConfiguration<*>) {
        fun <T : Annotation> addMapping(annotationClass: Class<T>, validatorClass: Class<out ConstraintValidator<T, *>>) {
            configuration.createConstraintMapping().apply {
                constraintDefinition(annotationClass)
                    .includeExistingValidators(false)
                    .validatedBy(validatorClass)
                configuration.addMapping(this)
            }
        }

        addMapping(GroupName::class.java, GitLabGroupNameValidator::class.java)
        addMapping(GroupSlug::class.java, GitLabGroupSlugValidator::class.java)
        addMapping(NoDuplicates::class.java, NoDuplicatesValidator::class.java)
    }
}
