package nl.rug.digitallab.repoman.worker.gitlab.validator

import jakarta.validation.ConstraintValidatorContext
import nl.rug.digitallab.repoman.common.events.constraints.GroupSlug

/**
 * Constraint validator for [GroupSlug].
 *
 * Constraints:
 * - Must start with a letter (a-zA-Z) or digit (0-9).
 * - Must not contain consecutive special characters.
 * - Cannot start or end with a special character.
 * - Can contain only letters (a-zA-Z), digits (0-9), underscores (_), dots (.), or dashes (-).
 * - Cannot end in .git or .atom.
 *
 * https://docs.gitlab.com/ee/user/reserved_names.html#limitations-on-usernames-project-and-group-names
 */
class GitLabGroupSlugValidator :
    GitLabBaseValidator<GroupSlug>("^(?=[a-zA-Z0-9])[a-zA-Z0-9]+(?:(?![_.-]{2,})[a-zA-Z0-9_.-])*[a-zA-Z0-9]\$")
{
    override fun isValid(value: CharSequence?, context: ConstraintValidatorContext?): Boolean {
        // Null values are considered valid, as they should be validated by @NotNull
        if (value == null)
            return true

        // Cannot end in .git or .atom.
        if(value.endsWith(".git") || value.endsWith(".atom")) {
            constraintViolationMessage(context, "Group slug '$value' cannot end in .git or .atom")
            return false
        }

        return super.isValid(value, context)
    }
}
