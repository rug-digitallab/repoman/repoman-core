package nl.rug.digitallab.repoman.worker.gitlab.configs

import io.quarkus.runtime.annotations.StaticInitSafe
import io.smallrye.config.ConfigMapping
import nl.rug.digitallab.repoman.common.GroupId

/**
 * @property groupId The group id of the GitLab courses group.
 */
@StaticInitSafe
@ConfigMapping(prefix = "digital-lab.worker-gitlab.courses")
interface CoursesConfig {
    val groupId: GroupId
}
