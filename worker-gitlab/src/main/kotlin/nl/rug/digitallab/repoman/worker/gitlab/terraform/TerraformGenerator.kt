package nl.rug.digitallab.repoman.worker.gitlab.terraform

import com.hashicorp.cdktf.App
import com.hashicorp.cdktf.AppConfig
import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.common.events.operations.CreateCourseInstanceEvent
import nl.rug.digitallab.repoman.common.events.operations.CreateCourseSeriesEvent
import nl.rug.digitallab.repoman.common.events.operations.UpdateEnrollmentsEvent
import nl.rug.digitallab.repoman.common.events.operations.UpdateStudentGroupsEvent
import nl.rug.digitallab.repoman.worker.gitlab.configs.CoursesConfig
import nl.rug.digitallab.repoman.worker.gitlab.command.CommandResult
import nl.rug.digitallab.repoman.worker.gitlab.command.TerraformResult
import nl.rug.digitallab.repoman.worker.gitlab.terraform.stacks.CourseSeriesStack
import nl.rug.digitallab.repoman.worker.gitlab.terraform.stacks.CourseInstanceStack
import nl.rug.digitallab.repoman.worker.gitlab.terraform.stacks.EnrollmentsStack
import nl.rug.digitallab.repoman.worker.gitlab.terraform.stacks.StudentGroupsStack
import org.jboss.logging.Logger
import java.nio.file.Path
import kotlin.io.path.readText

/**
 * The [TerraformGenerator] is responsible for generating Terraform stacks using [CourseSeriesStack],
 * [CourseInstanceStack], and [StudentGroupsStack].
 */
@ApplicationScoped
class TerraformGenerator {
    @Inject
    private lateinit var log: Logger

    @Inject
    private lateinit var coursesConfig: CoursesConfig

    /**
     * Generate a Terraform stack for a course series
     *
     * @param event The event that contains the information to create the course series.
     *
     * @return The [CommandResult] of generating the Terraform stack.
     */
    @WithSpan
    fun generateCourseSeriesConfig(event: CreateCourseSeriesEvent, outputDirectory: Path): TerraformResult {
        log.info("Creating Terraform config for courseSeries ${event.courseSeriesName}")

        val courseSeriesStack = CourseSeriesStack(
            App(getAppConfig(outputDirectory)),
            event.courseSeriesName,
            event.courseSeriesSlug,
            coursesConfig.groupId,
            event.groupId,
        )
        courseSeriesStack.synth()

        return TerraformResult(
            stackId = courseSeriesStack.id,
            stackContent = getStackContent(outputDirectory, courseSeriesStack.id),
        )
    }

    /**
     * Create a Terraform RepoManStack for a course instance.
     *
     * @param event The event that contains the information to create the course instance.
     *
     * @return The [CommandResult] of generating the Terraform stack.
     */
    @WithSpan
    fun generateCourseInstanceConfig(event: CreateCourseInstanceEvent, outputDirectory: Path): TerraformResult {
        log.info("Creating Terraform config for courseInstance ${event.courseInstanceName} of courseSeries ${event.courseSeriesSlug}")

        val courseInstanceStack = CourseInstanceStack(
            App(getAppConfig(outputDirectory)),
            event.courseSeriesSlug,
            event.courseInstanceName,
            event.courseInstanceSlug,
            event.configuration,
        )
        courseInstanceStack.synth()

        return TerraformResult(
            stackId = courseInstanceStack.id,
            stackContent = getStackContent(outputDirectory, courseInstanceStack.id),
        )
    }

    /**
     * Create a Terraform RepoManStack for the course instance student groups.
     *
     * @param event The event that contains the information to update the student groups.
     *
     * @return The [CommandResult] of generating the Terraform stack.
     */
    @WithSpan
    fun generateStudentGroupsConfig(event: UpdateStudentGroupsEvent, outputDirectory: Path): TerraformResult {
        log.info("Creating Terraform config for student groups of courseInstance ${event.courseInstanceSlug} of courseSeries ${event.courseSeriesSlug}")

        val studentGroupsStack = StudentGroupsStack(
            App(getAppConfig(outputDirectory)),
            event.courseInstanceSlug,
            event.groups,
            event.configuration,
        )
        studentGroupsStack.synth()

        return TerraformResult(
            stackId = studentGroupsStack.id,
            stackContent = getStackContent(outputDirectory, studentGroupsStack.id),
        )
    }

    /**
     * Create a Terraform RepoManStack for the course instance enrollments.
     *
     * @param event The event that contains the information to update the enrollments.
     *
     * @return The [CommandResult] of generating the Terraform stack.
     */
    @WithSpan
    fun generateEnrollmentsConfig(event: UpdateEnrollmentsEvent, outputDirectory: Path): TerraformResult {
        log.info("Creating Terraform config for enrollments of courseInstance ${event.courseInstanceSlug} of courseSeries ${event.courseSeriesSlug}")

        val enrollmentsStack = EnrollmentsStack(
            App(getAppConfig(outputDirectory)),
            event.courseInstanceSlug,
            event.enrollments,
            event.configuration,
        )
        enrollmentsStack.synth()

        return TerraformResult(
            stackId = enrollmentsStack.id,
            stackContent = getStackContent(outputDirectory, enrollmentsStack.id),
        )
    }

    /**
     * Get the configuration used by Terraform.
     * At this moment it simply sets the output directory.
     *
     * @param outputDirectory The directory where the Terraform stack should be generated.
     *
     * @return The [AppConfig] used by Terraform.
     */
    private fun getAppConfig(outputDirectory: Path): AppConfig {
        return AppConfig.builder()
            .outdir(outputDirectory.toString())
            .build()
    }

    /**
     * Returns the content of the stack file based on the output directory and the stack id.
     *
     * @param outputDirectory The directory where the Terraform stack should be generated.
     * @param stackId Name of the stack, which is based on its slug(s).
     *
     * @return The content of the stack file.
     */
    private fun getStackContent(outputDirectory: Path, stackId: String): String {
        val tfFileName = "cdk.tf.json" // Default of cdk output files.
        return outputDirectory.resolve("stacks/$stackId/$tfFileName").readText()
    }
}
