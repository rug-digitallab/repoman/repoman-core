package nl.rug.digitallab.repoman.worker.gitlab.validator

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.common.GroupId
import nl.rug.digitallab.repoman.common.dtos.Enrollment
import nl.rug.digitallab.repoman.worker.gitlab.Throttler
import nl.rug.digitallab.repoman.worker.gitlab.configs.ArtifactsConfig
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.User
import org.jboss.logging.Logger
import kotlin.jvm.optionals.getOrNull

/**
 * Defines the GitLab validations that can be performed using the GitLab API.
 */
@ApplicationScoped
class GitLabValidations {
    @Inject
    private lateinit var gitLabApi: GitLabApi

    @Inject
    private lateinit var throttle: Throttler

    @Inject
    private lateinit var artifactsConfig: ArtifactsConfig

    @Inject
    private lateinit var log: Logger

    /**
     * Check if a branch exists in the GitLab artifact repository.
     *
     * @param branchName The name of the branch to check.
     *
     * @throws IllegalStateException if the branch does not exist.
     */
    fun checkBranchExists(branchName: String) {
        check(throttle { gitLabApi.repositoryApi.getOptionalBranch(artifactsConfig.projectId, branchName).getOrNull() } != null) {
            "Branch '$branchName' does not exist."
        }
    }

    /**
     * Check if a file exists in the GitLab artifact repository.
     *
     * @param branchName The name of the branch to check.
     * @param fileName   The name of the file to check.
     *
     * @throws IllegalStateException if the file does not exist in the branch.
     */
    fun checkFileExists(branchName: String, fileName: String) {
        check(throttle { gitLabApi.repositoryFileApi.getOptionalFile(artifactsConfig.projectId, fileName, branchName).getOrNull() } != null) {
            "File '$fileName' does not exist in branch '$branchName'."
        }
    }

    /**
     * Check if a group exists in GitLab.
     *
     * @param groupId The ID of the group to check.
     *
     * @throws IllegalStateException if the group does not exist.
     */
    fun checkGroupExists(groupId: GroupId) {
        check(throttle { gitLabApi.groupApi.getOptionalGroup(groupId).getOrNull() } != null) {
            "Group ID '$groupId' does not exist."
        }
    }

    /**
     * Require all handles in a list of enrollments to exist in GitLab.
     *
     * @param enrollments The list of enrollments to check.
     * @param failOnInvalidHandles Whether to fail if an invalid handle is found.
     *
     * @return The list of invalid handles.
     *
     * @throws IllegalArgumentException if a handle does not exist in GitLab.
     */
    fun requireAllHandlesExist(enrollments: List<Enrollment>, failOnInvalidHandles: Boolean): List<HandleValidation> {
        // Merge all handles into a set of unique handles, and check if these handles exist
        val handleValidations =
            enrollments
                .map { it.handle }
                .toSet()
                .map {
                    HandleValidation(it, throttle { gitLabApi.userApi.getOptionalUser(it).getOrNull() })
                }

        // Check if all handles exist in GitLab
        val invalidHandles = handleValidations.filter { it.user == null }

        val warning = "The following handles do not exist in GitLab: ${invalidHandles.joinToString { it.handle }}"
        if(failOnInvalidHandles)
            require(invalidHandles.isEmpty()) { warning }
        else if(invalidHandles.isNotEmpty())
            log.warn(warning)

        return invalidHandles
    }

    /**
     * Helper class to store the handle and whether it exists in GitLab.
     *
     * @property handle The handle that was checked.
     * @property user The user that was found in GitLab, or null if the user does not exist.
     */
    data class HandleValidation(val handle: String, val user: User?)
}
