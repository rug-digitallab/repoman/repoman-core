package nl.rug.digitallab.repoman.worker.gitlab.command.impl

import jakarta.validation.Valid
import nl.rug.digitallab.repoman.common.events.operations.CreateCourseSeriesEvent
import nl.rug.digitallab.repoman.worker.gitlab.terraform.TerraformFacade
import nl.rug.digitallab.repoman.worker.gitlab.command.Command

/**
 * A Terraform command that creates a course series.
 *
 * @param event The event that contains the information to create the course series.
 * @param terraformFacade The Terraform facade to execute the command.
 */
data class CreateCourseSeriesCommand(
    @field:Valid val event: CreateCourseSeriesEvent,
    private val terraformFacade: TerraformFacade,
) : Command {
    override fun execute() =
        terraformFacade.createCourseSeries(event)

    override fun toString(): String {
        return "CreateCourseSeriesCommand(event=$event)"
    }
}
