package nl.rug.digitallab.repoman.worker.gitlab

import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.inject.Named
import nl.rug.digitallab.repoman.worker.gitlab.configs.ArtifactsConfig
import nl.rug.digitallab.repoman.worker.gitlab.exceptions.UploadRetryExceeded
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.GitLabApiException
import org.gitlab4j.api.models.RepositoryFile
import org.jboss.logging.Logger
import java.lang.Exception
import java.lang.Thread.sleep

/**
 * GitLab uploader that uploads the content of a file to a file in a branch on GitLab.
 */
@ApplicationScoped
class GitLabUploader {
    @Inject
    private lateinit var log: Logger

    @Inject
    private lateinit var artifactsConfig: ArtifactsConfig

    @Inject
    private lateinit var gitlabApi: GitLabApi

    @Inject
    @Named("gitlab")
    private lateinit var throttle: Throttler

    /**
     * Recursive function that tries to upload the file content to GitLab a given number of times.
     *
     * @param branchName Name of the branch to which the file should be uploaded.
     * @param fileName Name of the to be uploaded file.
     * @param content Content that should be written to the file.
     * @param remainingRetries The number of retries left.
     *
     * @throws UploadRetryExceeded exception thrown when the max number of retries is reached.
     */
    @WithSpan
    fun uploadWithRetries(branchName: String, fileName: String, content: String, remainingRetries: Int = 3) {
        try {
            upload(branchName, fileName, content)
        } catch (e: Exception) {
            if (remainingRetries > 0) {
                log.warn("Something went wrong whilst uploading $fileName to branch $branchName on GitLab. $remainingRetries retries left. Exception: ${e.message}")
                sleep(artifactsConfig.retryTimeoutMs)
                uploadWithRetries(branchName, fileName, content, remainingRetries - 1)
            } else {
                log.error("Something went wrong whilst uploading files to GitLab: ${e.message}", e)
                throw UploadRetryExceeded(branchName, e)
            }
        }
    }

    /**
     * Creates the branch if it does not exist and uploads the content to the file in the branch.
     *
     * @param branchName Name of the branch to which the file should be uploaded.
     * @param fileName Name of the to be uploaded file.
     * @param content Content that should be written to the file.
     */
    private fun upload(branchName:String, fileName: String, content: String) {
        createBranchIfNotExists(branchName)
        uploadFile(branchName, fileName, content)
    }

    /**
     * Checks whether the branch exists, if not it creates it.
     *
     * @param branchName Name of the branch to check.
     */
    private fun createBranchIfNotExists(branchName: String) {
        val repositoryApi = gitlabApi.repositoryApi
        val branches = throttle { repositoryApi.getBranches(artifactsConfig.projectId) }
        val branchExists = branches.any { it.name == branchName }

        if (branchExists) return

        throttle { repositoryApi.createBranch(artifactsConfig.projectId, branchName, artifactsConfig.mainBranch) }
    }

    /**
     * Uploads the file content to a file in a branch on GitLab.
     *
     * @param branchName Name of the branch to which the file should be uploaded.
     * @param fileName Name of the to be uploaded file.
     * @param content Content that should be written to the file.
     */
    private fun uploadFile(branchName: String, fileName: String, content: String) {
        val repositoryFileApi = gitlabApi.repositoryFileApi

        val file = RepositoryFile()
        file.fileName = fileName
        file.filePath = fileName
        file.content = content

        try {
            throttle {
                repositoryFileApi.updateFile(artifactsConfig.projectId, file, branchName, "Updated file $fileName")
            }
            log.debug("Updated file $fileName in branch $branchName")
        } catch (_: GitLabApiException) {
            throttle {
                repositoryFileApi.createFile(artifactsConfig.projectId, file, branchName, "Updated file $fileName")
            }
            log.debug("Created file $fileName in branch $branchName")
        }
    }
}
