package nl.rug.digitallab.repoman.worker.gitlab.terraform.stacks

import com.hashicorp.cdktf.App
import com.hashicorp.cdktf.Token
import com.hashicorp.cdktf.providers.gitlab.data_gitlab_user.DataGitlabUser
import com.hashicorp.cdktf.providers.gitlab.group_membership.GroupMembership
import nl.rug.digitallab.repoman.common.dtos.*
import nl.rug.digitallab.repoman.common.enums.StudentPermissions
import nl.rug.digitallab.repoman.common.events.configurations.EnrollmentsConfiguration
import nl.rug.digitallab.repoman.worker.gitlab.enums.BaseCourseInstanceGroup
import nl.rug.digitallab.repoman.worker.gitlab.terraform.lowercase
import org.gitlab4j.api.models.AccessLevel

typealias UserDataCache = Map<String, DataGitlabUser>

/**
 * Class representing course instance enrollments as a RepoManStack, which is a Terraform CDK concept.
 * One stack represents one Terraform project or module. IDs must be unique in a RepoManStack.
 *
 * @param courseInstanceSlug The slug of the course instance for which we are creating enrollments.
 * @param enrollments The enrollments for the course instance.
 * @param enrollmentsConfiguration The configuration for the enrollments.
 */
class EnrollmentsStack(
    scope: App,
    private val courseInstanceSlug: String,
    enrollments: Enrollments,
    private val enrollmentsConfiguration: EnrollmentsConfiguration,
) : RepoManStack(scope, "ci_${courseInstanceSlug}_enrollments") {
    private val studentsGroupId = id { "\${local.ci_${courseInstanceSlug}_students_group_id}" }
    private val studentResourcesGroupId = id { "\${local.ci_${courseInstanceSlug}_student_resources_group_id}" }
    private val teacherResourcesGroupId = id { "\${local.ci_${courseInstanceSlug}_teacher_resources_group_id}" }

    init {
        // Create all user data references and cache them for later use.
        val userDataCache = createAllUserData(enrollments)

        createStudentMemberships(enrollments.students, userDataCache)
        createReviewerMemberships(enrollments.reviewers, userDataCache)
        createTeacherMemberships(enrollments.teachers, userDataCache)
        createMaintainerMemberships(enrollments.maintainers, userDataCache)
    }

    /**
     * Create all user data references for all handles in the course instance enrollments.
     *
     * @param users The users to create data references for.
     *
     * @return A map of user handles to their data references.
     */
    private fun createAllUserData(users: Enrollments): UserDataCache {
        return sequenceOf(users.students, users.reviewers, users.teachers, users.maintainers)
            .flatten()
            .map { it.handle }
            .toSet() // Unique handles only
            .associateWith { createUserData(it) }
    }

    /**
     * Creates a user data reference in terraform which we can use to reference them.
     *
     * @param handle - Handle for their GitLab account.
     */
    private fun createUserData(handle: String): DataGitlabUser {
        return DataGitlabUser.Builder.create(this, id { "ci_${courseInstanceSlug}_$handle" })
            .username(handle)
            .build()
    }

    /**
     * Creates the enrollments for all students.
     *
     * @param students - All students for which we want to create enrollments.
     * @param userDataCache - Cache of all user data references.
     */
    private fun createStudentMemberships(students: List<Student>, userDataCache: UserDataCache) {
        // Executed for each enrollment in the list of students, even if students have multiple enrollments:
        // - Adds the student to their group if they have a group assigned.
        students
            .filter { it.groupSlug != null }
            .forEach { student ->
                val studentData = userDataCache.findUser(student.handle)
                val groupId = Token.asString(id { "\${gitlab_group.ci_${courseInstanceSlug}_${student.groupSlug!!}.id}" })

                // Check the configured student permissions and assign the correct role.
                val studentRole = when(enrollmentsConfiguration.studentPermissions) {
                    StudentPermissions.FULL -> AccessLevel.MAINTAINER
                    StudentPermissions.LIMITED -> AccessLevel.DEVELOPER
                }

                addUserToGroupId(studentData, student.handle, student.groupSlug!!, groupId, studentRole)
            }

        // Executed only once for each student:
        // - Adds the student to the student resources group.
        students
            .uniqueUsernames()
            .forEach { student ->
                val studentData = userDataCache.findUser(student.handle)
                addUserToGroupId(studentData, student.handle, BaseCourseInstanceGroup.STUDENT_RESOURCES.slug, studentResourcesGroupId, AccessLevel.REPORTER)
            }
    }

    /**
     * Creates the enrollments for all reviewers .
     *
     * @param reviewers - All reviewers for which we want to create enrollments.
     * @param userDataCache - Cache of all user data references.
     */
    private fun createReviewerMemberships(reviewers: List<Reviewer>, userDataCache: UserDataCache) {
        // Executed for each enrollment in the list of reviewers, even if reviewers have multiple enrollments:
        // - Adds the reviewer to their group.
        reviewers.forEach { reviewer ->
            val reviewerData = userDataCache.findUser(reviewer.handle)
            val groupId = Token.asString(id { "\${gitlab_group.ci_${courseInstanceSlug}_${reviewer.groupSlug}.id}" })
            addUserToGroupId(reviewerData, reviewer.handle, reviewer.groupSlug, groupId, AccessLevel.REPORTER)
        }
    }

    /**
     * Creates the enrollments for all teachers.
     *
     * @param teachers - All teachers for which we want to create enrollments.
     * @param userDataCache - Cache of all user data references.
     */
    private fun createTeacherMemberships(teachers: List<Teacher>, userDataCache: UserDataCache) {
        // Executed for each enrollment in the list of teachers, even if teachers have multiple enrollments.
        // - Adds the teacher to a specific student group if they have a group assigned.
        teachers
            .filter { it.groupSlug != null }
            .forEach { teacher ->
                val teacherData = userDataCache.findUser(teacher.handle)
                val groupId = Token.asString(id { "\${gitlab_group.ci_${courseInstanceSlug}_${teacher.groupSlug!!}.id}" })
                addUserToGroupId(teacherData, teacher.handle, teacher.groupSlug!!, groupId, AccessLevel.MAINTAINER)
            }

        // Executed only once for each teacher with no group slug:
        // - Adds the teacher to the overarching students group.
        teachers
            .filter { it.groupSlug == null }
            .uniqueUsernames()
            .forEach { teacher ->
                val teacherData = userDataCache.findUser(teacher.handle)
                addUserToGroupId(teacherData, teacher.handle, BaseCourseInstanceGroup.STUDENTS.slug, studentsGroupId, AccessLevel.MAINTAINER)
            }

        // Executed only once for each teacher.
        // - Adds the teacher to the student resources group.
        // - Adds the teacher to the teacher resources group.
        teachers
            .uniqueUsernames()
            .forEach { teacher ->
                val teacherData = userDataCache.findUser(teacher.handle)
                addUserToGroupId(teacherData, teacher.handle, BaseCourseInstanceGroup.STUDENT_RESOURCES.slug, studentResourcesGroupId, AccessLevel.DEVELOPER)
                addUserToGroupId(teacherData, teacher.handle, BaseCourseInstanceGroup.TEACHER_RESOURCES.slug, teacherResourcesGroupId, AccessLevel.REPORTER)
            }
    }

    /**
     * Creates the enrollments for all maintainers.
     *
     * @param maintainers - All maintainers for which we want to create enrollments.
     * @param userDataCache - Cache of all user data references.
     */
    private fun createMaintainerMemberships(maintainers: List<Maintainer>, userDataCache: UserDataCache) =
        maintainers.forEach { maintainer ->
            val maintainerData = userDataCache.findUser(maintainer.handle)

            addUserToGroupId(maintainerData, maintainer.handle, BaseCourseInstanceGroup.STUDENTS.slug, studentsGroupId, AccessLevel.OWNER)
            addUserToGroupId(maintainerData, maintainer.handle, BaseCourseInstanceGroup.STUDENT_RESOURCES.slug, studentResourcesGroupId, AccessLevel.OWNER)
            addUserToGroupId(maintainerData, maintainer.handle, BaseCourseInstanceGroup.TEACHER_RESOURCES.slug, teacherResourcesGroupId, AccessLevel.OWNER)
        }

    /**
     * Helper function for creating the group enrollments for all enrollments, so for students, maintainers, and teachers.
     *
     * @param userData - Terraform data object for referencing the user.
     * @param username - The number used to reference them in RepoMan (S or P number for example).
     * @param groupSlug - The Gitlab group slug on which we want to add them, these are mainly the slug of the DefaultCourseInstanceGroup.
     * @param accessLevel - Level of access that the user should have on the group.
     */
    private fun addUserToGroupId(userData: DataGitlabUser, username: String, groupSlug: String, groupId: String, accessLevel: AccessLevel) =
        GroupMembership.Builder.create(this, id { "ci_${courseInstanceSlug}_${username}_$groupSlug" })
            .groupId(groupId)
            .userId(userData.userId)
            .accessLevel(accessLevel.lowercase())
            .build()

    /**
     * Helper function to find a user data reference by handle.
     *
     * @param handle The handle of the user to find.
     *
     * @return The user data reference for the user with the given handle.
     *
     * @throws IllegalStateException if the user with the given handle is not found, this should never happen!
     */
    private fun UserDataCache.findUser(handle: String): DataGitlabUser = this[handle] ?: error("User with handle $handle not found")

    /**
     * Helper function to get the list of unique users from a list of enrollments.
     *
     * @return A list of unique enrollments.
     */
    private fun List<Enrollment>.uniqueUsernames(): List<Enrollment> = this.groupBy { it.username }.values.map { it.first() }
}
