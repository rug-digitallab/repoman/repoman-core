package nl.rug.digitallab.repoman.worker.gitlab.workers

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.common.OperationId
import nl.rug.digitallab.repoman.common.enums.OperationType
import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.events.configurations.ProjectSource
import nl.rug.digitallab.repoman.common.events.configurations.StudentGroupsConfiguration
import nl.rug.digitallab.repoman.common.events.operations.OperationEvent
import nl.rug.digitallab.repoman.worker.gitlab.command.CommandInvoker
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.CreateCourseInstanceCommand
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.CreateCourseSeriesCommand
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.UpdateEnrollmentsCommand
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.UpdateStudentGroupsCommand
import nl.rug.digitallab.repoman.worker.gitlab.test.*
import org.junit.jupiter.api.Test
import org.mockito.Mockito.verify
import org.mockito.kotlin.*

@QuarkusTest
class OperationConsumerTest {
    @Inject
    private lateinit var operationConsumer: OperationConsumer

    @InjectMock
    private lateinit var producer: OperationStatusProducer

    @InjectMock
    private lateinit var commandInvoker: CommandInvoker

    @Test
    @RunOnVertxContext
    fun `The correct number of events should be produced after a new course series event`(asserter: UniAsserter) {
        val event = generateCreateCourseSeriesEvent()

        asserter.assertThat({ operationConsumer.handleOperation(event) }) {
            verify(producer).queueStartedStatus(event)
            verify(producer).queueCompletedStatus(event)
            verify(commandInvoker).execute(any<CreateCourseSeriesCommand>())
        }
    }

    @Test
    @RunOnVertxContext
    fun `The correct number of events should be produced after a new course series with existing group event`(asserter: UniAsserter) {
        val event = generateCreateCourseSeriesEvent(groupId = 1)

        asserter.assertThat({ operationConsumer.handleOperation(event) }) {
            verify(producer).queueStartedStatus(event)
            verify(producer).queueCompletedStatus(event)
            verify(commandInvoker).execute(any<CreateCourseSeriesCommand>())
        }
    }

    @Test
    @RunOnVertxContext
    fun `The correct number of events should be produced after a new course instance event`(asserter: UniAsserter) {
        val event = generateCreateCourseInstanceEvent()

        asserter.assertThat({ operationConsumer.handleOperation(event) }) {
            verify(producer).queueStartedStatus(event)
            verify(producer).queueCompletedStatus(event)
            verify(commandInvoker).execute(any<CreateCourseInstanceCommand>())
        }
    }

    @Test
    @RunOnVertxContext
    fun `The correct number of events should be produced after a new update student groups event`(asserter: UniAsserter) {
        val event = generateUpdateStudentGroupsEvent()

        asserter.assertThat({ operationConsumer.handleOperation(event) }) {
            verify(producer).queueStartedStatus(event)
            verify(producer).queueCompletedStatus(event)
            verify(commandInvoker).execute(any<UpdateStudentGroupsCommand>())
        }
    }

    @Test
    @RunOnVertxContext
    fun `The correct number of events should be produced after a new update student groups event with forked project ID`(asserter: UniAsserter) {
        val event = generateUpdateStudentGroupsEvent(configuration = StudentGroupsConfiguration(projectSource = ProjectSource.FORK, projectId = 1))

        asserter.assertThat({ operationConsumer.handleOperation(event) }) {
            verify(producer).queueStartedStatus(event)
            verify(producer).queueCompletedStatus(event)
            verify(commandInvoker).execute(any<UpdateStudentGroupsCommand>())
        }
    }

    @Test
    @RunOnVertxContext
    fun `The correct number of events should be produced after a new update enrollments event`(asserter: UniAsserter) {
        val event = generateUpdateEnrollmentsEvent()

        asserter.assertThat({ operationConsumer.handleOperation(event) }) {
            verify(producer).queueStartedStatus(event)
            verify(producer).queueCompletedStatus(event)
            verify(commandInvoker).execute(any<UpdateEnrollmentsCommand>())
        }
    }

    @Test
    @RunOnVertxContext
    fun `Incorrect events should not be handled and result in a failed status`(asserter: UniAsserter) {
        val incorrectEvent = IncorrectEvent()

        asserter.assertThat({ operationConsumer.handleOperation(incorrectEvent) }) {
            verify(producer).queueFailedStatus(eq(incorrectEvent), any(), eq(null))
            verifyNoInteractions(commandInvoker)
        }

    }

    @Test
    @RunOnVertxContext
    fun `Commands that fail to be executed should result in a failed status`(asserter: UniAsserter) {
        whenever(
            commandInvoker.execute(any<CreateCourseSeriesCommand>())
        ).thenThrow(RuntimeException("Test exception"))

        val event = generateCreateCourseSeriesEvent()

        asserter.assertThat({ operationConsumer.handleOperation(event) }) {
            verify(producer).queueStartedStatus(event)
            verify(producer).queueFailedStatus(eq(event), any(), anyOrNull())
        }
    }
}

private data class IncorrectEvent(
    override var operationId: OperationId = OperationId.randomUUID(),
    override var platform: Platform = Platform.GITLAB,
) : OperationEvent {
    override val operationType: OperationType = OperationType.CREATE_COURSE_SERIES
}
