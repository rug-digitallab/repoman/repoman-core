package nl.rug.digitallab.repoman.worker.gitlab.terraform

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralDirectory
import nl.rug.digitallab.repoman.worker.gitlab.test.generateCreateCourseInstanceEvent
import nl.rug.digitallab.repoman.worker.gitlab.test.generateCreateCourseSeriesEvent
import nl.rug.digitallab.repoman.worker.gitlab.test.generateUpdateStudentGroupsEvent
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle
import org.junit.jupiter.api.Assertions.assertTrue
import java.nio.file.Paths
import kotlin.io.path.exists

@QuarkusTest
@TestInstance(Lifecycle.PER_CLASS)
class TerraformGeneratorTest {
    @Inject
    lateinit var generator: TerraformGenerator

    @Test
    fun `The terraform file should exist after course series artifact generation`() = withEphemeralDirectory { outputDirectory ->
        val event = generateCreateCourseSeriesEvent()

        val result = generator.generateCourseSeriesConfig(event, outputDirectory)
        val file = Paths.get(outputDirectory.toString(), "stacks", result.stackId)
        assertTrue(file.exists())
    }

    @Test
    fun `The terraform file should exist after course series with existing group ID artifact generation`() = withEphemeralDirectory { outputDirectory ->
        val event = generateCreateCourseSeriesEvent(groupId = 1)

        val result = generator.generateCourseSeriesConfig(event, outputDirectory)
        val file = Paths.get(outputDirectory.toString(), "stacks", result.stackId)
        assertTrue(file.exists())
    }

    @Test
    fun `The terraform file should exist after course instances artifact generation`() = withEphemeralDirectory { outputDirectory ->
        val event = generateCreateCourseInstanceEvent()

        val result = generator.generateCourseInstanceConfig(event, outputDirectory)
        val file = Paths.get(outputDirectory.toString(), "stacks", result.stackId)
        assertTrue(file.exists())
    }

    @Test
    fun `The terraform file should exist after student group artifact generation`() = withEphemeralDirectory { outputDirectory ->
        val event = generateUpdateStudentGroupsEvent()

        val result = generator.generateStudentGroupsConfig(event, outputDirectory)
        val file = Paths.get(outputDirectory.toString(), "stacks", result.stackId)
        assertTrue(file.exists())
    }

    @Test
    fun `The terraform file should exist after enrollments artifact generation`() = withEphemeralDirectory { outputDirectory ->
        val event = generateUpdateStudentGroupsEvent()

        val result = generator.generateStudentGroupsConfig(event, outputDirectory)
        val file = Paths.get(outputDirectory.toString(), "stacks", result.stackId)
        assertTrue(file.exists())
    }
}
