package nl.rug.digitallab.repoman.worker.gitlab.terraform.command.filter.impl

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.common.events.configurations.EnrollmentsConfiguration
import nl.rug.digitallab.repoman.common.dtos.Student
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.impl.NormalizationFilter
import nl.rug.digitallab.repoman.worker.gitlab.terraform.TerraformFacade
import nl.rug.digitallab.repoman.worker.gitlab.test.*
import org.gitlab4j.api.GitLabApi
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.mockito.kotlin.*
import java.util.Optional

@QuarkusTest
class NormalizationFilterTest {
    @Inject
    private lateinit var normalizationFilter: NormalizationFilter

    @InjectMock
    private lateinit var gitLabApi: GitLabApi

    @Inject
    private lateinit var terraformFacade: TerraformFacade

    @BeforeEach
    fun setup() {
        whenever(gitLabApi.userApi).thenReturn(mock())
        whenever(gitLabApi.userApi.getOptionalUser(any<String>())).thenReturn(
            Optional.of(mock())
        )
    }

    @Test
    fun `The GitLab API should be called to check if the user exists`() {
        assertDoesNotThrow {
            normalizationFilter.filter(generateUpdateEnrollmentsCommand(terraformFacade)) { it.execute() }
        }

        verify(gitLabApi.userApi).getOptionalUser("alice")
        verify(gitLabApi.userApi).getOptionalUser("bob")
        verify(gitLabApi.userApi).getOptionalUser("eve")
    }

    @Test
    fun `Handles with '@' symbols should be normalized`() {
        val command =
            normalizationFilter.filter(generateUpdateEnrollmentsCommand(
                terraformFacade,
                event = generateUpdateEnrollmentsEvent(
                    students = listOf(Student("@alice", "s123456", "Group 1")),
                )
            )) { it.execute() }

        verify(gitLabApi.userApi).getOptionalUser("alice")
        verify(gitLabApi.userApi, times(0)).getOptionalUser("@alice")
    }

    @Test
    fun `Enrollments with non-existent GitLab handles should pass when fail on invalid handles is disabled`() {
        // Mock the fact that the user does not exist
        whenever(gitLabApi.userApi.getOptionalUser(any<String>())).thenReturn(
            Optional.empty()
        )

        assertDoesNotThrow {
            normalizationFilter.filter(generateUpdateEnrollmentsCommand(
                terraformFacade,
                generateUpdateEnrollmentsEvent(
                    configuration = EnrollmentsConfiguration(
                        failOnInvalidHandles = false
                    )
                )
            )) { it.execute() }
        }

        verify(gitLabApi.userApi).getOptionalUser("alice")
        verify(gitLabApi.userApi).getOptionalUser("bob")
        verify(gitLabApi.userApi).getOptionalUser("eve")
    }


    @Test
    fun `Enrollments with non-existent GitLab handles should result in a validation error when fail on invalid handles is enabled`() {
        // Mock the fact that the user does not exist
        whenever(gitLabApi.userApi.getOptionalUser(any<String>())).thenReturn(
            Optional.empty()
        )

        assertThrows<IllegalArgumentException> {
            normalizationFilter.filter(generateUpdateEnrollmentsCommand(terraformFacade)) { it.execute() }
        }

        verify(gitLabApi.userApi).getOptionalUser("alice")
        verify(gitLabApi.userApi).getOptionalUser("bob")
        verify(gitLabApi.userApi).getOptionalUser("eve")
    }
}
