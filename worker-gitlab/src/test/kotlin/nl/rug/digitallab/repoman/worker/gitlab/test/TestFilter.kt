package nl.rug.digitallab.repoman.worker.gitlab.test

import nl.rug.digitallab.repoman.worker.gitlab.command.Command
import nl.rug.digitallab.repoman.worker.gitlab.command.CommandResult
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.CommandFilter
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.RunCommandFilter

open class TestFilter: CommandFilter {
    override val name: String = "TestFilter"

    override fun filter(command: Command, nextFilter: RunCommandFilter): CommandResult {
        // Do something with the command
        return nextFilter(command)
    }
}
