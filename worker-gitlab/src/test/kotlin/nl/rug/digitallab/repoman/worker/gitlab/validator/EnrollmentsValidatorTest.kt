package nl.rug.digitallab.repoman.worker.gitlab.validator

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import jakarta.validation.Validator
import nl.rug.digitallab.repoman.common.dtos.Enrollments
import nl.rug.digitallab.repoman.common.dtos.Student
import nl.rug.digitallab.repoman.common.events.constraints.NoDuplicates
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@QuarkusTest
class EnrollmentsValidatorTest {
    @Inject
    private lateinit var enrollmentsValidator: Validator

    @Test
    fun `Null list should result in no constraint violations`() {
        val testClass = TestClass(null)
        val violations = enrollmentsValidator.validate(testClass)
        assert(violations.isEmpty())
    }

    @Test
    fun `No duplicates should result in no constraint violations`() {
        val testClass = TestClass(listOf("a", "b", "c"))
        val violations = enrollmentsValidator.validate(testClass)
        assert(violations.isEmpty())
    }

    @Test
    fun `Duplicates should result in a constraint violation`() {
        val testClass = TestClass(listOf("a", "b", "a"))
        val violations = enrollmentsValidator.validate(testClass)

        assertTrue(violations.isNotEmpty())
        assertEquals(1, violations.size)
        assertEquals("Duplicate value 'a' found 2 times", violations.first().message)
    }

    @Test
    fun `Duplicate students should result in a constraint violation`() {
        val testClass = Enrollments(
            listOf(
                Student("a", "b", "Group 1"),
                Student("a", "b", "Group 1"),
            ),
            listOf(),
            listOf(),
            listOf(),
        )
        val violations = enrollmentsValidator.validate(testClass)

        assertTrue(violations.isNotEmpty())
        assertEquals(1, violations.size)
        assertEquals("Duplicate value 'Student(handle=a, username=b, groupName=Group 1)' found 2 times", violations.first().message)
    }

    data class TestClass(
        @field:NoDuplicates
        val list: List<String>?
    )
}
