package nl.rug.digitallab.repoman.worker.gitlab.terraform.command

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.approvaltests.ApprovalTestSpec
import nl.rug.digitallab.repoman.common.dtos.Maintainer
import nl.rug.digitallab.repoman.common.dtos.Student
import nl.rug.digitallab.repoman.common.dtos.StudentGroup
import nl.rug.digitallab.repoman.common.dtos.Teacher
import nl.rug.digitallab.repoman.common.enums.StudentPermissions
import nl.rug.digitallab.repoman.common.events.configurations.CourseInstanceConfiguration
import nl.rug.digitallab.repoman.common.events.configurations.EnrollmentsConfiguration
import nl.rug.digitallab.repoman.common.events.configurations.ProjectSource
import nl.rug.digitallab.repoman.common.events.configurations.StudentGroupsConfiguration
import nl.rug.digitallab.repoman.worker.gitlab.command.CommandResult
import nl.rug.digitallab.repoman.worker.gitlab.command.TerraformResult
import nl.rug.digitallab.repoman.worker.gitlab.terraform.TerraformFacade
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.CommandFilterChain
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.impl.AdaptFilter
import nl.rug.digitallab.repoman.worker.gitlab.test.*
import org.approvaltests.Approvals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import org.junit.platform.commons.annotation.Testable

@QuarkusTest
class StackContentTest {
    @Inject
    private lateinit var adaptFilter: AdaptFilter

    @Inject
    private lateinit var terraformFacade: TerraformFacade

    @TestFactory
    fun `Executing the command through the adapt filter should generate the expected JSON output`(): List<DynamicTest> {
        val filters = CommandFilterChain(listOf(adaptFilter))

        val courseSeriesCommandWithGroupId = generateCreateCourseSeriesCommand(terraformFacade, generateCreateCourseSeriesEvent(groupId = 1))
        val courseSeriesCommandWithoutGroupId = generateCreateCourseSeriesCommand(terraformFacade, generateCreateCourseSeriesEvent(groupId = null))
        val courseInstanceCommandWithoutGeneralIssues = generateCreateCourseInstanceCommand(terraformFacade, generateCreateCourseInstanceEvent(configuration = CourseInstanceConfiguration(createGeneralIssues = false)))
        val updateStudentGroupsCommandWithMultipleStudentGroups = generateUpdateStudentGroupsCommand(terraformFacade, generateUpdateStudentGroupsEvent(
            groups = listOf(
                StudentGroup("Group 1"),
                StudentGroup("Group 2"),
            )
        ))
        val updateStudentGroupsCommandWithForkId = generateUpdateStudentGroupsCommand(terraformFacade, generateUpdateStudentGroupsEvent(configuration = StudentGroupsConfiguration(projectSource = ProjectSource.FORK, projectId = 2)))
        val updateStudentGroupsCommandWithoutForkId = generateUpdateStudentGroupsCommand(terraformFacade, generateUpdateStudentGroupsEvent(configuration = StudentGroupsConfiguration(projectSource = ProjectSource.NONE, projectId = null)))
        val updateStudentGroupsCommandWithTemplateId = generateUpdateStudentGroupsCommand(terraformFacade, generateUpdateStudentGroupsEvent(configuration = StudentGroupsConfiguration(projectSource = ProjectSource.TEMPLATE, projectId = 3)))

        val updateEnrollmentsCommand = generateUpdateEnrollmentsCommand(terraformFacade, generateUpdateEnrollmentsEvent())
        val updateEnrollmentsCommandWithMultipleEnrollments = generateUpdateEnrollmentsCommand(terraformFacade, generateUpdateEnrollmentsEvent(
            maintainers = listOf(
                Maintainer("maintainer1", "Maintainer 1"),
            ),
            teachers = listOf(
                Teacher("teacher1", "Teacher 1", null), // Teacher with no student group
                Teacher("teacher2", "Teacher 2", "Group 1"), // Teacher with 2 student groups
                Teacher("teacher2", "Teacher 2", "Group 2"),
            ),
            students = listOf(
                Student("student1", "Student 1", "Group 1"), // Student with 2 student groups
                Student("student1", "Student 1", "Group 2"),
                Student("student2", "Student 2", "Group 2"),
                Student("student3", "Student 3", null), // Student with no student group
            )
        ))
        val updateEnrollmentsCommandWithoutStudents = generateUpdateEnrollmentsCommand(terraformFacade, generateUpdateEnrollmentsEvent(
            maintainers = listOf(
                Maintainer("maintainer1", "Maintainer 1"),
            ),
            teachers = listOf(
                Teacher("teacher1", "Teacher 1", null), // Teacher with no student group
                Teacher("teacher2", "Teacher 2", "Group 1"), // Teacher with 2 student groups
                Teacher("teacher2", "Teacher 2", "Group 2"),
            ),
            students = listOf()
        ))
        val updateEnrollmentsCommandWithLimitedPermissions = generateUpdateEnrollmentsCommand(terraformFacade, generateUpdateEnrollmentsEvent(
            configuration = EnrollmentsConfiguration(studentPermissions = StudentPermissions.LIMITED),
        ))

        return listOf(
            TestCase("course-series-with-group-id") { filters.execute(courseSeriesCommandWithGroupId) },
            TestCase("course-series-without-group-id") { filters.execute(courseSeriesCommandWithoutGroupId) },
            TestCase("course-instance-with-general-issues") { filters.execute(generateCreateCourseInstanceCommand(terraformFacade)) },
            TestCase("course-instance-without-general-issues") { filters.execute(courseInstanceCommandWithoutGeneralIssues) },
            TestCase("course-instance-groups") { filters.execute(generateUpdateStudentGroupsCommand(terraformFacade)) },
            TestCase("course-instance-groups-with-multiple-student-groups") { filters.execute(updateStudentGroupsCommandWithMultipleStudentGroups) },
            TestCase("course-instance-groups-with-fork-id") { filters.execute(updateStudentGroupsCommandWithForkId) },
            TestCase("course-instance-groups-with-template-id") { filters.execute(updateStudentGroupsCommandWithTemplateId) },
            TestCase("course-instance-groups-without-fork-template-id") { filters.execute(updateStudentGroupsCommandWithoutForkId) },
            TestCase("course-instance-enrollments") { filters.execute(updateEnrollmentsCommand) },
            TestCase("course-instance-enrollments-with-multiple-enrollments") { filters.execute(updateEnrollmentsCommandWithMultipleEnrollments) },
            TestCase("course-instance-enrollments-without-students") { filters.execute(updateEnrollmentsCommandWithoutStudents) },
            TestCase("course-instance-enrollments-with-limited-permissions") { filters.execute(updateEnrollmentsCommandWithLimitedPermissions) },
        ).map { testCase ->
            DynamicTest.dynamicTest("Executing the command through the adapt filter should generate the expected JSON output for ${testCase.name}") {
                testStack(testCase)
            }
        }
    }

    @Testable // This annotation is required to make the test discoverable by Approval Tests
    private fun testStack(testCase: TestCase) {
        // Set up the approval tests: file location and simple other settings
        val approvalSpec = ApprovalTestSpec(
            outputDirectory = "tests/stacks",
            name = testCase.name,
            attributes = testCase.attributes
        )

        val result = testCase.execute()
        require(result is TerraformResult)

        Approvals.verify(adapt(result.stackContent), approvalSpec.toOptions())
    }

    private fun adapt(stackContent: String): String {
        // Always fix "expires_at" : "yyyy-MM-dd" to a fixed date
        val expiresAtRegex = Regex("""expires_at"\s*:\s*"\d{4}-\d{2}-\d{2}"""")
        val fixedExpiresAt = expiresAtRegex.replace(stackContent, """expires_at": "1970-01-01"""")

        return fixedExpiresAt
    }
}

data class TestCase(
    val name: String,
    val attributes: List<String> = listOf("stack"),
    val execute: () -> CommandResult,
)
