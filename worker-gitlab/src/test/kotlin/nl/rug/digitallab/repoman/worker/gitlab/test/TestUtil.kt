package nl.rug.digitallab.repoman.worker.gitlab.test

import nl.rug.digitallab.repoman.common.GroupId
import nl.rug.digitallab.repoman.common.OperationId
import nl.rug.digitallab.repoman.common.dtos.*
import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.events.configurations.CourseInstanceConfiguration
import nl.rug.digitallab.repoman.common.events.configurations.EnrollmentsConfiguration
import nl.rug.digitallab.repoman.common.events.configurations.ProjectSource
import nl.rug.digitallab.repoman.common.events.configurations.StudentGroupsConfiguration
import nl.rug.digitallab.repoman.common.events.operations.CreateCourseInstanceEvent
import nl.rug.digitallab.repoman.common.events.operations.CreateCourseSeriesEvent
import nl.rug.digitallab.repoman.common.events.operations.UpdateEnrollmentsEvent
import nl.rug.digitallab.repoman.common.events.operations.UpdateStudentGroupsEvent
import nl.rug.digitallab.repoman.worker.gitlab.terraform.TerraformFacade
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.CreateCourseInstanceCommand
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.CreateCourseSeriesCommand
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.UpdateEnrollmentsCommand
import nl.rug.digitallab.repoman.worker.gitlab.command.impl.UpdateStudentGroupsCommand

const val dummyCourseSeriesName = "Operating Systems"
const val dummyCourseSeriesSlug = "operating-systems"
const val dummyCourseInstanceName = "2024-2025"
const val dummyCourseInstanceSlug = "2024-2025"

fun generateCreateCourseSeriesEvent(
    name: String = dummyCourseSeriesName,
    slug: String = dummyCourseSeriesSlug,
    groupId: GroupId? = null,
) = CreateCourseSeriesEvent(
    operationId = OperationId.randomUUID(),
    platform = Platform.GITLAB,
    courseSeriesName = name,
    courseSeriesSlug = slug,
    groupId = groupId,
)

fun generateCreateCourseSeriesCommand(
    terraformFacade: TerraformFacade,
    event: CreateCourseSeriesEvent = generateCreateCourseSeriesEvent(),
) = CreateCourseSeriesCommand(
    event,
    terraformFacade,
)

fun generateCreateCourseInstanceEvent(
    name: String = dummyCourseInstanceName,
    slug: String = dummyCourseInstanceSlug,
    seriesSlug: String = dummyCourseSeriesSlug,
    configuration: CourseInstanceConfiguration = CourseInstanceConfiguration(
      createGeneralIssues = true,
    ),
) = CreateCourseInstanceEvent(
    operationId = OperationId.randomUUID(),
    platform = Platform.GITLAB,
    courseSeriesSlug = seriesSlug,
    courseInstanceName = name,
    courseInstanceSlug = slug,
    configuration = configuration,
)

fun generateCreateCourseInstanceCommand(
    terraformFacade: TerraformFacade,
    event: CreateCourseInstanceEvent = generateCreateCourseInstanceEvent(),
) = CreateCourseInstanceCommand(
    event,
    terraformFacade,
)

fun generateUpdateStudentGroupsEvent(
    seriesSlug: String = dummyCourseSeriesSlug,
    instanceSlug: String = dummyCourseInstanceSlug,
    groups: List<StudentGroup> = listOf(
        StudentGroup("Group 1"),
        StudentGroup("Group 2"),
        StudentGroup("Group 3"),
    ),
    configuration: StudentGroupsConfiguration = StudentGroupsConfiguration(
        projectSource = ProjectSource.NONE,
        projectId = null,
    ),
) = UpdateStudentGroupsEvent(
    operationId = OperationId.randomUUID(),
    platform = Platform.GITLAB,
    courseSeriesSlug = seriesSlug,
    courseInstanceSlug = instanceSlug,
    groups = groups,
    configuration = configuration,
)

fun generateUpdateStudentGroupsCommand(
    terraformFacade: TerraformFacade,
    event: UpdateStudentGroupsEvent = generateUpdateStudentGroupsEvent(),
) = UpdateStudentGroupsCommand(
    event,
    terraformFacade,
)

fun generateUpdateEnrollmentsEvent(
    seriesSlug: String = dummyCourseSeriesSlug,
    instanceSlug: String = dummyCourseInstanceSlug,
    students: List<Student> = listOf(Student("alice", "s123456", "Group 1")),
    reviewers: List<Reviewer> = listOf(Reviewer("charlie", "s1234567", "Group 2")),
    teachers: List<Teacher> = listOf(Teacher("bob", "p123456", null)),
    maintainers: List<Maintainer> = listOf(Maintainer("eve", "p23456789")),
    configuration: EnrollmentsConfiguration = EnrollmentsConfiguration(
        failOnInvalidHandles = true,
    ),
) = UpdateEnrollmentsEvent(
    operationId = OperationId.randomUUID(),
    platform = Platform.GITLAB,
    courseSeriesSlug = seriesSlug,
    courseInstanceSlug = instanceSlug,
    enrollments = Enrollments(
        students = students,
        reviewers = reviewers,
        teachers = teachers,
        maintainers = maintainers,
    ),
    configuration = configuration,
)

fun generateUpdateEnrollmentsCommand(
    terraformFacade: TerraformFacade,
    event: UpdateEnrollmentsEvent = generateUpdateEnrollmentsEvent(),
) = UpdateEnrollmentsCommand(
    event,
    terraformFacade,
)
