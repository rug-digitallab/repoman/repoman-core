package nl.rug.digitallab.repoman.worker.gitlab.terraform.command

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.worker.gitlab.GitLabUploader
import nl.rug.digitallab.repoman.worker.gitlab.command.CommandInvoker
import nl.rug.digitallab.repoman.worker.gitlab.command.TerraformResult
import nl.rug.digitallab.repoman.worker.gitlab.exceptions.FailedCommandException
import nl.rug.digitallab.repoman.worker.gitlab.terraform.TerraformFacade
import nl.rug.digitallab.repoman.worker.gitlab.test.generateUpdateStudentGroupsCommand
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.User
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.util.Optional

@QuarkusTest
class CommandInvokerTest {
    @Inject
    private lateinit var commandInvoker: CommandInvoker

    @Inject
    private lateinit var terraformFacade: TerraformFacade

    // Mock the uploader and api, so we can actually test the whole filter chain
    @InjectMock
    private lateinit var gitLabUploader: GitLabUploader

    @InjectMock
    private lateinit var gitLabApi: GitLabApi

    @BeforeEach
    fun setup() {
        whenever(gitLabApi.groupApi).thenReturn(mock())
        whenever(gitLabApi.repositoryApi).thenReturn(mock())
        whenever(gitLabApi.repositoryFileApi).thenReturn(mock())
        whenever(gitLabApi.userApi).thenReturn(mock())
    }

    @Test
    fun `Invoking a command should succeed`() {
        whenever(gitLabApi.groupApi.getOptionalGroup(any())).thenReturn(
            Optional.of(mock())
        )

        whenever(gitLabApi.repositoryApi.getOptionalBranch(any(), any())).thenReturn(
            Optional.of(mock())
        )

        whenever(gitLabApi.repositoryFileApi.getOptionalFile(any(), any(), any())).thenReturn(
            Optional.of(mock())
        )

        whenever(gitLabApi.userApi.getOptionalUser(any<String>())).thenReturn(
            Optional.of(mock())
        )


        val command = generateUpdateStudentGroupsCommand(terraformFacade)

        val result = assertDoesNotThrow {
            commandInvoker.execute(command)
        }

        require(result is TerraformResult)
        assertTrue(result.stackContent.isNotBlank())
        assertTrue(result.stackContent.isNotEmpty())
    }

    @Test
    fun `Invoking a command with a failing filter should result in a failed command exception`() {
        whenever(gitLabApi.userApi.getOptionalUser(any<String>())).thenReturn(
            Optional.of(User().apply { username = "student" })
        )

        val command = generateUpdateStudentGroupsCommand(terraformFacade)
        whenever(gitLabUploader.uploadWithRetries(any(), any(), any(), any())).thenThrow(RuntimeException("Upload failed"))

        val exception = assertThrows<FailedCommandException> { commandInvoker.execute(command) }
        assertNotNull(exception.cause)
        assertNotNull(exception.errorId)
    }
}
