package nl.rug.digitallab.repoman.worker.gitlab

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.worker.gitlab.configs.ArtifactsConfig
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.GitLabApiException
import org.gitlab4j.api.RepositoryApi
import org.gitlab4j.api.RepositoryFileApi
import org.gitlab4j.api.models.RepositoryFile
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.anyString
import org.mockito.Mockito.mock
import org.mockito.kotlin.*

@QuarkusTest
class GitLabUploaderTest {
    @InjectMock
    private lateinit var gitLabApi: GitLabApi

    @Inject
    private lateinit var gitLabUploader: GitLabUploader

    @Inject
    private lateinit var artifactsConfig: ArtifactsConfig

    private val branchName = "branchName"
    private val fileName = "fileName"
    private val content = "content"

    @BeforeEach
    fun setup() {
        val repositoryApi: RepositoryApi = mock()
        val repositoryFileApi: RepositoryFileApi = mock()

        whenever(gitLabApi.repositoryApi).thenReturn(repositoryApi)
        whenever(gitLabApi.repositoryFileApi).thenReturn(repositoryFileApi)
    }

    @Test
    fun `Uploading a terraform file to GitLab should succeed`() {
        val repositoryApi = gitLabApi.repositoryApi
        val repositoryFileApi = gitLabApi.repositoryFileApi

        gitLabUploader.uploadWithRetries(branchName, fileName, content)

        verify(repositoryApi).getBranches(artifactsConfig.projectId)
        verify(repositoryApi).createBranch(artifactsConfig.projectId, branchName, artifactsConfig.mainBranch)
        verify(repositoryFileApi).updateFile(
            any(),
            ArgumentMatchers.any(RepositoryFile::class.java),
            anyString(),
            anyString(),
        )
    }

    @Test
    fun `Uploading a terraform file to GitLab should create a new file if it does not exist`() {
        val repositoryFileApi = gitLabApi.repositoryFileApi
        whenever(repositoryFileApi.updateFile(any(), ArgumentMatchers.any(RepositoryFile::class.java), anyString(), anyString())).then {
            throw GitLabApiException("File exists")
        }

        gitLabUploader.uploadWithRetries(branchName, fileName, content)

        verify(repositoryFileApi).createFile(
            any(),
            ArgumentMatchers.any(RepositoryFile::class.java),
            anyString(),
            anyString(),
        )
    }

    @Test
    fun `Uploading a terraform file to GitLab should be retries a given amount of times`() {
        val repositoryApi = gitLabApi.repositoryApi

        whenever(repositoryApi.getBranches(anyOrNull())).then {
            throw GitLabApiException("Test Exception")
        }

        val retries = 5

        try {
            gitLabUploader.uploadWithRetries(branchName, fileName, content, retries)
        } catch (e: Exception) {
            // Do nothing
        }

        verify(repositoryApi, times(retries + 1)).getBranches(any())
    }
}


