package nl.rug.digitallab.repoman.worker.gitlab.workers

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.reactive.messaging.memory.InMemoryConnector
import io.smallrye.reactive.messaging.memory.InMemorySink
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.common.OperationId
import nl.rug.digitallab.repoman.common.enums.OperationStatus
import nl.rug.digitallab.repoman.common.enums.Platform
import nl.rug.digitallab.repoman.common.events.operations.CreateCourseSeriesEvent
import nl.rug.digitallab.repoman.common.events.operations.status.OperationStatusEvent
import org.awaitility.Awaitility.await
import org.eclipse.microprofile.reactive.messaging.spi.Connector
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.kotlin.any

@QuarkusTest
class OperationStatusProducerTest {
    @Inject
    private lateinit var producer: OperationStatusProducer

    @Inject
    @Connector("smallrye-in-memory")
    lateinit var connector: InMemoryConnector

    private val testEvent = CreateCourseSeriesEvent(
        OperationId.randomUUID(),
        Platform.GITLAB,
        "Test CourseSeries",
        "test-course-series",
        null,
    )

    @BeforeEach
    fun `Empty the queue`() {
        val consumer = connector.sink<OperationStatusEvent>("worker-status")
        consumer.clear()
    }

    @Test
    @RunOnVertxContext
    fun `Queuing operation status should result in the expected event being consumed`(asserter: UniAsserter) {
        val inMemorySink = connector.sink<OperationStatusEvent>("worker-status")

        val failedOperationEvent = CreateCourseSeriesEvent(
            testEvent.operationId,
            Platform.GITLAB,
            "Test CourseSeries",
            "test-course-series",
            null,
        )

        asserter.assertThat({ producer.queueStartedStatus(testEvent) }) {
            assertOperationStatusEvent(inMemorySink, OperationStatus.STARTED)
        }.assertThat({ producer.queueCompletedStatus(testEvent) }) {
            assertOperationStatusEvent(inMemorySink, OperationStatus.COMPLETED)
        }.assertThat({ producer.queueFailedStatus(failedOperationEvent, anyString(), any()) }) {
            assertOperationStatusEvent(inMemorySink, OperationStatus.FAILED)
        }
    }

    private fun assertOperationStatusEvent(inMemorySink: InMemorySink<OperationStatusEvent>, expectedStatus: OperationStatus) {
        await().until {
            inMemorySink.received().isNotEmpty()
        }

        val receivedEvent = inMemorySink.received().first().payload
        inMemorySink.clear()

        assertEquals(testEvent.operationId, receivedEvent.operationEvent.operationId)
        assertEquals(expectedStatus, receivedEvent.operationStatus)
    }
}
