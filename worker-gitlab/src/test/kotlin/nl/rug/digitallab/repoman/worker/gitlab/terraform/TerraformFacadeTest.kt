package nl.rug.digitallab.repoman.worker.gitlab.terraform

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.worker.gitlab.test.generateCreateCourseInstanceEvent
import nl.rug.digitallab.repoman.worker.gitlab.test.generateCreateCourseSeriesEvent
import nl.rug.digitallab.repoman.worker.gitlab.test.generateUpdateEnrollmentsEvent
import nl.rug.digitallab.repoman.worker.gitlab.test.generateUpdateStudentGroupsEvent
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow

@QuarkusTest
class TerraformFacadeTest {
    @Inject
    private lateinit var facade: TerraformFacade

    @Test
    fun `The facade should generate the expected Terraform stack for course series`() {
        val result = assertDoesNotThrow {
            facade.createCourseSeries(generateCreateCourseSeriesEvent())
        }

        assertTrue(result.stackContent.isNotBlank())
        assertTrue(result.stackContent.isNotEmpty())
    }

    @Test
    fun `The facade should generate the expected Terraform stack for course instances`() {
        val result = assertDoesNotThrow {
            facade.createCourseInstance(generateCreateCourseInstanceEvent())
        }

        assertTrue(result.stackContent.isNotBlank())
        assertTrue(result.stackContent.isNotEmpty())
    }

    @Test
    fun `The facade should generate the expected Terraform stack for student groups`() {
        val result = assertDoesNotThrow {
            facade.updateStudentGroups(generateUpdateStudentGroupsEvent())
        }

        assertTrue(result.stackContent.isNotBlank())
        assertTrue(result.stackContent.isNotEmpty())
    }

    @Test
    fun `The facade should generate the expected Terraform stack for enrollments`() {
        val result = assertDoesNotThrow {
            facade.updateEnrollments(generateUpdateEnrollmentsEvent())
        }

        assertTrue(result.stackContent.isNotBlank())
        assertTrue(result.stackContent.isNotEmpty())
    }
}
