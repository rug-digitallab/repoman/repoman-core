package nl.rug.digitallab.repoman.worker.gitlab.terraform.command.filter.impl

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.common.dtos.Student
import nl.rug.digitallab.repoman.common.dtos.StudentGroup
import nl.rug.digitallab.repoman.common.dtos.Teacher
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.impl.ValidationFilter
import nl.rug.digitallab.repoman.worker.gitlab.terraform.TerraformFacade
import nl.rug.digitallab.repoman.worker.gitlab.test.*
import org.gitlab4j.api.GitLabApi
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.mockito.kotlin.*
import java.util.Optional

@QuarkusTest
class ValidationFilterTest {
    @Inject
    private lateinit var validationFilter: ValidationFilter

    @InjectMock
    private lateinit var gitLabApi: GitLabApi

    @Inject
    private lateinit var terraformFacade: TerraformFacade

    @BeforeEach
    fun setup() {
        whenever(gitLabApi.groupApi).thenReturn(mock())
        whenever(gitLabApi.repositoryApi).thenReturn(mock())
        whenever(gitLabApi.repositoryFileApi).thenReturn(mock())
        whenever(gitLabApi.userApi).thenReturn(mock())

        whenever(gitLabApi.groupApi.getOptionalGroup(any())).thenReturn(
            Optional.of(mock())
        )

        whenever(gitLabApi.repositoryApi.getOptionalBranch(any(), any())).thenReturn(
            Optional.of(mock())
        )

        whenever(gitLabApi.repositoryFileApi.getOptionalFile(any(), any(), any())).thenReturn(
            Optional.of(mock())
        )

        whenever(gitLabApi.userApi.getOptionalUser(any<String>())).thenReturn(
            Optional.of(mock())
        )
    }

    @TestFactory
    fun `Valid commands should pass validation`(): List<DynamicTest> {
        val validCommands = listOf(
            generateCreateCourseSeriesCommand(terraformFacade),
            generateCreateCourseInstanceCommand(terraformFacade),
            generateUpdateStudentGroupsCommand(terraformFacade),
            generateUpdateEnrollmentsCommand(terraformFacade),
        )

        return validCommands.map { command ->
            DynamicTest.dynamicTest("Command $command should pass validation") {
                assertDoesNotThrow {
                    validationFilter.filter(command) { it.execute() }
                }
            }
        }
    }

    @Test
    fun `Commands that are not handled by the validation filter should pass validation`() {
        val command = TestCommand()

        assertDoesNotThrow {
            validationFilter.filter(command) { it.execute() }
        }
    }

    @TestFactory
    fun `Slugs and group names that do not adhere to GitLab's naming restrictions should result in a validation error`(): List<DynamicTest> {
        val invalidCommands = listOf(
            generateCreateCourseSeriesCommand(terraformFacade, generateCreateCourseSeriesEvent(name = "inv@lid name")),
            generateCreateCourseSeriesCommand(terraformFacade, generateCreateCourseSeriesEvent(slug = "inv@lid-slug")),
            generateCreateCourseInstanceCommand(terraformFacade, generateCreateCourseInstanceEvent(name = "inv@lid name")),
            generateCreateCourseInstanceCommand(terraformFacade, generateCreateCourseInstanceEvent(slug = "inv@lid-slug")),
            generateCreateCourseInstanceCommand(terraformFacade, generateCreateCourseInstanceEvent(seriesSlug = "inv@lid-slug")),
            generateUpdateStudentGroupsCommand(terraformFacade, generateUpdateStudentGroupsEvent(seriesSlug = "inv@lid-slug")),
            generateUpdateStudentGroupsCommand(terraformFacade, generateUpdateStudentGroupsEvent(instanceSlug = "inv@lid-slug")),
            generateUpdateStudentGroupsCommand(terraformFacade, generateUpdateStudentGroupsEvent(
                groups = listOf(
                    StudentGroup("inv@lid name"),
                )
            )),
            generateUpdateEnrollmentsCommand(terraformFacade, generateUpdateEnrollmentsEvent(seriesSlug ="inv@lid-slug")),
            generateUpdateEnrollmentsCommand(terraformFacade, generateUpdateEnrollmentsEvent(instanceSlug = "inv@lid-slug")),
        )

        return invalidCommands.map { command ->
            DynamicTest.dynamicTest("Command $command should fail validation") {
                assertThrows<IllegalArgumentException> {
                    validationFilter.filter(command) { it.execute() }
                }
            }
        }
    }

    @TestFactory
    fun `Commands that reference a non-existent course series should result in a validation error`(): List<DynamicTest> {
        // Mock the fact that the course series branch does not exist
        whenever(gitLabApi.repositoryApi.getOptionalBranch(any(), any())).thenReturn(
            Optional.empty()
        )

        val invalidCommands = listOf(
            generateCreateCourseInstanceCommand(terraformFacade, generateCreateCourseInstanceEvent(seriesSlug = "non-existent-slug")),
            generateUpdateStudentGroupsCommand(terraformFacade, generateUpdateStudentGroupsEvent(seriesSlug = "non-existent-slug")),
        )

        return invalidCommands.map { command ->
            DynamicTest.dynamicTest("Command $command should fail validation") {
                assertThrows<IllegalStateException> {
                    validationFilter.filter(command) { it.execute() }
                }
            }
        }
    }

    @Test
    fun `Commands that require files to be present in the artifacts repository should fail when the file is not present`() {
        // Mock the fact that the file does not exist
        whenever(gitLabApi.repositoryFileApi.getOptionalFile(any(), any(), any())).thenReturn(
            Optional.empty()
        )

        val invalidCommands = listOf(
            generateCreateCourseInstanceCommand(terraformFacade),
            generateUpdateStudentGroupsCommand(terraformFacade),
        )

        invalidCommands.forEach { command ->
            assertThrows<IllegalStateException> {
                validationFilter.filter(command) { it.execute() }
            }
        }
    }

    @TestFactory
    fun `Commands that have duplicate enrollments should result in a validation error`(): List<DynamicTest> {
        val invalidCommands = listOf(
            generateUpdateEnrollmentsCommand(
                terraformFacade, generateUpdateEnrollmentsEvent(
                    teachers = listOf(
                        Teacher("teacher1", "Teacher 1", null),
                        Teacher("teacher1", "Teacher 1", null),
                    ),
                )
            ),
            generateUpdateEnrollmentsCommand(
                terraformFacade, generateUpdateEnrollmentsEvent(
                    students = listOf(
                        Student("student1", "Student 1", "Group 1"),
                        Student("student1", "Student 1", "Group 1"),
                    ),
                )
            ),
        )

        return invalidCommands.map { command ->
            DynamicTest.dynamicTest("Command $command should fail validation") {
                assertThrows<IllegalArgumentException> {
                    validationFilter.filter(command) { it.execute() }
                }
            }
        }
    }
}
