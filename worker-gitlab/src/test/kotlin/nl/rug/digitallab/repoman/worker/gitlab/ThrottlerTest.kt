package nl.rug.digitallab.repoman.worker.gitlab

import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@QuarkusTest
class ThrottlerTest {
    @Test
    fun `Making 5 calls through a throttler with 1 request per seconds should take at least 4 seconds`() {
        val throttle = Throttler(1.0) // 1 call per second

        val start = System.currentTimeMillis()

        // Do 5 times
        repeat(5) {
            throttle { /* Do nothing */ }
        }

        val end = System.currentTimeMillis()

        // Should take at least 4 seconds
        assertTrue(end - start >= 4000)
    }
}
