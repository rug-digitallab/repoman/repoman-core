package nl.rug.digitallab.repoman.worker.gitlab.terraform.command.filter.impl

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.worker.gitlab.GitLabUploader
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.impl.UploadFilter
import nl.rug.digitallab.repoman.worker.gitlab.terraform.TerraformFacade
import nl.rug.digitallab.repoman.worker.gitlab.test.generateCreateCourseSeriesCommand
import nl.rug.digitallab.repoman.worker.gitlab.test.generateUpdateEnrollmentsCommand
import nl.rug.digitallab.repoman.worker.gitlab.test.generateUpdateStudentGroupsCommand
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.assertDoesNotThrow
import org.mockito.kotlin.*

@QuarkusTest
class UploadFilterTest {
    @Inject
    private lateinit var uploadFilter: UploadFilter

    @Inject
    private lateinit var terraformFacade: TerraformFacade

    @InjectMock
    private lateinit var gitLabUploader: GitLabUploader

    @TestFactory
    fun `The upload filter should call the GitLab uploader for commands that require it`(): List<DynamicTest> {
        val commands = listOf(
            generateCreateCourseSeriesCommand(terraformFacade),
            generateCreateCourseSeriesCommand(terraformFacade),
            generateUpdateStudentGroupsCommand(terraformFacade),
            generateUpdateEnrollmentsCommand(terraformFacade),
        )

        return commands.map { command ->
            DynamicTest.dynamicTest("The ${command::class.simpleName} command should call the GitLab uploader") {
                assertDoesNotThrow {
                    uploadFilter.filter(command) { it.execute() }
                }

                verify(gitLabUploader).uploadWithRetries(any(), any(), any(), any())
                reset(gitLabUploader)
            }
        }
    }

    @Test
    fun `The upload filter should ignore commands that do not require uploading`() {
        val command = AdaptFilterTest.MockCommand()
        uploadFilter.filter(command) { it.execute() }
        verifyNoInteractions(gitLabUploader)
    }
}
