package nl.rug.digitallab.repoman.worker.gitlab.terraform.command.filter

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.CommandFilterChain
import nl.rug.digitallab.repoman.worker.gitlab.test.TestCommand
import nl.rug.digitallab.repoman.worker.gitlab.test.TestFilter
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.kotlin.any
import org.mockito.kotlin.eq
import org.mockito.kotlin.times
import org.mockito.kotlin.whenever

@QuarkusTest
class CommandFilterChainTest {
    @Test
    fun `The filter chain should execute all filters and the command exactly once`() {
        val testFilter1: TestFilter = mock()
        val testFilter2: TestFilter = mock()
        val testCommand: TestCommand = mock()

        whenever(testFilter1.filter(any(), any())).thenCallRealMethod()
        whenever(testFilter2.filter(any(), any())).thenCallRealMethod()
        whenever(testCommand.execute()).thenCallRealMethod()

        val filterChain = CommandFilterChain(listOf(testFilter1, testFilter2))
        filterChain.execute(testCommand)

        verify(testFilter1, times(1)).filter(eq(testCommand), any())
        verify(testFilter2, times(1)).filter(eq(testCommand), any())
        verify(testCommand, times(1)).execute()
    }

    @Test
    fun `Applying the same filter twice should call the filter exactly twice`() {
        val testFilter: TestFilter = mock()
        val testCommand: TestCommand = mock()

        whenever(testFilter.filter(any(), any())).thenCallRealMethod()
        whenever(testCommand.execute()).thenCallRealMethod()

        val filterChain = CommandFilterChain(listOf(testFilter, testFilter))
        filterChain.execute(testCommand)

        verify(testFilter, times(2)).filter(eq(testCommand), any())
        verify(testCommand, times(1)).execute()
    }

    @Test
    fun `Applying no filters should simply execute the command`() {
        val testCommand: TestCommand = mock()

        whenever(testCommand.execute()).thenCallRealMethod()

        val filterChain = CommandFilterChain(listOf())
        filterChain.execute(testCommand)

        verify(testCommand, times(1)).execute()
    }
}

