package nl.rug.digitallab.repoman.worker.gitlab.validator

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import jakarta.validation.Validator
import nl.rug.digitallab.repoman.common.events.configurations.ProjectSource
import nl.rug.digitallab.repoman.common.events.configurations.StudentGroupsConfiguration
import nl.rug.digitallab.repoman.common.events.constraints.GroupName
import nl.rug.digitallab.repoman.common.events.constraints.GroupSlug
import nl.rug.digitallab.repoman.common.events.operations.OperationEvent
import nl.rug.digitallab.repoman.worker.gitlab.test.generateCreateCourseInstanceEvent
import nl.rug.digitallab.repoman.worker.gitlab.test.generateCreateCourseSeriesEvent
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory

@QuarkusTest
class GitLabValidatorTest {
    @Inject
    private lateinit var gitLabValidator: Validator

    @Test
    fun `Setting a project source to NONE but providing a project id should result in a constraint violation`() {
        val configuration = StudentGroupsConfiguration(
            projectSource = ProjectSource.NONE,
            projectId = 1
        )

        val violations = gitLabValidator.validate(configuration)

        assertTrue(violations.isNotEmpty())
        assertEquals("The projectId should only be set when the project source is FORK or TEMPLATE.", violations.first().message)
    }

    @TestFactory
    fun `Setting project source to FORK or TEMPLATE but not providing a project id should result in a constraint violation`() =
        listOf(ProjectSource.FORK, ProjectSource.TEMPLATE)
            .map { projectSource ->
                DynamicTest.dynamicTest("Setting project source to $projectSource but not providing a project id should result in a constraint violation") {
                    val configuration = StudentGroupsConfiguration(
                        projectSource = projectSource,
                        projectId = null
                    )

                    val violations = gitLabValidator.validate(configuration)

                    assertTrue(violations.isNotEmpty())
                    assertEquals("The projectId should only be set when the project source is FORK or TEMPLATE.", violations.first().message)
                }
            }

    @TestFactory
    fun `Valid group slugs should result in no constraint violations`() = listOf(
        generateCreateCourseSeriesEvent(slug = "valid-slug"),
        generateCreateCourseInstanceEvent(slug = "valid-slug"),
        generateCreateCourseSeriesEvent(slug = "this.i_s.valid"),
        generateCreateCourseSeriesEvent(slug = "0123_abcd_4567"),
        generateCreateCourseSeriesEvent(slug = "repository.git-example"),
        generateCreateCourseSeriesEvent(slug = "editor.atom-example"),
    ).map { event ->
        DynamicTest.dynamicTest("Valid group slugs should result in no constraint violations for: $event") {
            val violations = gitLabValidator.validate(event)
            assertTrue(violations.isEmpty())
        }
    }

    @TestFactory
    fun `Valid group names should result in no constraint violations`() = listOf(
        generateCreateCourseSeriesEvent(name = "Valid Name"),
        generateCreateCourseInstanceEvent(name = "Valid Name"),
        generateCreateCourseSeriesEvent(name = "This (is) also valid"),
        generateCreateCourseSeriesEvent(name = "Another __valid__ name"),
    ).map { event ->
        DynamicTest.dynamicTest("Valid group names should result in no constraint violations for: $event") {
            val violations = gitLabValidator.validate(event)
            assertTrue(violations.isEmpty())
        }
    }

    @TestFactory
    fun `Invalid group slugs should result in a constraint violation`() = listOf(
        TestCase(
            event = generateCreateCourseSeriesEvent(slug = "illegal.git"),
            expectedMessage = "Group slug 'illegal.git' cannot end in .git or .atom"
        ),
        TestCase(
            event = generateCreateCourseInstanceEvent(slug = "illegal.atom"),
            expectedMessage = "Group slug 'illegal.atom' cannot end in .git or .atom"
        ),
        TestCase(
            event = generateCreateCourseSeriesEvent(slug = "v2"),
            expectedMessage = "'v2' is a reserved name!"
        ),
        TestCase(
            event = generateCreateCourseSeriesEvent(slug = "invalid!format"),
            expectedMessage = "'invalid!format' has an invalid format"
        ),
        TestCase(
            event = generateCreateCourseSeriesEvent(slug = "invalid..format"),
            expectedMessage = "'invalid..format' has an invalid format"
        ),
        TestCase(
            event = generateCreateCourseInstanceEvent(slug = "invalidformat."),
            expectedMessage = "'invalidformat.' has an invalid format"
        ),
    ).map { testCase ->
        DynamicTest.dynamicTest("Invalid group slugs should result in a constraint violation for: ${testCase.event}") {
            val violations = gitLabValidator.validate(testCase.event)
            assertTrue(violations.isNotEmpty())
            assertEquals(listOf(testCase.expectedMessage), violations.map { it.message })
        }
    }

    @TestFactory
    fun `Invalid group names should result in a constraint violation`() = listOf(
        TestCase(
            event = generateCreateCourseSeriesEvent(name = "v2"),
            expectedMessage = "'v2' is a reserved name!"
        ),
        TestCase(
            event = generateCreateCourseSeriesEvent(name = "invalid!format"),
            expectedMessage = "'invalid!format' has an invalid format"
        ),
        TestCase(
            event = generateCreateCourseInstanceEvent(name = "invalidformat."),
            expectedMessage = "'invalidformat.' has an invalid format"
        ),
    ).map { testCase ->
        DynamicTest.dynamicTest("Invalid group names should result in a constraint violation for: ${testCase.event}") {
            val violations = gitLabValidator.validate(testCase.event)
            assertTrue(violations.isNotEmpty())
            assertEquals(listOf(testCase.expectedMessage), violations.map { it.message })
        }
    }

    @Test
    fun `Group slug and name validators should pass on null values`() {
        val nullable = NullableTest(
            nullableSlug = null,
            nullableName = null
        )

        val violations = gitLabValidator.validate(nullable)

        assertTrue(violations.isEmpty())
    }

    data class NullableTest(
        @field:GroupSlug
        val nullableSlug: String?,

        @field:GroupName
        val nullableName: String?,
    )

    data class TestCase(
        val event: OperationEvent,
        val expectedMessage: String
    )
}
