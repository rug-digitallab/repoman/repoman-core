package nl.rug.digitallab.repoman.worker.gitlab.terraform.command.filter.impl

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.repoman.worker.gitlab.command.Command
import nl.rug.digitallab.repoman.worker.gitlab.command.CommandResult
import nl.rug.digitallab.repoman.worker.gitlab.command.TerraformResult
import nl.rug.digitallab.repoman.worker.gitlab.command.filter.impl.AdaptFilter
import nl.rug.digitallab.repoman.worker.gitlab.terraform.TerraformFacade
import nl.rug.digitallab.repoman.worker.gitlab.test.generateCreateCourseInstanceCommand
import nl.rug.digitallab.repoman.worker.gitlab.test.generateCreateCourseSeriesCommand
import nl.rug.digitallab.repoman.worker.gitlab.test.generateUpdateEnrollmentsCommand
import nl.rug.digitallab.repoman.worker.gitlab.test.generateUpdateStudentGroupsCommand
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory

@QuarkusTest
class AdaptFilterTest {
    @Inject
    lateinit var adaptFilter: AdaptFilter

    @Inject
    lateinit var terraformFacade: TerraformFacade

    // Check whether the to be removed properties are in fact removed when the file gets adapted
    @TestFactory
    fun `The expected properties should be removed from the JSON contents after adapting the file`(): List<DynamicTest> {
        val commands = listOf(
            generateCreateCourseSeriesCommand(terraformFacade),
            generateCreateCourseInstanceCommand(terraformFacade),
            generateUpdateStudentGroupsCommand(terraformFacade),
            generateUpdateEnrollmentsCommand(terraformFacade),
        )

        return commands.map { command ->
            DynamicTest.dynamicTest("The ${command::class.simpleName} command should be adapted correctly") {
                val result = adaptFilter.filter(command) { it.execute() }
                require(result is TerraformResult)

                val adaptedContent = result.stackContent

                assertFalse(adaptedContent.contains("\"variable\""))
                assertFalse(adaptedContent.contains("\"terraform\""))
                assertFalse(adaptedContent.contains("\"provider\""))
                assertTrue(adaptedContent.contains("\"resource\""))
            }
        }
    }

    @Test
    fun `Results that should not be adapted should remain unchanged`() {
        val adaptedResult = adaptFilter.filter(MockCommand()) { it.execute() }
        require(adaptedResult is MockResult)
        assertEquals("test", adaptedResult.result)
    }

    class MockResult(val result: String) : CommandResult

    class MockCommand : Command {
        override fun execute(): CommandResult = MockResult("test")
    }
}
