package nl.rug.digitallab.repoman.worker.gitlab.test

import nl.rug.digitallab.repoman.worker.gitlab.command.Command
import nl.rug.digitallab.repoman.worker.gitlab.command.CommandResult
import nl.rug.digitallab.repoman.worker.gitlab.command.TerraformResult

open class TestCommand : Command {
    override fun execute(): CommandResult =
        TerraformResult(
            stackId = "",
            stackContent = "",
        )
}
