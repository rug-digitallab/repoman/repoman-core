{
  "resource" : {
    "gitlab_branch" : {
      "ci_2024_2025_group_1_project_develop" : {
        "name" : "develop",
        "project" : "${gitlab_project.ci_2024_2025_group_1_project.id}",
        "ref" : "main"
      },
      "ci_2024_2025_group_2_project_develop" : {
        "name" : "develop",
        "project" : "${gitlab_project.ci_2024_2025_group_2_project.id}",
        "ref" : "main"
      },
      "ci_2024_2025_group_3_project_develop" : {
        "name" : "develop",
        "project" : "${gitlab_project.ci_2024_2025_group_3_project.id}",
        "ref" : "main"
      }
    },
    "gitlab_branch_protection" : {
      "ci_2024_2025_group_1_project_main" : {
        "allow_force_push" : false,
        "branch" : "main",
        "merge_access_level" : "maintainer",
        "project" : "${gitlab_project.ci_2024_2025_group_1_project.id}",
        "push_access_level" : "no one"
      },
      "ci_2024_2025_group_2_project_main" : {
        "allow_force_push" : false,
        "branch" : "main",
        "merge_access_level" : "maintainer",
        "project" : "${gitlab_project.ci_2024_2025_group_2_project.id}",
        "push_access_level" : "no one"
      },
      "ci_2024_2025_group_3_project_main" : {
        "allow_force_push" : false,
        "branch" : "main",
        "merge_access_level" : "maintainer",
        "project" : "${gitlab_project.ci_2024_2025_group_3_project.id}",
        "push_access_level" : "no one"
      }
    },
    "gitlab_group" : {
      "ci_2024_2025_group_1" : {
        "name" : "Group 1",
        "parent_id" : "${local.ci_2024_2025_students_group_id}",
        "path" : "group-1"
      },
      "ci_2024_2025_group_2" : {
        "name" : "Group 2",
        "parent_id" : "${local.ci_2024_2025_students_group_id}",
        "path" : "group-2"
      },
      "ci_2024_2025_group_3" : {
        "name" : "Group 3",
        "parent_id" : "${local.ci_2024_2025_students_group_id}",
        "path" : "group-3"
      }
    },
    "gitlab_project" : {
      "ci_2024_2025_group_1_project" : {
        "archive_on_destroy" : true,
        "default_branch" : "main",
        "name" : "Group 1 Project",
        "namespace_id" : "${gitlab_group.ci_2024_2025_group_1.id}",
        "path" : "group-1-project",
        "visibility_level" : "private"
      },
      "ci_2024_2025_group_2_project" : {
        "archive_on_destroy" : true,
        "default_branch" : "main",
        "name" : "Group 2 Project",
        "namespace_id" : "${gitlab_group.ci_2024_2025_group_2.id}",
        "path" : "group-2-project",
        "visibility_level" : "private"
      },
      "ci_2024_2025_group_3_project" : {
        "archive_on_destroy" : true,
        "default_branch" : "main",
        "name" : "Group 3 Project",
        "namespace_id" : "${gitlab_group.ci_2024_2025_group_3.id}",
        "path" : "group-3-project",
        "visibility_level" : "private"
      }
    }
  }
}