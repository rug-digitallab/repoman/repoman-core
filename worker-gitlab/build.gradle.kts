plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.project")
}

dependencies {
    val cdktfVersion: String by project
    val providerGitlabVersion: String by project
    val kotlinMockitoVersion: String by project
    val gitlab4jVersion: String by project
    val commonKotlinVersion: String by project

    implementation(project(":common-repoman"))

    implementation("com.hashicorp:cdktf:$cdktfVersion")
    implementation("com.hashicorp:cdktf-provider-gitlab:$providerGitlabVersion")
    implementation("org.gitlab4j:gitlab4j-api:$gitlab4jVersion")
    implementation("io.quarkus:quarkus-hibernate-validator")
    implementation("io.quarkus:quarkus-messaging-kafka")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation("io.quarkus:quarkus-observability-devservices-lgtm")

    testImplementation("io.quarkus:quarkus-junit5-mockito")
    testImplementation("io.quarkus:quarkus-test-kafka-companion")
    testImplementation("io.quarkus:quarkus-test-vertx")
    testImplementation("org.mockito.kotlin:mockito-kotlin:$kotlinMockitoVersion")
    testImplementation("io.smallrye.reactive:smallrye-reactive-messaging-in-memory")
    testImplementation("nl.rug.digitallab.common.kotlin:approval-tests:$commonKotlinVersion")
}
